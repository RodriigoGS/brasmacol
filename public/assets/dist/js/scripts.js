(function ($) {

	/**
	 * File IMAGE
	 */

	function readURL(input, fake) 
	{
		var fake = fake.closest('.input-file-fake');

	    if (input.files && input.files[0]) 
	    {
	        var reader = new FileReader();

	        reader.onload = function (e) 
	        {
	        	fake.find('.thumb-text').text(input.files[0].name);
	            fake.find('.thumb-img').css('background-image', 'url(' + e.target.result + ')');
	        }

	        reader.readAsDataURL(input.files[0]);
	    }
	}

	$("#thumb, #image, #catalogo").change(function(){
	    readURL(this, $(this));
	});

	/**
	 * INPUT CPF/CNPJ
	 */

	if( $('.js-cpf-cnpj') )
	{
		$('.js-cpf-cnpj').click(function ()
		{
			if($(this).val() == 'cpf') 
			{
				$('#cpf').removeClass('hide').attr('required', 'required').val('');
				$('#cnpj').removeAttr('required').addClass('hide').val('');

			} else if($(this).val() == 'cnpj') 
			{
				$('#cnpj').removeClass('hide').attr('required', 'required').val('');
				$('#cpf').addClass('hide').removeAttr('required').val('');
			}
		});
	}

})(jQuery);