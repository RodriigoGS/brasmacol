$(document).ready(function(){
	var modalAcesso = document.querySelector('.modalAcesso'),
		modal = document.getElementById('modal'),
		modalFechar = document.querySelector('.btn-close');

	$('.timeline-btn').on('click', function(){
		var id = $(this).data('target'),
			alvo = $(''+id+'');

		$('.timeline-btn').removeClass('active');
		$('.historia_tab').removeClass('active');
		alvo.addClass('active');
		$(this).addClass('active');
	});

	$(window).scroll(function(){
		if($(this).scrollTop() > 0){
			$('.menu').addClass('scrolled');
		}else{
			$('.menu').removeClass('scrolled');
		}
	});

	$('.ActiveTabs').click(function(e){
		e.preventDefault();
		var link = $(this).data('w-tab'),
			linkAtivo = $('.tab-pane1.w--tab-active'),
			tabAtiva = $('.ActiveTabs.w--current');

		tabAtiva.removeClass('w--current');
		$(this).addClass('w--current');

		if($(this).data('w-tab') != 'Tab-all'){
			linkAtivo.removeClass('w--tab-active');
			$('#'+link+'').addClass('w--tab-active');
		}
		else{
			$('.tab-pane1').addClass('w--tab-active');
		}
	});


	modalAcesso.addEventListener("click", function(event){
		console.log('aqui');
		event.preventDefault();
		modal.classList.add('active');
	});

	modalFechar.addEventListener("click", function(){
		modal.classList.remove('active');
	});
});
