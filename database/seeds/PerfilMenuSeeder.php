<?php

use Illuminate\Database\Seeder;

class PerfilMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {     
      DB::table('manager_perfil_menu')->truncate();
        
    	for($i=1; $i <= 73; $i++){

      	 DB::table('manager_perfil_menu')->insert(
  			   array('id_perfil' => 1,
  			         'id_menu' => $i)
        );
    	}

      for($i=14; $i <= 73; $i++){

          DB::table('manager_perfil_menu')->insert(
              array('id_perfil' => 2,
                    'id_menu' => $i)
          );
          
      }
       
    }
}
