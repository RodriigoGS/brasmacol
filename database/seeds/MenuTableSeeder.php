<?php

use App\Menu;
use Illuminate\Database\Seeder;


class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Menu::truncate();

      //------------------------------------------------------
      $menuAvo = Menu::create([
        'id_usuario' => 1,
        'descricao' => 'Administração',        
        'icone' => 'fa-briefcase',
        'grupo_ordem' => 18,
        'ordem_interna' => 0,
        'status' => 1
      ]);

    	$menu = Menu::create([
        'id_usuario' => 1,
        'pai' => $menuAvo->id,
		    'descricao' => 'Menus',        
        'icone' => 'fa-tasks',
        'grupo_ordem' => 18,
        'ordem_interna' => 1,
        'status' => 1
  		]);

  		Menu::create([
        'id_usuario' => 1,
  			'pai' => $menu->id,
		    'descricao' => 'Listar',
        'rota' => 'manager.menus.index',
        'icone' => 'fa-list',
        'grupo_ordem' => 18,
        'ordem_interna' => 1,
        'status' => 1
  		]);

      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Novo',
        'rota' => 'manager.menus.create',
        'icone' => 'fa-pencil-square-o',
        'grupo_ordem' => 18,
        'ordem_interna' => 1,
        'status' => 1
      ]);


      $menu = Menu::create([
        'id_usuario' => 1,
        'pai' => $menuAvo->id,
        'descricao' => 'Perfil',        
        'icone' => 'fa-user-secret',
        'grupo_ordem' => 18,
        'ordem_interna' => 2,
        'status' => 1
      ]);

      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Listar',
        'rota' => 'manager.perfil.index',
        'icone' => 'fa-list',
        'grupo_ordem' => 18,
        'ordem_interna' => 2,
        'status' => 1
      ]);

      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Novo',
        'rota' => 'manager.perfil.create',
        'icone' => 'fa-pencil-square-o',
        'grupo_ordem' => 18,
        'ordem_interna' => 2,
        'status' => 1
      ]);

      $menu = Menu::create([
        'id_usuario' => 1,
        'pai' => $menuAvo->id,
        'descricao' => 'Usuários',        
        'icone' => 'fa-users',
        'grupo_ordem' => 18,
        'ordem_interna' => 3,
        'status' => 1
      ]);

      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Listar',
        'rota' => 'manager.usuario.index',
        'icone' => 'fa-list',
        'grupo_ordem' => 18,        
        'ordem_interna' => 3,
        'status' => 1
      ]);

      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Novo',
        'rota' => 'manager.usuario.create',
        'icone' => 'fa-pencil-square-o',
        'grupo_ordem' => 18,
        'ordem_interna' => 3,
        'status' => 1
      ]);

      $menu = Menu::create([
        'id_usuario' => 1,
        'pai' => $menuAvo->id,
        'descricao' => 'SiteMap',        
        'icone' => 'fa-map-o',
        'grupo_ordem' => 18,
        'ordem_interna' => 4,
        'status' => 1
      ]);

      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Listar',
        'rota' => 'manager.sitemap.index',
        'icone' => 'fa-list',
        'grupo_ordem' => 18,        
        'ordem_interna' => 4,
        'status' => 1
      ]);

      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Novo',
        'rota' => 'manager.sitemap.create',
        'icone' => 'fa-pencil-square-o',
        'grupo_ordem' => 18,
        'ordem_interna' => 4,
        'status' => 1
      ]);

      $menu = Menu::create([
        'id_usuario' => 1,
        'pai' => $menuAvo->id,
        'descricao' => 'Idiomas',        
        'icone' => 'fa-volume-up ',
        'grupo_ordem' => 18,
        'ordem_interna' => 5,
        'status' => 1
      ]);

      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Listar',
        'rota' => 'manager.idiomas.index',
        'icone' => 'fa-list',
        'grupo_ordem' => 18,
        'ordem_interna' => 5,
        'status' => 1
      ]);

      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Novo',
        'rota' => 'manager.idiomas.create',
        'icone' => 'fa-pencil-square-o',
        'grupo_ordem' => 18,
        'ordem_interna' => 5,
        'status' => 1
      ]);

      //------------------------------------------------------
      $menuAvo = Menu::create([
        'id_usuario' => 1,
        'descricao' => 'Notícias',        
        'icone' => 'fa-newspaper-o',
        'grupo_ordem' => 2,
        'ordem_interna' => 0,
        'status' => 1
      ]);

      $menu = Menu::create([
        'id_usuario' => 1,
        'pai' => $menuAvo->id,
        'descricao' => 'Artigos',        
        'icone' => 'fa-file-word-o',
        'grupo_ordem' => 2,
        'ordem_interna' => 1,
        'status' => 1
      ]);

      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Listar',
        'rota' => 'manager.artigo.index',
        'icone' => 'fa-list',
        'grupo_ordem' => 2,
        'ordem_interna' => 1,
        'status' => 1
      ]);

      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Novo',
        'rota' => 'manager.artigo.create',
        'icone' => 'fa-pencil-square-o',
        'grupo_ordem' => 2,
        'ordem_interna' => 1,
        'status' => 1
      ]);

      $menu = Menu::create([
        'id_usuario' => 1,
        'pai' => $menuAvo->id,
        'descricao' => 'Categorias',        
        'icone' => 'fa-object-group',
        'grupo_ordem' => 2,
        'ordem_interna' => 2,
        'status' => 1
      ]);

      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Listar',
        'rota' => 'manager.categoria.index',
        'icone' => 'fa-list',
        'grupo_ordem' => 2,
        'ordem_interna' => 2,
        'status' => 1
      ]);

      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Novo',
        'rota' => 'manager.categoria.create',
        'icone' => 'fa-pencil-square-o',
        'grupo_ordem' => 2,
        'ordem_interna' => 2,
        'status' => 1
      ]);

      //------------------------------------------------------
      $menuAvo = Menu::create([
        'id_usuario' => 1,
        'descricao' => 'Imagens',        
        'icone' => 'fa-image',
        'grupo_ordem' => 3,
        'ordem_interna' => 0,
        'status' => 1
      ]);

      $menu = Menu::create([
        'id_usuario' => 1,
        'pai' => $menuAvo->id,
        'descricao' => 'Galerias',        
        'icone' => 'fa-picture-o',
        'grupo_ordem' => 3,
        'ordem_interna' => 1,
        'status' => 1
      ]);

      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Listar',
        'rota' => 'manager.galeria.index',
        'icone' => 'fa-list',
        'grupo_ordem' => 3,
        'ordem_interna' => 1,
        'status' => 1
      ]);

      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Novo',
        'rota' => 'manager.galeria.create',
        'icone' => 'fa-pencil-square-o',
        'grupo_ordem' => 3,
        'ordem_interna' => 1,
        'status' => 1
      ]);


      $menu = Menu::create([
        'id_usuario' => 1,
        'pai' => $menuAvo->id,
        'descricao' => 'Slider',        
        'icone' => 'fa-file-image-o',
        'grupo_ordem' => 3,
        'ordem_interna' => 2,
        'status' => 1
      ]);

      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Listar',
        'rota' => 'manager.slider.index',
        'icone' => 'fa-list',
        'grupo_ordem' => 3,
        'ordem_interna' => 2,
        'status' => 1
      ]);

      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Novo',
        'rota' => 'manager.slider.create',
        'icone' => 'fa-pencil-square-o',
        'grupo_ordem' => 3,
        'ordem_interna' => 2,
        'status' => 1
      ]);

      //------------------------------------------------------
      $menuAvo = Menu::create([
        'id_usuario' => 1,
        'descricao' => 'Contatos',        
        'icone' => 'fa-phone',
        'grupo_ordem' => 4,
        'ordem_interna' => 0,
        'status' => 1
      ]);

      $menu = Menu::create([
        'id_usuario' => 1,
        'pai' => $menuAvo->id,
        'descricao' => 'Newsletter',        
        'icone' => 'fa-envelope-o',
        'grupo_ordem' => 4,
        'ordem_interna' => 1,
        'status' => 1
      ]);


      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Listar',
        'rota' => 'manager.newsletter.index',
        'icone' => 'fa-list',
        'grupo_ordem' => 4,
        'ordem_interna' => 1,
        'status' => 1
      ]);

      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Novo',
        'rota' => 'manager.newsletter.create',
        'icone' => 'fa-pencil-square-o',
        'grupo_ordem' => 4,
        'ordem_interna' => 1,
        'status' => 1
      ]);


      $menu = Menu::create([
        'id_usuario' => 1,
        'pai' => $menuAvo->id,
        'descricao' => 'Fale Conosco',        
        'icone' => 'fa-comment-o',
        'grupo_ordem' => 4,
        'ordem_interna' => 2,
        'status' => 1
      ]);


      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Listar',
        'rota' => 'manager.contato.index',
        'icone' => 'fa-list',
        'grupo_ordem' => 4,
        'ordem_interna' => 2,
        'status' => 1
      ]);

      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Novo',
        'rota' => 'manager.contato.create',
        'icone' => 'fa-pencil-square-o',
        'grupo_ordem' => 4,
        'ordem_interna' => 2,
        'status' => 1
      ]);

      $menu = Menu::create([
        'id_usuario' => 1,
        'pai' => $menuAvo->id,
        'descricao' => 'Trabalhe Conosco',        
        'icone' => 'fa-comment-o',
        'grupo_ordem' => 4,
        'ordem_interna' => 3,
        'status' => 1
      ]);


      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Listar',
        'rota' => 'manager.trabalheconosco.index',
        'icone' => 'fa-list',
        'grupo_ordem' => 4,
        'ordem_interna' => 3,
        'status' => 1
      ]);

      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Novo',
        'rota' => 'manager.trabalheconosco.create',
        'icone' => 'fa-pencil-square-o',
        'grupo_ordem' => 4,
        'ordem_interna' => 3,
        'status' => 1
      ]);

       $menu = Menu::create([
        'id_usuario' => 1,
        'pai' => $menuAvo->id,
        'descricao' => 'Solicitação de Acesso',        
        'icone' => 'fa-comment-o',
        'grupo_ordem' => 4,
        'ordem_interna' => 4,
        'status' => 1
      ]);


      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Listar',
        'rota' => 'manager.soliciteacesso.index',
        'icone' => 'fa-list',
        'grupo_ordem' => 4,
        'ordem_interna' => 4,
        'status' => 1
      ]);

      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Novo',
        'rota' => 'manager.soliciteacesso.create',
        'icone' => 'fa-pencil-square-o',
        'grupo_ordem' => 4,
        'ordem_interna' => 4,
        'status' => 1
      ]);

      //------------------------------------------------------
      $menuAvo = Menu::create([
        'id_usuario' => 1,
        'descricao' => 'Gerenciador',        
        'icone' => 'fa-files-o',
        'grupo_ordem' => 5,
        'ordem_interna' => 0,
        'status' => 1
      ]);

      $menu = Menu::create([
        'id_usuario' => 1,
        'pai' => $menuAvo->id,
        'descricao' => 'Arquivos',        
        'icone' => 'fa-file-o',
        'grupo_ordem' => 5,
        'ordem_interna' => 1,
        'status' => 1
      ]);


      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Listar',
        'rota' => 'manager.arquivo.index',
        'icone' => 'fa-list',
        'grupo_ordem' => 5,
        'ordem_interna' => 1,
        'status' => 1
      ]);

      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Novo',
        'rota' => 'manager.arquivo.create',
        'icone' => 'fa-pencil-square-o',
        'grupo_ordem' => 5,
        'ordem_interna' => 1,
        'status' => 1
      ]);


      //------------------------------------------------------
      $menuAvo = Menu::create([
        'id_usuario' => 1,
        'descricao' => 'Relatórios',        
        'icone' => 'fa-line-chart',
        'grupo_ordem' => 8,
        'ordem_interna' => 0,
        'status' => 1
      ]);

      $menu = Menu::create([
        'id_usuario' => 1,
        'pai' => $menuAvo->id,
        'descricao' => 'Visitas',        
        'icone' => 'fa-users',
        'grupo_ordem' => 8,
        'ordem_interna' => 1,
        'status' => 1
      ]);


      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Listar',
        'rota' => 'manager.relatorio.index',
        'icone' => 'fa-bar-chart',
        'grupo_ordem' => 8,
        'ordem_interna' => 1,
        'status' => 1
      ]);

      //------------------------------------------------------
      //
      $menuAvo = Menu::create([
        'id_usuario' => 1,
        'descricao' => 'Produtos',        
        'icone' => 'fa-shopping-cart',
        'grupo_ordem' => 9,
        'ordem_interna' => 0,
        'status' => 1
      ]);

      $menu = Menu::create([
        'id_usuario' => 1,
        'pai' => $menuAvo->id,
        'descricao' => 'Produto',        
        'icone' => 'fa-shopping-basket',
        'grupo_ordem' => 9,
        'ordem_interna' => 1,
        'status' => 1
      ]);


      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Listar',
        'rota' => 'manager.produtos.index',
        'icone' => 'fa-list',
        'grupo_ordem' => 9,
        'ordem_interna' => 1,
        'status' => 1
      ]);

      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Novo',
        'rota' => 'manager.produtos.create',
        'icone' => 'fa-pencil-square-o',
        'grupo_ordem' => 9,
        'ordem_interna' => 1,
        'status' => 1
      ]);

      $menu = Menu::create([
        'id_usuario' => 1,
        'pai' => $menuAvo->id,
        'descricao' => 'Categoria',        
        'icone' => 'fa-shopping-basket',
        'grupo_ordem' => 9,
        'ordem_interna' => 2,
        'status' => 1
      ]);


      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Listar',
        'rota' => 'manager.produtos.categorias.index',
        'icone' => 'fa-list',
        'grupo_ordem' => 9,
        'ordem_interna' => 2,
        'status' => 1
      ]);

      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Novo',
        'rota' => 'manager.produtos.categorias.create',
        'icone' => 'fa-pencil-square-o',
        'grupo_ordem' => 9,
        'ordem_interna' => 2,
        'status' => 1
      ]);

      //
      //------------------------------------------------------
      //

      $menuAvo = Menu::create([
        'id_usuario' => 1,
        'descricao' => 'Videos',        
        'icone' => 'fa-youtube-play',
        'grupo_ordem' => 10,
        'ordem_interna' => 0,
        'status' => 1
      ]);

      $menu = Menu::create([
        'id_usuario' => 1,
        'pai' => $menuAvo->id,
        'descricao' => 'Vídeo',        
        'icone' => 'fa-video-camera',
        'grupo_ordem' => 10,
        'ordem_interna' => 1,
        'status' => 1
      ]);


      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Listar',
        'rota' => 'manager.videos.index',
        'icone' => 'fa-list',
        'grupo_ordem' => 10,
        'ordem_interna' => 1,
        'status' => 1
      ]);

      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Novo',
        'rota' => 'manager.videos.create',
        'icone' => 'fa-pencil-square-o',
        'grupo_ordem' => 10,
        'ordem_interna' => 1,
        'status' => 1
      ]);

      $menu = Menu::create([
        'id_usuario' => 1,
        'pai' => $menuAvo->id,
        'descricao' => 'Categoria',        
        'icone' => 'fa-shopping-basket',
        'grupo_ordem' => 10,
        'ordem_interna' => 2,
        'status' => 1
      ]);


      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Listar',
        'rota' => 'manager.videos.categorias.index',
        'icone' => 'fa-list',
        'grupo_ordem' => 10,
        'ordem_interna' => 2,
        'status' => 1
      ]);

      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Novo',
        'rota' => 'manager.videos.categorias.create',
        'icone' => 'fa-pencil-square-o',
        'grupo_ordem' => 10,
        'ordem_interna' => 2,
        'status' => 1
      ]);
        
      //
      //------------------------------------------------------
      //

      $menuAvo = Menu::create([
        'id_usuario' => 1,
        'descricao' => 'Clientes',        
        'icone' => 'fa-users',
        'grupo_ordem' => 11,
        'ordem_interna' => 0,
        'status' => 1
      ]);

      $menu = Menu::create([
        'id_usuario' => 1,
        'pai' => $menuAvo->id,
        'descricao' => 'Cliente',        
        'icone' => 'fa-user',
        'grupo_ordem' => 11,
        'ordem_interna' => 1,
        'status' => 1
      ]);


      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Listar',
        'rota' => 'manager.clientes.index',
        'icone' => 'fa-list',
        'grupo_ordem' => 11,
        'ordem_interna' => 1,
        'status' => 1
      ]);

      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Novo',
        'rota' => 'manager.clientes.create',
        'icone' => 'fa-pencil-square-o',
        'grupo_ordem' => 11,
        'ordem_interna' => 1,
        'status' => 1
      ]);

      //
      //------------------------------------------------------
      //

      $menuAvo = Menu::create([
        'id_usuario' => 1,
        'descricao' => 'Catálogos',        
        'icone' => 'fa-object-group',
        'grupo_ordem' => 12,
        'ordem_interna' => 0,
        'status' => 1
      ]);

      $menu = Menu::create([
        'id_usuario' => 1,
        'pai' => $menuAvo->id,
        'descricao' => 'Catálogo',        
        'icone' => 'fa-file-pdf-o',
        'grupo_ordem' => 12,
        'ordem_interna' => 1,
        'status' => 1
      ]);


      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Listar',
        'rota' => 'manager.catalogos.index',
        'icone' => 'fa-list',
        'grupo_ordem' => 12,
        'ordem_interna' => 1,
        'status' => 1
      ]);

      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Novo',
        'rota' => 'manager.catalogos.create',
        'icone' => 'fa-pencil-square-o',
        'grupo_ordem' => 12,
        'ordem_interna' => 1,
        'status' => 1
      ]);

      //
      //------------------------------------------------------
      //

      $menuAvo = Menu::create([
        'id_usuario' => 1,
        'descricao' => 'Empresas',        
        'icone' => 'fa-university',
        'grupo_ordem' => 13,
        'ordem_interna' => 0,
        'status' => 1
      ]);

      $menu = Menu::create([
        'id_usuario' => 1,
        'pai' => $menuAvo->id,
        'descricao' => 'Empresa',        
        'icone' => 'fa-briefcase',
        'grupo_ordem' => 13,
        'ordem_interna' => 1,
        'status' => 1
      ]);


      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Listar',
        'rota' => 'manager.empresas.index',
        'icone' => 'fa-list',
        'grupo_ordem' => 13,
        'ordem_interna' => 1,
        'status' => 1
      ]);

      Menu::create([
        'id_usuario' => 1,
        'pai' => $menu->id,
        'descricao' => 'Novo',
        'rota' => 'manager.empresas.create',
        'icone' => 'fa-pencil-square-o',
        'grupo_ordem' => 13,
        'ordem_interna' => 1,
        'status' => 1
      ]);

    }
}
