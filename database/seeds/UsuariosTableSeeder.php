<?php

use Illuminate\Database\Seeder;
use App\Usuario;

class UsuariosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Usuario::truncate();

    	Usuario::create([
    		'id_perfil' => 1,
		    'nome' => 'Administrador',
            'usuario' => 'admin',
            'email' => 'contato@designio.com.br',
            'senha' => '$2y$10$9DgDKEvH3J.GRoncSMu.Hu3DGp/CEHZAbHQ/SfcRzA126vR2oMwVq',
            'status' => 1,
            'imagem' => 'admin.png',
            'remember_token' => str_random(10),
  		]);       
    }
}
