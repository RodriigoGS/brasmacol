<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        DB::statement('SET FOREIGN_KEY_CHECKS=0;'); //desativar validacoes de chaves estrangeiras        


        $this->call(PerfilTableSeeder::class);
        $this->call(UsuariosTableSeeder::class);
        $this->call(MenuTableSeeder::class);
        $this->call(PerfilMenuSeeder::class);
        $this->call(ConfigsTableSeeder::class);
        $this->call(IdiomasTableSeeder::class);


        DB::statement('SET FOREIGN_KEY_CHECKS=1;'); //reativar validacoes de chaves estrangeiras
    }
}
