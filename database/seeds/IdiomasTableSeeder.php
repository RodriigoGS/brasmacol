<?php

use Illuminate\Database\Seeder;

use App\Idioma;

class IdiomasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * TRUNCATE
         */
        Idioma::truncate();

    	/**
    	 * [$idiomas description]
    	 * @var [type]
    	 */
        $idiomas = [
        	0 => [
        		'locale' 	=> 'pt-BR',
        		'format' 	=> 'pt_BR',
        		'nome' 		=> 'Português Brasil'
        	],
        	1 => [
        		'locale' 	=> 'en',
        		'format' 	=> 'en',
        		'nome' 		=> 'English'
        	],
        	2 => [
        		'locale' 	=> 'es',
        		'format' 	=> 'es_ES',
        		'nome' 		=> 'Español'
        	],
        ];

        /**
         * [idiomas]
         * @var [type]
         */
        foreach ($idiomas as $idioma) 
        {
        	Idioma::create( $idioma );
        }
    }
}
