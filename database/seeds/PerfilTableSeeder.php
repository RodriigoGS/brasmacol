<?php

use Illuminate\Database\Seeder;
use App\Perfil;

class PerfilTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Perfil::truncate();

        Perfil::create([
		    'descricao' => 'Master',
            'status' => 1
  		]);

        Perfil::create([
            'descricao' => 'Administrador',
            'status' => 1
        ]);
  		
    }
}
