<?php

use App\Config;
use Illuminate\Database\Seeder;

class ConfigsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Config::create([
	        'nome' => 'admin_url',        
	        'valor' => 'http://localhost/manager-2/manager/'
	    ]);

	    Config::create([
	        'nome' => 'title',        
	        'valor' => 'Manager - Designio'
	    ]);

	    Config::create([
	        'nome' => 'site_title',        
	        'valor' => ''
	    ]);

	    Config::create([
	        'nome' => 'site_description',
	        'valor' => ''
	    ]);

	    Config::create([
	        'nome' => 'site_tags',
	        'valor' => ''
	    ]);

	    Config::create([
	        'nome' => 'site_email',
	        'valor' => ''
	    ]);

	    Config::create([
	        'nome' => 'site_telefone',
	        'valor' => ''
	    ]);

	    Config::create([
	        'nome' => 'site_latlong',
	        'valor' => ''
	    ]);

	    Config::create([
	        'nome' => 'site_endereco',
	        'valor' => ''
	    ]);

	    Config::create([
	        'nome' => 'site_url',
	        'valor' => 'http://localhost/manager-2/'
	    ]);

	    Config::create([
	        'nome' => 'site_social_facebook',
	        'valor' => ''
	    ]);

	    Config::create([
	        'nome' => 'site_social_instagram',
	        'valor' => ''
	    ]);

	    Config::create([
	        'nome' => 'site_social_youtube',
	        'valor' => ''
	    ]);

	    Config::create([
	        'nome' => 'site_social_twitter',
	        'valor' => ''
	    ]);

	    Config::create([
	        'nome' => 'site_social_pinterest',
	        'valor' => ''
	    ]);

	    Config::create([
	        'nome' => 'criptografia',
	        'valor' => ''
	    ]);
    }
}
