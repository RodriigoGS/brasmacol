<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDescricaoToCategoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categorias', function (Blueprint $table) 
        {
            $table->text('descricao')->after('thumb')->nullable();
            
            $table->integer('idioma_id')->unsigned()->nullable()->after('id_usuario');
            $table->foreign('idioma_id')->references('id')->on('idiomas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categorias', function (Blueprint $table) 
        {
            $table->dropColumn('descricao');
            $table->dropColumn('idioma_id');
        });
    }
}
