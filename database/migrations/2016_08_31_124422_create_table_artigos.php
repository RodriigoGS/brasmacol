<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableArtigos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manager_artigos', function (Blueprint $table) {
            $table->increments('id');            
            $table->string('titulo');
            $table->string('slug');
            $table->text('resumo')->nullable();
            $table->text('texto');
            $table->string('imagem')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('manager_artigos');
    }
}
