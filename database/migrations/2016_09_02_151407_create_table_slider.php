<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSlider extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manager_slider', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_usuario')->unsigned();
            $table->foreign('id_usuario')->references('id')->on('manager_usuarios')
                                                           ->onDelete('no action')
                                                           ->onUpdate('no action');
            $table->integer('id_imagem')->unsigned()->nullable();
            $table->string('titulo')->nullable();
            $table->string('titulo_botao')->nullable();
            $table->text('texto')->nullable();
            $table->string('url')->nullable();
            $table->integer('ordem');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('manager_slider');
    }
}
