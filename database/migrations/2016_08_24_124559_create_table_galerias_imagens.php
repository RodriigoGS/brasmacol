<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGaleriasImagens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manager_galerias_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_galeria')->unsigned()->nullable();            
            $table->foreign('id_galeria')->references('id')->on('manager_galerias')
                                         ->onDelete('set null')
                                         ->onUpdate('no action');
            $table->string('imagem');
            $table->integer('ordem')->nullable()->default(1);
            $table->integer('status')->nullable()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('manager_galerias_imagens');
    }
}
