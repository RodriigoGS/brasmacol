<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) 
        {
            $table->increments('id');

            $table->integer('idioma_id')->unsigned()->nullable();
            $table->foreign('idioma_id')->references('id')->on('idiomas');

            $table->integer('ano');
            $table->text('descricao');
            $table->string('image');

            $table->char('status', 1)->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('empresas');
    }
}
