<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableArtigosCategorias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manager_artigos_categorias', function (Blueprint $table) {
            $table->integer('id_artigo')->unsigned();
            $table->integer('id_categoria')->unsigned();
            $table->foreign('id_artigo')->references('id')->on('manager_artigos')->onDelete('CASCADE');
            $table->foreign('id_categoria')->references('id')->on('manager_categorias')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('manager_artigos_categorias');
    }
}
