<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manager_menus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pai')->unsigned()->nullable();
            $table->foreign('pai')
                      ->references('id')->on('manager_menus')
                      ->onDelete('set null')
                      ->onUpdate('no action');
            $table->string('descricao');
            $table->string('rota')->nullable();
            $table->string('icone')->nullable();
            $table->integer('grupo_ordem')->nullable();
            $table->integer('ordem_interna')->nullable()->default(1);
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('manager_menus');
    }
}
