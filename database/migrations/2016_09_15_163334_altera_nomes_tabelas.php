<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlteraNomesTabelas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('manager_categorias', 'categorias');
        Schema::rename('manager_artigos', 'artigos');
        Schema::rename('manager_artigos_categorias', 'artigos_categorias');
        Schema::rename('manager_contatos', 'contatos');
        Schema::rename('manager_galerias', 'galerias');
        Schema::rename('manager_galerias_imagens', 'galerias_imagens');
        Schema::rename('manager_newsletter', 'newsletter');
        Schema::rename('manager_slider', 'sliders');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
