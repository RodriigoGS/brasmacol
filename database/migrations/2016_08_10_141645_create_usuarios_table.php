<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manager_usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_perfil')->unsigned()->nullable();
            $table->foreign('id_perfil')->references('id')->on('manager_perfis')
                                        ->onDelete('set null')
                                        ->onUpdate('no action');
            $table->string('nome');
            $table->string('usuario')->unique();
            $table->string('email')->unique();
            $table->string('senha');
            $table->integer('status')->default(1);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('manager_usuarios');
    }
}
