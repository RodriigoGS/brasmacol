<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class CreateAcessosTable extends Migration
{
    /**
     * Run the migrations. Compatability only with MySQL
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acessos', function (Blueprint $table) {
            $table->increments('id');            
            $table->timestamp('data')->default(DB::raw('CURRENT_TIMESTAMP'))->comment = "timestamp considera fuso horario, UTC";
            $table->string('ip');
            $table->string('caminho')->nullable();
            $table->string('useragent')->nullable();
            
        }); 

        //PARTICIONAMENTO - compativel somente com MySQL (testes em MySQL 5.6.30)
        DB::update("ALTER TABLE acessos DROP PRIMARY KEY, ADD PRIMARY KEY (`id`, `data`);");
        $partitions = 'ALTER TABLE acessos PARTITION BY RANGE(unix_timestamp(data))(';
        $j = 1;
        for($i=1; $i <=5; $i++){ //quantidade de particoes por anos neste caso (5 anos)
            
            $year = date('Y', strtotime('+'.($i-1).' years'));
            //teremos 2 particoes por ano, 1 por semestre:
            $partitions .= "PARTITION p".$year.($j)." values less than (floor(unix_timestamp('".$year."-06-01 00:00:00'))),";
            $partitions .= "PARTITION p".$year.($j+1)." values less than (floor(unix_timestamp('".$year."-12-01 00:00:00'))),";            
            $j += 2;
        }                
        //particao default para registros maiores que os 5 anos presentes nas particoes anteriores:
        $partitions .= "PARTITION poutros VALUES LESS THAN (MAXVALUE)";
        $partitions .= ');';
        DB::update($partitions);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('acessos');
    }
}
