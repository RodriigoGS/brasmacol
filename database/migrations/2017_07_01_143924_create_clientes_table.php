<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) 
        {
            $table->increments('id');

            $table->string('nome');
            $table->string('cpf', 20)->unique();
            $table->string('cnpj', 20)->unique();
            
            $table->string('telefone', 20);
            $table->string('email', 50);

            $table->char('status', 1)->default(1);
            $table->string('remember_token');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clientes');
    }
}
