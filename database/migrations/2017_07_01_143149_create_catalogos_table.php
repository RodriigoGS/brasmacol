<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalogos', function (Blueprint $table) 
        {
            $table->increments('id');

            $table->integer('idioma_id')->unsigned()->nullable();
            $table->foreign('idioma_id')->references('id')->on('idiomas');

            $table->string('nome');
            $table->string('catalogo');
            $table->string('thumb');
            $table->string('slug');
            
            $table->char('status', 1)->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('catalogos');
    }
}
