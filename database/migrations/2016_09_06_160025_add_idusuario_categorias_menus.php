<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdusuarioCategoriasMenus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('manager_categorias', function ($table){
            $table->integer('id_usuario')->unsigned()->nullable()->after('id');
            $table->foreign('id_usuario')->references('id')->on('manager_usuarios');
        });
        Schema::table('manager_menus', function ($table){
            $table->integer('id_usuario')->unsigned()->nullable()->after('pai');
            $table->foreign('id_usuario')->references('id')->on('manager_usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
