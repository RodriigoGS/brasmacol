<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produtos', function (Blueprint $table) 
        {
            $table->increments('id');

            $table->integer('galeria_id')->unsigned()->nullable();
            $table->foreign('galeria_id')->references('id')->on('galerias');

            $table->integer('idioma_id')->unsigned()->nullable();
            $table->foreign('idioma_id')->references('id')->on('idiomas');

            $table->string('titulo');
            $table->text('descricao')->nullable();
            $table->string('link')->nullable();
            $table->string('slug');
            $table->char('status', 1)->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('produtos');
    }
}
