<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePerfilMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manager_perfil_menu', function (Blueprint $table) {
            $table->integer('id_perfil')->unsigned();
            $table->foreign('id_perfil')->references('id')->on('manager_perfis');
            $table->integer('id_menu')->unsigned();
            $table->foreign('id_menu')->references('id')->on('manager_menus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('manager_perfil_menu');
    }
}
