@extends('layouts.app')

@section('content')
    <section class="content-header">
      <h1>
        <i class="fa fa-files-o"></i> Gerenciador de Arquivos
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-file-image-o"></i> Gerenciador de Arquivos</a></li>
        <li class="active">Listar</li>
      </ol>
      @if( Auth::user()->temRota('manager.arquivo.create') )        
        <a class="btn btn-block btn-primary btn-sm btn-adicionar" href="{{ url('manager/arquivo/create') }}">
          <i class="fa fa-edit"></i> Adicionar
        </a>         
      @endif
    </section>
   @if (session('status')) 
      <section class="content-header">
        <div class="alert alert-{{ session('status') }}">
            {{ session('retornomensagem') }}
        </div>
      </section>
    @endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">        
          <div class="box">

            <!-- /.box-header -->
            <div class="box-body">
               <table id="datatable" class="table table-bordered table-hover row-border">
                <thead>
                <tr>                                                      
                    <th>Arquivo</th>
                    <th>Título</th>
                    <th>Data</th>
                    <th>Status</th>                    
                    <th>Ações</th>                  
                </tr>
                </thead>
                <tbody>
                @foreach($arquivos as $arquivo)            
                <tr>                
                    <td>                  
                        <a class="btn btn-app no-margin" href="{{ asset('upload').'/'.$arquivo->arquivo }}" target="_blank">
                          <i class="fa {{ $controlador->faIcon($arquivo->extensao) }}"></i> Link
                        </a>                
                    </td>
                    <td>{{ $arquivo->titulo }}</td>
                    <td>{{ $arquivo->created_at->format('d/m/Y') }}</td>
                    <td><span class="badge {{ ($arquivo->status == '1' ? 'bg-green' : 'bg-yellow') }}">{{ $controlador->exibeStatus($arquivo->status) }}</span></td>                    
                    <td>
                        <a class="btn btn-app no-margin" href="{{ url('manager/arquivo/edit', $arquivo->id) }}">
                          <i class="fa fa-edit"></i> Editar
                        </a>
                        <a class="btn btn-app no-margin" href="{{ url('manager/arquivo/delete', $arquivo->id) }}">
                          <i class="fa fa-trash-o"></i> Excluir
                        </a>
                    </td>
                </tr>
                @endforeach                
                </tbody>               
              </table>              
            </div>
            <!-- /.box-body -->           
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
