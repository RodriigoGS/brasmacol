<div class="form-group">
  {!! Form::label('titulo', 'Título:') !!}
    <div class="input-group">
      {!! Form::text('titulo', null,  ['id' => 'titulo', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'Título']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>

<div class="form-group">
  {!! Form::label('nome', 'Arquivo inserido:') !!}                        
  <a href="{{ asset('upload').'/'.$arquivo->arquivo }}" target="_blank">Visualizar</a>
</div>

<div class="form-group">
	{!! Form::label('arquivo', 'Substituir por:') !!}
	{!! Form::file('arquivo') !!}
</div>

<div class="form-group">
	{!! Form::label('status', 'Status:') !!}	
	{!! Form::select('status', array(1 => 'Ativo', 2 => 'Inativo'), null, ['class' => 'form-control', 'required' => 'required']) !!}
</div>