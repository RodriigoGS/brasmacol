@extends('layouts.app')

@section('content')

<section class="content-header">
      <h1>
        <i class="fa fa-bar-chart"></i> Relatórios
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-bar-chart"></i> Relatórios</a></li>
        <li class="active">Acessos</li>
      </ol>
    </section>
   @if (session('status')) 
      <section class="content-header">
        <div class="alert alert-{{ session('status') }}">
            {{ session('retornomensagem') }}
        </div>
      </section>
    @endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">

          <div class="box box-info">
            <div class="box-header">
            <div class="box-header with-border">
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>              
              <h3 class="box-title">Page Views no Site</h3>
            </div>
            </div>
            <form name="filtroacessos" id="filtroacessos" class="form-horizontal">
              <div class="box-body">
                <div class="form-group">                

                  <label for="mesacesso" class="col-md-1 col-sm-2 control-label">Mês</label>
                  <div class="col-sm-2">
                    <select name="mesacesso" id="mesacesso" class="form-control">
                    @for($i=1; $i <= 12; $i++)
                      <option value="{{ $i }}"{{ (date('m') == $i? ' selected="selected"' : null) }}>{{ $i }}</option>
                    @endfor
                    </select>
                  </div>
                  <label for="anoacesso" class="col-md-1 col-sm-2 control-label">Ano</label>
                  <div class="col-sm-2">
                    <select name="anoacesso" id="anoacesso" class="form-control">
                    @for($i=2017; $i < 2056; $i++)
                      <option value="{{ $i }}"{{ (date('Y') == $i? ' selected="selected"' : null) }}>{{ $i }}</option>
                    @endfor
                    </select>
                  </div>

                  <label for="" class="col-md-1 col-sm-2 control-label"></label>
                  <div class="col-sm-2">
                    <button type="submit" class="btn btn-default enviarfiltroacessos"><i class="fa fa-sliders"></i> Atualizar</button>
                  </div>
                </div>                                 
              </div>                        
            </form>
            <script>
              $(document).ready(function(event) {
                $( "#filtroacessos" ).submit(function( event ) {                
                  var ano = $('select[name=anoacesso]').val();
                  var mes = $('select[name=mesacesso]').val();
                  $(".enviarfiltroacessos").html('<i class="fa fa-spinner fa-spin"></i> Atualizando');                

                  event.preventDefault();                  
                  $.ajax({
                    headers: {
                      'X-CSRF-Token': '{{ Session::getToken() }}'
                    },
                    type: "POST",
                    data: { mes: mes, ano: ano, ajax:'true' },
                    url: '{{ route('manager.grafico.acessos') }}',
                    success: function(data) {
                      var obj = jQuery.parseJSON(data);
                      areaChartData.datasets[0].data = obj;
                      barChart.Bar(barChartData, barChartOptions);
                      $(".enviarfiltroacessos").html('<i class="fa fa-spinner fa-sliders"></i> Atualizar');
                    },
                    error: function() {
                      $(".enviarfiltroacessos").html('<i class="fa fa-spinner fa-sliders"></i> Atualizar');
                    }
                    });
                  });
              });
            </script>
              <div class="box-body">
                <div class="chart">
                  <canvas width="488" height="229" id="barChart" style="height: 229px; width: 488px;"></canvas>
              </div>
            </div>
          </div>

          <div class="box box-warning">
            <div class="box-header">
            <div class="box-header with-border">
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>              
              <h3 class="box-title">Visitas Únicas</h3>
            </div>
            </div>
            <form name="filtroacessos" id="filtroacessosunicos" class="form-horizontal">
              <div class="box-body">
                <div class="form-group">                

                  <label for="mesacessounico" class="col-md-1 col-sm-2 control-label">Mês</label>
                  <div class="col-sm-2">
                    <select name="mesacessounico" id="mesacessounico" class="form-control">
                    @for($i=1; $i <= 12; $i++)
                      <option value="{{ $i }}"{{ (date('m') == $i? ' selected="selected"' : null) }}>{{ $i }}</option>
                    @endfor
                    </select>
                  </div>
                  <label for="anoacessounico" class="col-md-1 col-sm-2 control-label">Ano</label>
                  <div class="col-sm-2">
                    <select name="anoacessounico" id="anoacessounico" class="form-control">
                    @for($i=2017; $i < 2056; $i++)
                      <option value="{{ $i }}"{{ (date('Y') == $i? ' selected="selected"' : null) }}>{{ $i }}</option>
                    @endfor
                    </select>
                  </div>

                  <label for="" class="col-md-1 col-sm-2 control-label"></label>
                  <div class="col-sm-2">
                    <button type="submit" class="btn btn-default enviarfiltroacessosunicos"><i class="fa fa-sliders"></i> Atualizar</button>
                  </div>
                </div>                                 
              </div>                        
            </form>
            <script>
              $(document).ready(function(event) {
                $( "#filtroacessosunicos" ).submit(function( event ) {                
                  var ano = $('select[name=anoacessounico]').val();
                  var mes = $('select[name=mesacessounico]').val();
                  $(".enviarfiltroacessosunicos").html('<i class="fa fa-spinner fa-spin"></i> Atualizando');                

                  event.preventDefault();                  
                  $.ajax({
                    headers: {
                      'X-CSRF-Token': '{{ Session::getToken() }}'
                    },
                    type: "POST",
                    data: { mes: mes, ano: ano, ajax:'true' },
                    url: '{{ route('manager.relatorio.graficoacessosunicos') }}',
                    success: function(data) {                      
                      var obj = jQuery.parseJSON(data);
                      areaChartDataVU.datasets[0].data = obj;
                      barChartVU.Bar(barChartVUData, barChartVUOptions);
                      $(".enviarfiltroacessosunicos").html('<i class="fa fa-spinner fa-sliders"></i> Atualizar');
                    },
                    error: function() {
                      $(".enviarfiltroacessosunicos").html('<i class="fa fa-spinner fa-sliders"></i> Atualizar');
                    }
                    });
                  });
              });
            </script>
              <div class="box-body">
                <div class="chart">
                  <canvas width="488" height="229" id="barChartVU" style="height: 229px; width: 488px;"></canvas>
              </div>
            </div>
          </div>  
          <!-- /.box -->

          <div class="row">
            <div class="col-lg-6 col-xs-6">
              <div class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">Acessos por Navegador</h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-8">
                      <div class="chart-responsive">
                        <canvas id="pieChart" height="150"></canvas>
                      </div>
                      <!-- ./chart-responsive -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-4">
                      <ul class="chart-legend clearfix">
                        <li><i class="fa fa-circle-o text-orange"></i> FireFox </li>
                        <li><i class="fa fa-circle-o text-light-blue"></i> IE</li>
                        <li><i class="fa fa-circle-o text-yellow"></i> Chrome</li>
                        <li><i class="fa fa-circle-o text-aqua"></i> Safari</li>
                        <li><i class="fa fa-circle-o text-red"></i> Opera</li>
                        <li><i class="fa fa-circle-o text-gray"></i> Outros</li>
                      </ul>
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->
                </div>
                <!-- /.box-body -->               
              </div>            
              <!-- /.box -->     
            </div>

            <div class="col-lg-6 col-xs-6">
              <div class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">Acessos por Sistema Operacional</h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-8">
                      <div class="chart-responsive">
                        <canvas id="pieChart2" height="150"></canvas>
                      </div>
                      <!-- ./chart-responsive -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-4">
                      <ul class="chart-legend clearfix">
                        <li><i class="fa fa-circle-o text-orange"></i> Windows</li>
                        <li><i class="fa fa-circle-o text-light-blue"></i> Linux</li>
                        <li><i class="fa fa-circle-o text-yellow"></i> Android</li>
                        <li><i class="fa fa-circle-o text-aqua"></i> iPad</li>
                        <li><i class="fa fa-circle-o text-red"></i> Iphone</li>
                        <li><i class="fa fa-circle-o text-gray"></i> Outros</li>
                      </ul>
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->
                </div>
                <!-- /.box-body -->               
              </div>            
              <!-- /.box -->     
            </div>

          </div>  
  
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Acessos por Páginas</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
            <table class="table">
              <tbody>
                <tr>                    
                  <th>Página</th>
                  <th>Page Views</th>
                  <th>Acessos Únicos</th>                   
                </tr>
              <?php
                $totalPageViews = 0;
                $totalUnicas = 0;
              ?>
              @for($i=0; $i < count($porpagina); $i++)
                <tr>                    
                  <td>{{ $porpagina[$i]['caminho'] }}</td>
                  <td>{{ $porpagina[$i]['pageviews'] }}</td>
                  <td>{{ $porpagina[$i]['acessosunicos'] }}</td>                   
                </tr>
                <?php
                  $totalPageViews += $porpagina[$i]['pageviews'];
                  $totalUnicas += $porpagina[$i]['acessosunicos'];
                ?>
              @endfor
                <tr>                    
                  <td><strong>Total</strong></td>
                  <td>{{ $totalPageViews }}</td>
                  <td>{{ $totalUnicas }}</td>                   
                </tr>       
              </tbody>
            </table>
          </div>
            <!-- /.box-body -->               
          </div>            
          <!-- /.box -->     

        </div>
         
      </div>    
      <!-- /.row -->

    </section>
    <!-- /.content -->
@endsection