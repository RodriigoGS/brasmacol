<div class="form-group">
  {!! Form::label('ano', 'Ano:') !!}
    <div class="input-group">
      {!! Form::number('ano', $empresa->ano,  ['id' => 'ano', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'Ano']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
    {!! Form::label('descricao', 'Descrição:') !!}
    {!! Form::textarea('descricao', $empresa->descricao,  ['id' => 'editor1', 'class' => 'form-control']) !!}
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('idioma_id', 'Status:') !!}
  {!! Form::select('idioma_id', config('location.model'), null, ['class' => 'form-control', 'required' => 'required']) !!}
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('status', 'Status:') !!}
  {!! Form::select('status', array(1 => 'Ativo', 2 => 'Inativo'), $empresa->status, ['class' => 'form-control', 'required' => 'required']) !!}
</div>
<!-- /.form group -->

<script>
$( document ).ready(function() {  
    CKEDITOR.basePath = '{{ asset('assets/plugins/ckeditor/').'/' }}';
    CKEDITOR.plugins.basePath = '{{ asset('assets/plugins/ckeditor/plugins/') }}' + '/';
    CKEDITOR.replace( 'editor1', {
        customConfig: '{{ asset('assets/plugins/ckeditor/config.js') }}'
    });         
});
</script>