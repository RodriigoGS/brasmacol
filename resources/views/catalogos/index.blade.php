@extends('layouts.app')

@section('content')
    <section class="content-header">
      <h1>
        <i class="fa fa-file-pdf-o"></i> Catalogos
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-file-pdf-o"></i> Catalogos</a></li>
        <li class="active">Listar</li>
      </ol>
      @if( Auth::user()->temRota('manager.catalogos.create') )        
        <a class="btn btn-block btn-primary btn-sm btn-adicionar" href="{{ url('manager/catalogos/create') }}">
          <i class="fa fa-edit"></i> Adicionar
        </a>         
      @endif
    </section>
    @if(session('status')) 
      <section class="content-header">
        <div class="alert alert-{{ session('status') }}">
            {{ session('retornomensagem') }}
        </div>
      </section>
    @endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">        
          <div class="box">

            <!-- /.box-header -->
            <div class="box-body">
               <table id="datatable" class="table table-bordered table-hover row-border">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Nome</th>
                  <th>Idioma</th>
                  <th>Gerado em</th>
                  <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                @foreach($catalogos as $catalogo)            
                <tr>
                  <td>{{ $catalogo->id }}</td>
                  <td>{{ $catalogo->nome }}</td>
                  <td>{{ config('location.model')[$catalogo->idioma_id] }}</td>
                  <td>{{ $catalogo->created_at->format('d/m/Y H:i:s') }}</td>
                  <td>                      
                      @if(Auth::user()->id_perfil == 1)
                        <a class="btn btn-app no-margin" href="{{ url('manager/catalogos/edit', $catalogo->id) }}">
                          <i class="fa fa-edit"></i> Editar
                        </a>
                      @endif
                      <a class="btn btn-app no-margin" href="{{ url('manager/catalogos/delete', $catalogo->id) }}">
                        <i class="fa fa-trash-o"></i> Excluir
                      </a>
                  </td>
                </tr>
                @endforeach                
                </tbody>               
              </table>              
            </div>
            <!-- /.box-body -->           
            </div>
          </div>
          <!-- /.box -->
        </div>      
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
