<div class="form-group">
  {!! Form::label('nome', 'Nome:') !!}
    <div class="input-group">
      {!! Form::text('nome', $catalogo->nome,  ['id' => 'nome', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'Nome do catálogo']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
    {!! Form::label('catalogo', 'Catálogo:') !!}
    <div class="input-file-fake">
      <label for="catalogo" class="thumb-text">{{ $catalogo->catalogo }}</label>
      <span class="thumb-img"></span>
      {!! Form::file('catalogo', null,  ['id' => 'catalogo', 'class' => 'form-control input-file js-input-file']) !!}
    </div>
    <!-- /.input-fake -->
</div>
<!-- /.form group -->

<div class="form-group">
    {!! Form::label('thumb', 'Thumbnail:') !!}
    <div class="input-file-fake">
      <label for="thumb" class="thumb-text">{{ $catalogo->thumb }}</label>
      <span class="thumb-img" style="background-image: url(' {{ asset('upload/catalogo/' . $catalogo->thumb)  }} ')"></span>
      {!! Form::file('thumb', null,  ['id' => 'thumb', 'class' => 'form-control input-file js-input-file']) !!}
    </div>
    <!-- /.input-fake -->
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('slug', 'URL:') !!}
    <div class="input-group">
      {!! Form::text('slug', $catalogo->slug,  ['id' => 'slug', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'URL do catálogo']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('idioma_id', 'Idioma:') !!}
  {!! Form::select('idioma_id', config('location.model'), $catalogo->idioma_id, ['class' => 'form-control', 'required' => 'required']) !!}
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('status', 'Status:') !!}
  {!! Form::select('status', array(1 => 'Ativo', 2 => 'Inativo'), $catalogo->status, ['class' => 'form-control', 'required' => 'required']) !!}
</div>
<!-- /.form group -->