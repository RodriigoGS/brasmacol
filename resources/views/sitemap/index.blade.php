@extends('layouts.app')

@section('content')
    <section class="content-header">
      <h1>
        <i class="fa fa-map-o"></i> SiteMap
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-map-o"></i> SiteMap</a></li>
        <li class="active">Listar</li>
      </ol>
      @if( Auth::user()->temRota('manager.sitemap.create') )        
        <a class="btn btn-block btn-primary btn-sm btn-adicionar" href="{{ url('manager/sitemap/create') }}">
          <i class="fa fa-edit"></i> Adicionar
        </a>         
      @endif
    </section>
   @if (session('status')) 
      <section class="content-header">
        <div class="alert alert-{{ session('status') }}">
            {{ session('retornomensagem') }}
        </div>
      </section>
    @endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">        
          <div class="box">

            <!-- /.box-header -->
            <div class="box-body">
               <table id="datatable" class="table table-bordered table-hover row-border">
                <thead>
                <tr>
                  <th>ID</th>
                    <th>localização</th>
                    <th>Frequência de Mudança</th>
                    <th>Prioridade</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                @foreach($sitemaps as $sitemap)            
                <tr>
                    <td>{{ $sitemap->id }}</td>
                    <td>{{ $sitemap->loc }}</td>
                    <td>{{ $sitemap->changefreq }}</td>
                    <td>{{ $sitemap->priority }}</td>
                    <td>
                        <a class="btn btn-app no-margin" href="{{ url('manager/sitemap/edit', $sitemap->id) }}">
                          <i class="fa fa-edit"></i> Editar
                        </a>
                        <a class="btn btn-app no-margin" href="{{ url('manager/sitemap/destroy', $sitemap->id) }}">
                          <i class="fa fa-trash-o"></i> Excluir
                        </a>
                    </td>
                </tr>
                @endforeach                
                </tbody>               
              </table>              
            </div>
            <!-- /.box-body -->           
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
