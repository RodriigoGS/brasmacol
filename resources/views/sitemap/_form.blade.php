<div class="form-group">
  {!! Form::label('loc', 'Localização:') !!}
    <div class="input-group">
      {!! Form::text('loc', null,  ['id' => 'loc', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'URL']) !!}
      <div class="input-group-addon">
        <i class="fa fa-link"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('arquivo', 'Arquivo:') !!}
    <div class="input-group">
      {!! Form::text('arquivo', null,  ['id' => 'arquivo', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'Arquivo']) !!}
      <div class="input-group-addon">
        <i class="fa fa-file"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
	{!! Form::label('changefreq', 'Frequência:') !!}	
	{!! Form::select('changefreq', $changefreq, null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('priority', 'Prioridade:') !!}	
	{!! Form::select('priority', $priority, null, ['class' => 'form-control']) !!}
</div>
