<div class="form-group">
  {!! Form::label('descricao', 'Título:') !!}
    <div class="input-group">
      {!! Form::text('descricao', null,  ['id' => 'descricao', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'Título']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->
<div class="form-group">
	{!! Form::label('status', 'Status:') !!}	
	{!! Form::select('status', array(1 => 'Ativo', 2 => 'Inativo'), (isset($galeria->status)? $galeria->status : 1), ['class' => 'form-control', 'required' => 'required']) !!}
</div>