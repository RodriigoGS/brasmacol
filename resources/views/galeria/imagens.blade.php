@if( !empty($imagens) )                        
    @foreach($imagens as $imagem)
        <div class="col-md-3 col-sm-3 col-xs-6 no-padding">
            <a href="{{ asset('upload').'/'.$imagem->imagem }}" target="_blank"><img src="{{ asset('upload').'/'.$imagem->imagem }}"  alt="Designio Projetos Criativos" class="img-responsive pad"></a>                            
            <a href="{{ route('manager.galeria.imagemexcluir', ['id_galeria' => $imagem->id_galeria, 'id' =>$imagem->id]) }}" id="botaoexcluir" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
        </div>                
    @endforeach
@endif