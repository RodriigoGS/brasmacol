@extends('layouts.app')

@section('content')
    <section class="content-header">
      <h1>
        <i class="fa fa-picture-o"></i> Galerias
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-picture-o"></i> Galerias</a></li>
        <li class="active">Listar</li>
      </ol>
      @if( Auth::user()->temRota('manager.galeria.create') )        
        <a class="btn btn-block btn-primary btn-sm btn-adicionar" href="{{ url('manager/galeria/create') }}">
          <i class="fa fa-edit"></i> Adicionar
        </a>         
      @endif
    </section>
   @if (session('status')) 
      <section class="content-header">
        <div class="alert alert-{{ session('status') }}">
            {{ session('retornomensagem') }}
        </div>
      </section>
    @endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">        
          <div class="box">

            <!-- /.box-header -->
            <div class="box-body">
               <table id="datatable" class="table table-bordered table-hover row-border">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Item</th>
                  <th>Status</th>
                  <th>Ações</th>                  
                </tr>
                </thead>
                <tbody>
                @foreach($galerias as $galeria)            
                <tr>
                    <td>{{ $galeria->id }}</td>
                    <td>{{ $galeria->descricao }}</td>
                    <td><span class="badge {{ ($galeria->status == '1' ? 'bg-green' : 'bg-yellow') }}">{{ $controlador->exibeStatus($galeria->status) }}</span></td>                    
                    <td>
                        <a class="btn btn-app no-margin" href="{{ url('manager/galeria/edit', $galeria->id) }}">
                          <i class="fa fa-edit"></i> Editar
                        </a>
                        <a class="btn btn-app no-margin" href="{{ url('manager/galeria/delete', $galeria->id) }}">
                          <i class="fa fa-trash-o"></i> Excluir
                        </a>
                    </td>
                </tr>
                @endforeach                
                </tbody>               
              </table>              
            </div>
            <!-- /.box-body -->           
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection