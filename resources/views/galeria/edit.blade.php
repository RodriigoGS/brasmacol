@extends('layouts.app')

@section('content')
<style>
    #botaoexcluir{
        position: absolute;
        top: 5px;
        right: 5px;
        z-index: 100;
    }
</style>

    <section class="content-header">
      <h1>
        <i class="fa fa-picture-o"></i> Editando Galeria        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-picture-o"></i> Editando Galeria</a></li>
        <li class="active">Editar</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
              <h3 class="box-title"><i class="fa fa-edit"></i> Editar</h3>
            </div>
            <div class="box-body">
                    
                @if (session('status')) 
                    <div class="alert alert-{{ session('status') }}">
                        {{ session('retornomensagem') }}
                    </div>
                @endif
                
                    @if($errors->any())
                        <ul class="alert">
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                {!! Form::model($galeria, ['route' => ['manager.galeria.update', $galeria->id], 'method' => 'put']) !!}                            


                    @include('galeria._form')                               
                  

                    <div class="box-footer">
                      <div class="box-tools pull-right">
                        <a class="btn btn-app btn-flat" href="{{ route('manager.galeria.index') }}">
                          <i class="fa fa-reply"></i> Voltar
                        </a>
                        <button type="submit" class="btn btn-app bg-green btn-flat">
                          <i class="fa fa-save"></i> Salvar
                        </button>
                      </div>
                    </div>

                {!! Form::close() !!}

            </div>            
          </div>
        </div>
        <div class="col-md-3">
          <div class="box box-danger">
            <div class="box-header">
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
              <h3 class="box-title"><i class="fa fa-info"></i> Informações</h3>
            </div>
            <div class="box-body no-padding">
              <table class="table table-bordered">
                <tr>
                  <td><big>Adicionado em:</big></td>
                  <td>{{ $galeria->created_at->format('d/m/Y H:i:s') }}</td>
                </tr>
                <tr>
                  <td><big>Alterado em:</big></td>
                  <td>{{ $galeria->updated_at->format('d/m/Y H:i:s') }}</td>
                </tr>
                <tr>
                  <td><big>Alterado por:</big></td>
                  <td>{{ $usuarioCadastro->nome }}</td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-md-8">
          <div class="box box-primary">
            <div class="box-header width-border">
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
              <h3 class="box-title"><i class="fa fa-camera-retro"></i> Galeria de Imagens</h3>
            </div>
            <div id="galeria" class="box-body">
               @if( !empty($imagens) )                                                        
                    @foreach($imagens as $imagem)
                        <div class="col-md-3 col-sm-3 col-xs-6 no-padding">
                            <a href="{{ asset('upload').'/'.$imagem->imagem }}" target="_blank"><img src="{{ asset('upload').'/'.$imagem->imagem }}"  alt="Designio Projetos Criativos" class="img-responsive pad"></a>                            
                            <a href="{{ route('manager.galeria.imagemexcluir', ['id_galeria' => $imagem->id_galeria, 'id' =>$imagem->id]) }}" class="btn btn-danger btn-sm" id="botaoexcluir"><i class="fa fa-trash"></i></a>
                        </div>                
                    @endforeach
                @endif                
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="box box-danger">
            <div class="box-header width-border">              
              <h3 class="box-title"><i class="fa fa-upload"></i> Enviar imagem</h3>
              <div id="dropzone" class="dropzone-projeto dz-clickable dz-started" style="width:100%;margin-bottom: 20px;">
                    Clique aqui ou arraste uma imagem para enviar</h5>
                </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </section>
<script>
    $(document).ready(function(){

        if($("div.dropzone-projeto").length > 0){
            // DROPZONE
            var baseUrl = "{{ route('manager.galeria.upload') }}";
            var token = "{{ Session::getToken() }}";
            Dropzone.autoDiscover = false;
             var myDropzone = new Dropzone("div.dropzone-projeto", {
                 url: baseUrl,
                 params: {
                    _token: token,
                    id_galeria: {{ $galeria->id }}
                  }
             });
             Dropzone.options.myAwesomeDropzone = {
                paramName: "file", // The name that will be used to transfer the file
                maxFilesize: 6, // MB
                maxFiles: 12,
                addRemoveLinks: true,
                acceptedFiles: "image/*",
                dictMaxFilesExceeded: "Fila de arquivos não pode ser maior que 12 imagens",
                dictFallbackMessage: "Seu navegador não suporta plugins de múltiplo uploads",
                dictInvalidFileType: "Extensão de arquivo inválido",
                dictFileTooBig: "Imagem precisa ser menor que 6Mb",              
              };
            

            myDropzone.on("sending", function(file, xhr, formData) {
                formData.append("id", $("#id").val());    
            });         

            myDropzone.on("complete", function(file) {

                $('.dz-preview').fadeOut(400);

                var id = {{ $galeria->id }};

                $.ajax({
                    headers: {
                        'X-CSRF-Token': "{{ Session::getToken() }}"
                    },
                    type: "POST",
                    data: { id:id, ajax:'true' },
                    url: "{{ route('manager.galeria.imagens') }}",
                    success: function(data) {
                            $('#galeria').html(data);
                    }
                });
            });
        }
    });
</script>
@endsection