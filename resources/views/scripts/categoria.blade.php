<script type="text/javascript">
$('body').on('click', '.op_add', function (event){ 
  event.preventDefault();

  var opcaovalue = $("#nova_cat").val(); 
  if(opcaovalue == "") return false;

  $.ajax({
    headers: {
            'X-CSRF-Token': "{{ Session::getToken() }}"
        },
        type: "POST",
        data: { titulo: opcaovalue, tipo: '{{ $tipo }}', request: '{{ $request }}', id: {{ $id }}, status: 1, ajax:'true' },
        url: $(this).attr('href'),      
        success: function(data) {
          $("#listagemcategorias").html(data);   
          $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue'
          });                 
        }
  });
});
</script>