<div class="form-group">
	{!! Form::label('nome', 'Nome:') !!}
	{!! Form::text('nome', null,  ['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('email', 'E-mail:') !!}
	{!! Form::email('email', null,  ['class' => 'form-control', 'required' => 'required']) !!}
</div>
<div class="form-group">
	{!! Form::label('status', 'Status:') !!}	
	{!! Form::select('status', array(1 => 'Ativo', 2 => 'Inativo'), null, ['class' => 'form-control', 'required' => 'required']) !!}
</div>
