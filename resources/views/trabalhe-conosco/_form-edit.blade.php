<div class="form-group">
  {!! Form::label('nome', 'Nome:') !!}
    <div class="input-group">
      {!! Form::text('nome', $trabalheConosco->nome,  ['id' => 'nome', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'Nome']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('telefone', 'Telefone:') !!}
    <div class="input-group">
      {!! Form::text('telefone', $trabalheConosco->telefone,  ['id' => 'telefone', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'Telefone']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('email', 'E-mail:') !!}
    <div class="input-group">
      {!! Form::text('email', $trabalheConosco->email,  ['id' => 'email', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'E-mail']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
    {!! Form::label('curriculo', 'Curriculo:') !!}
    <div class="input-file-fake">
      <label for="curriculo" class="thumb-text">{{ $trabalheConosco->curriculo }}</label>
      <span class="thumb-img"></span>
      {!! Form::file('curriculo', null,  ['id' => 'curriculo', 'class' => 'form-control input-file js-input-file', 'required' => 'required']) !!}
    </div>
    <!-- /.input-fake -->
</div>
<!-- /.form group -->

<div class="form-group">
    {!! Form::label('mensagem', 'Mensagem:') !!}
    {!! Form::textarea('mensagem', $trabalheConosco->mensagem,  ['id' => 'editor1', 'class' => 'form-control']) !!}
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('status', 'Status:') !!}
  {!! Form::select('status', array(1 => 'Ativo', 2 => 'Inativo'), $trabalheConosco->status, ['class' => 'form-control', 'required' => 'required']) !!}
</div>
<!-- /.form group -->

<script>
$( document ).ready(function() {  
    CKEDITOR.basePath = '{{ asset('assets/plugins/ckeditor/').'/' }}';
    CKEDITOR.plugins.basePath = '{{ asset('assets/plugins/ckeditor/plugins/') }}' + '/';
    CKEDITOR.replace( 'editor1', {
        customConfig: '{{ asset('assets/plugins/ckeditor/config.js') }}'
    });         
});
</script>