<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title> Manager </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-select/bootstrap-select.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('assets/dist/css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('assets/dist/css/skins/_all-skins.min.css') }}">
  <!-- Dropzone -->
  <link rel="stylesheet" href="{{ asset('assets/dist/css/dropzone.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset('assets/plugins/iCheck/flat/blue.css') }}">
  <!-- Morris chart -->
  <link rel="stylesheet" href="{{ asset('assets/plugins/morris/morris.css') }}">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{ asset('assets/plugins/datepicker/datepicker3.css') }}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('assets/plugins/daterangepicker/daterangepicker.css') }}">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
  <!-- DateTable -->
  <link rel="stylesheet" href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}">
  <!-- Timepicker -->
  <link rel="stylesheet" href="{{ asset('assets/plugins/timepicker/bootstrap-timepicker.min.css') }}">  
  <link rel="stylesheet" href="{{ asset('assets/plugins/select2/select2.min.css') }}">
  <!-- STYLE -->
  <link rel="stylesheet" href="{{ asset('assets/dist/css/style.css') }}">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- jQuery 2.2.3 -->
<script src="{{ asset('assets/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>

<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="{{ url('manager') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">Manager</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
        @if(isset($mensagemContato))
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu" id="mensagens">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              @if($mensagemContato->nao_lidos > 0)
                <span class="label label-danger">{{ $mensagemContato->nao_lidos }}</span>
              @endif              
            </a>
            <ul class="dropdown-menu">
              @if($mensagemContato->nao_lidos > 1)
                <li class="header">{{ $mensagemContato->nao_lidos }} novas mensagens</li>
              @elseif($mensagemContato->nao_lidos == 1)
                <li class="header">{{ $mensagemContato->nao_lidos }} nova mensagem</li>
              @else
                <li class="header">Sem novas mensagens</li>
              @endif
            @if($mensagemContato->nao_lidos > 0)
                <li>
                  <ul class="menu">
                  <!-- inner menu: contains the actual data -->
              @foreach($mensagens as $mensagem)                  
              <?php                      
                  $dataInicio = new DateTime($mensagem->created_at);
                  $dataFim = $dataInicio->diff(new DateTime('now'));                  
                  $minutos = $dataFim->days * 24 * 60;
                  $minutos += $dataFim->h * 60;                  
                  $minutos += $dataFim->i;
                  $horas = floor($minutos / 60);                  
              ?>
                    <li><!-- start message -->
                      <a href="{{ url('manager/contato/show', $mensagem->id) }}">
                        <h4 class="oxy-margin-0">
                          {{ $mensagem->nome }}
                          <small><i class="fa fa-clock-o"></i> {{ ($minutos <= 60)? $minutos. ' mins' : $horas.' horas' }}</small>
                        </h4>
                      </a>
                    </li>
                    <!-- end message -->                  
              @endforeach                
                  </ul>
                </li>
            @endif                
              </ul>
            </li>
        @endif

          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{ asset('upload').'/'.Auth::user()->imagem }}" class="user-image" alt="User Image">
              <span class="hidden-xs">{{ Auth::user()->nome }}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="{{ asset('upload').'/'.Auth::user()->imagem }}" class="img-circle" alt="User Image">

                <p>
                  {{ Auth::user()->nome }}
                  <small>{{ Auth::user()->email }}</small>
                </p>
              </li>
              <!-- Menu Body -->
              {{-- <li class="user-body">               
                <!-- /.row -->
              </li> --}}
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="{{ route('manager.usuario.useredit', Auth::user()->id) }}" class="btn btn-default btn-flat">Minha Conta</a>
                </div>
                <div class="pull-right">
                  <a href="{{ url('/auth/logout') }}" class="btn btn-default btn-flat">Sair</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
      @include('layouts.asideHeader')
    <!-- /.sidebar -->
  </aside>
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
      @yield('content', 'Corpo Principal')
    <!-- /.content -->
  </div>

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Versão</b> 2.0
    </div>
    <strong><a href="http://projetodesignio.com.br">Designio</a>.</strong> Todos os direitos reservados
  </footer>
  
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    @include('layouts.asideFooter')
  </aside>
  <!-- /.control-sidebar -->
  
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- Latest compiled and minified JavaScript -->
<script src="{{ asset('assets/plugins/bootstrap-select/bootstrap-select.min.js') }}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- Slimscroll -->
<script src="{{ asset('assets/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('assets/plugins/select2/select2.full.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/dist/js/app.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('assets/dist/js/demo.js') }}"></script>
<script src="{{ asset('assets/js/dropzone.js') }}"></script>
<script src="{{ asset('assets/plugins/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/plugins/datepicker/locales/bootstrap-datepicker.pt-BR.js') }}"></script>
<script src="{{ asset('assets/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('assets/plugins/input-mask/jquery.inputmask.money.min.js') }}"></script>
<script src="{{ asset('assets/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script src="{{ asset('assets/plugins/input-mask/jquery.inputmask.phone.extensions.js') }}"></script>
<script src="{{ asset('assets/plugins/iCheck/icheck.min.js') }}"></script>
<!-- ChartJS 1.0.1 -->
<script src="{{ asset('assets/plugins/chartjs/Chart.min.js') }}"></script>

{{-- SCRIPTS --}}
<script src="{{ asset('assets/dist/js/scripts.js') }}"></script>

<script>
var barChartOptions;
var areaChartData;
var barChart;

var barChartVUOptions;
var areaChartDataVU;
var barChartVU;

//piehcarts dos relatorios de acessos por navegador e OS
var pieChartCanvas;
var pieChart;
var PieData;
var pieChartCanvas2;
var pieChart2
var PieData2;
$(function () {   
    //Datatable 
    var datatable = $('#datatable').DataTable({
        "paging": true,
        "lengthChange": true,
        "bLengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "iDisplayLength": 10,
        "aLengthMenu": [[5, 10, 25, 50, 200, 500], [5, 10, 25, 50, 200, 500]],
        "language": {
            "url": "{{ url('assets/plugins/datatables/traducao.json') }}"
        }
    });  
    $('#search').keyup(function(){
      datatable.search($(this).val()).draw() ;
    });   
    //Timepicker
    $(".timepicker").timepicker({
      showInputs: false,
      showMeridian: false,
      showSeconds: true,
      minuteStep: 2,
    });
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      language: "pt-BR",
      format: 'dd/mm/yyyy',
    });

    $("#cpf").inputmask({"mask": "999.999.999-99"});
    $("#phone").inputmask({"mask": "[9][9](99) 9999-9999[9]"});
    $("#cep").inputmask({"mask": "99999-999"});
    
    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass: 'iradio_flat-blue'
    });

    $(".select2").select2();
  @if(Auth::guard('web')->check())
    setInterval(function() {
      $.ajax({        
          headers: {
              'X-CSRF-Token': "{{ Session::getToken() }}"
          },
          type: "POST", 
          data: { ajax:'true' },
          url: "{{ route('manager.contato.atualizacao') }}",
          success: function(data) {            
            $('#mensagens').html(data);
          }
      });
    }, 60000);
  @endif

  // Get context with jQuery - using jQuery's .get() method.
    var areaChartCanvas = $("#barChart").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    areaChart = new Chart(areaChartCanvas);    

    areaChartData = {
      labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14","15",
               "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", 
               "29","30", "31"
      ],
      datasets: [
        {
          label: "Visitas",
          fillColor: "rgba(210, 214, 222, 1)",
          strokeColor: "rgba(210, 214, 222, 1)",
          pointColor: "rgba(210, 214, 222, 1)",
          pointStrokeColor: "#c1c7d1",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: []
        }    
      ]
    };

    $.ajax({
      headers: {
        'X-CSRF-Token': '{{ Session::getToken() }}'
      },
      type: "POST",
      data: { mes: (new Date().getMonth())+1, ano: new Date().getFullYear(), ajax:'true' },
      url: '{{ route('manager.grafico.acessos') }}',
      success: function(data) {
        var obj = jQuery.parseJSON(data);        
        areaChartData.datasets[0].data = obj;
        barChart.Bar(barChartData, barChartOptions);       
      },
      error: function() {        
      }
    });

    barChartOptions = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: true,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - If there is a stroke on each bar
      barShowStroke: true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth: 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing: 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing: 1,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to make the chart responsive
      responsive: true,
      maintainAspectRatio: true
    };

    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas = $("#barChart").get(0).getContext("2d");
    barChart = new Chart(barChartCanvas);
    //barChart.Line(areaChartData, barChartOptions);
    barChartData = areaChartData;
    barChartData.datasets[0].fillColor = "#00a65a";
    barChartData.datasets[0].strokeColor = "#00a65a";
    barChartData.datasets[0].pointColor = "#00a65a";
    barChartOptions.datasetFill = false;
    barChart.Bar(barChartData, barChartOptions);

    //BAR CHART - Visitas Unicas    
    var areaChartCanvas = $("#barChartVU").get(0).getContext("2d");    
    areaChart = new Chart(areaChartCanvas);    

    areaChartDataVU = {
      labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14","15",
               "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", 
               "29","30", "31"
      ],
      datasets: [
        {
          label: "Visitas",
          fillColor: "rgba(210, 214, 222, 1)",
          strokeColor: "rgba(210, 214, 222, 1)",
          pointColor: "rgba(210, 214, 222, 1)",
          pointStrokeColor: "#c1c7d1",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: []
        }    
      ]
    };

    $.ajax({
      headers: {
        'X-CSRF-Token': '{{ Session::getToken() }}'
      },
      type: "POST",
      data: { mes: (new Date().getMonth())+1, ano: new Date().getFullYear(), ajax:'true' },
      url: '{{ route('manager.relatorio.graficoacessosunicos') }}',
      success: function(data) {
        var obj = jQuery.parseJSON(data);        
        areaChartDataVU.datasets[0].data = obj;
        barChartVU.Bar(barChartVUData, barChartVUOptions);       
      },
      error: function() {        
      }
    });

    barChartVUOptions = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: true,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - If there is a stroke on each bar
      barShowStroke: true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth: 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing: 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing: 1,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to make the chart responsive
      responsive: true,
      maintainAspectRatio: true
    };

    //-------------
    //- BAR CHART -
    //-------------
    var barChartVUCanvas = $("#barChartVU").get(0).getContext("2d");
    barChartVU = new Chart(barChartVUCanvas);    
    barChartVUData = areaChartDataVU;
    barChartVUData.datasets[0].fillColor = "#f39c12";
    barChartVUData.datasets[0].strokeColor = "#f39c12";
    barChartVUData.datasets[0].pointColor = "#f39c12";
    barChartVUOptions.datasetFill = false;
    barChartVU.Bar(barChartVUData, barChartVUOptions);


    //-------------
    //- PIE CHART - Acessos navegadores
    //-------------    
    pieChartCanvas = $("#pieChart").get(0).getContext("2d");
    pieChart = new Chart(pieChartCanvas);
    PieData = [
      {
        value: 0,
        color: "#ff7605",
        highlight: "#ff7605",
        label: "FireFox"
      },
      {
        value: 0,
        color: "#3c8dbc",
        highlight: "#3c8dbc",
        label: "IE"
      }, 
      {
        value: 0,
        color: "#f39c12",
        highlight: "#f39c12",
        label: "Chrome"
      },
      {
        value: 0,
        color: "#00c0ef",
        highlight: "#00c0ef",
        label: "Safari"
      },
      {
        value: 0,
        color: "#dd4b39",
        highlight: "#dd4b39",
        label: "Opera"
      },           
      {
        value: 0,
        color: "#d2d6de",
        highlight: "#d2d6de",
        label: "Outros"
      }
    ];


    var pieOptions = {
      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke: true,
      //String - The colour of each segment stroke
      segmentStrokeColor: "#fff",
      //Number - The width of each segment stroke
      segmentStrokeWidth: 2,
      //Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout: 50, // This is 0 for Pie charts
      //Number - Amount of animation steps
      animationSteps: 100,
      //String - Animation easing effect
      animationEasing: "easeOutBounce",
      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate: true,
      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale: false,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true,
      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
    };
    
    $.ajax({
      headers: {
        'X-CSRF-Token': '{{ Session::getToken() }}'
      },
      type: "POST",
      data: { ajax:'true' },
      url: '{{ route('manager.relatorio.graficonavegador') }}',
      success: function(data) {
        var obj = jQuery.parseJSON(data);
        PieData[0].value = obj.Firefox;
        PieData[1].value = obj.IE;
        PieData[2].value = obj.Chrome;
        PieData[3].value = obj.Safari;
        PieData[4].value = obj.Opera;
        PieData[5].value = obj.Outros;
        pieChart.Doughnut(PieData, pieOptions);
       
        console.log(PieData[0].value);
      },
      error: function() {        
      }
    });    
    
    //-------------
    //- PIE CHART - Acessos sistema operacional
    //-------------    
    pieChartCanvas2 = $("#pieChart2").get(0).getContext("2d");
    pieChart2 = new Chart(pieChartCanvas2);
    PieData2 = [
      {
        value: 0,
        color: "#ff7605",
        highlight: "#ff7605",
        label: "Windows"
      },
      {
        value: 0,
        color: "#3c8dbc",
        highlight: "#3c8dbc",
        label: "Linux"
      },
      {
        value: 0,
        color: "#f39c12",
        highlight: "#f39c12",
        label: "Android"
      },
      {
        value: 0,
        color: "#00c0ef",
        highlight: "#00c0ef",
        label: "iPad"
      },
      {
        value: 0,
        color: "#dd4b39",
        highlight: "#dd4b39",
        label: "iPhone"
      },
      {
        value: 0,
        color: "#d2d6de",
        highlight: "#d2d6de",
        label: "Outros"
      }
    ];    
    
    $.ajax({
      headers: {
        'X-CSRF-Token': '{{ Session::getToken() }}'
      },
      type: "POST",
      data: { ajax:'true' },
      url: '{{ route('manager.relatorio.graficoporsistema') }}',
      success: function(data) {
        var obj = jQuery.parseJSON(data);
        console.log(obj);
        PieData2[0].value = obj.Windows;
        PieData2[1].value = obj.Linux;
        PieData2[2].value = obj.Android;
        PieData2[3].value = obj.iPad;
        PieData2[4].value = obj.iPhone;
        PieData2[5].value = obj.Outros;
        pieChart2.Doughnut(PieData2, pieOptions);
               
      },
      error: function() {        
      }
    });
});
</script>
</body>
</html>