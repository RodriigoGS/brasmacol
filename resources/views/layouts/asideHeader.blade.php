<section class="sidebar">
  <!-- Sidebar user panel -->
  <div class="user-panel">
    <div class="pull-left image">      
      @if(Auth::guest()) <img src="{{ asset('assets/dist/img/designio.png') }}" class="img-circle" alt="User Image"> @endif
      @if(!Auth::guest()) <img src="{{ asset('upload').'/'.Auth::user()->imagem }}" class="img-circle" alt="{{ Auth::user()->nome }}"> @endif
    </div>
    <div class="pull-left info">
      <p>{{ Auth::user()->nome }}</p>
      <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
  </div>
  <!-- sidebar menu: : style can be found in sidebar.less -->
  <ul class="sidebar-menu">
    <li class="header">Menu Principal</li>
    <li><a href="{{ url('manager') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
    @if(!Auth::guest())
      @for($i=0; $i < count($menusacesso); $i++)
       <?php $menuitem = $menusacesso[$i]; ?>

          @if( ($menuitem->pai == null) && ($menuitem->rota != null) )
                    <li><a href="{{ route($menuitem->rota) }}"><i class="fa {{ $menuitem->icone }}"></i> <span>{{ $menuitem->descricao }}</span></a></li>                    
          @endif
          @if( ($menuitem->pai == null) && ($menuitem->rota == null) )
            <li class="treeview">
              <a href="#">
                <i class="fa {{ $menuitem->icone }}"></i> <span>{{ $menuitem->descricao }}</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
          @endif            
          @if( ($menuitem->pai != null) && ($menuitem->rota == null) ) 
              <ul class="treeview-menu">
                <li>
                  <a href="#"><i class="fa {{ $menuitem->icone }}"></i> <span>{{ $menuitem->descricao }}</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
          @endif
          @if( ($menuitem->pai != null) && ($menuitem->rota != null) )
                    <li><a href="{{ route($menuitem->rota) }}"><i class="fa {{ $menuitem->icone }}"></i> {{ $menuitem->descricao }}</a></li>
          @endif
          @if(isset($menusacesso[$i+1]))
              <?php $proximoItem = $menusacesso[$i+1]; //para verificar proximos itens de menu a fim de fechar tags corretamente 
              ?>               
              @if( ($proximoItem->pai != null) && ($proximoItem->rota == null) )
                @if( ($menuitem->pai != null) && ($menuitem->rota != null) ) 
                  </ul>
                </li>
              </ul>
                @endif
              @endif
              @if( ( ($proximoItem->pai == null) && ($proximoItem->rota == null) ) )
                @if( ($menuitem->pai != null) && ($menuitem->rota != null) )
                  </ul>
                </li>
              </ul>
            </li>
                @endif
              @endif
              @if( ( ($proximoItem->pai == null) && ($proximoItem->rota != null) ) )
                @if( ($menuitem->pai != null) && ($menuitem->rota != null) )
                  </ul>
                </li>
              </ul>
            </li>
                @endif
              @endif
          @endif
      @endfor
    @endif
      </ul>
    </ul>
    <li><a href="{{ url('/auth/logout') }}"><i class="fa fa-sign-out"></i> <span>Sair</span></a></li>
  </ul>
</section>