@extends('layouts.app')

@section('content')
    <section class="content-header">
      <h1>
        <i class="fa fa-bar-chart"></i> Histórico de Acesso
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-bar-chart"></i> Histórico de Acesso</a></li>
        <li class="active">Listar</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">        
          <div class="box">

            <!-- /.box-header -->
            <div class="box-body">
               <table id="datatable" class="table table-bordered table-hover row-border">
                <thead>
                <tr>
                  <th>IP</th>
                  <th>Acessou em</th>
                </tr>
                </thead>
                <tbody>
                @foreach($acessos as $acesso)            
                <tr>
                  <td>{{ $acesso->ip }}</td>
                  <td>{{ $acesso->created_at->format('d/m/Y H:i:s') }}</td>
                </tr>
                @endforeach                
                </tbody>               
              </table>              
            </div>
            <!-- /.box-body -->           
            </div>
          </div>
          <!-- /.box -->
        </div>      
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
