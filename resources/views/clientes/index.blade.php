@extends('layouts.app')

@section('content')
    <section class="content-header">
      <h1>
        <i class="fa fa-youtube-play"></i> Clientes
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-youtube-play"></i> Clientes</a></li>
        <li class="active">Listar</li>
      </ol>
      @if( Auth::user()->temRota('manager.clientes.create') )        
        <a class="btn btn-block btn-primary btn-sm btn-adicionar" href="{{ url('manager/clientes/create') }}">
          <i class="fa fa-edit"></i> Adicionar
        </a>         
      @endif
    </section>
   @if (session('status')) 
      <section class="content-header">
        <div class="alert alert-{{ session('status') }}">
            {{ session('retornomensagem') }}
        </div>
      </section>
    @endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">        
          <div class="box">

            <!-- /.box-header -->
            <div class="box-body">
               <table id="datatable" class="table table-bordered table-hover row-border">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Nome</th>
                  <th>CPF/CNPJ</th>
                  <th>Gerado em</th>
                  <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                @foreach($clientes as $cliente)            
                <tr>
                  <td>{{ $cliente->id }}</td>
                  <td>{{ $cliente->nome }}</td>
                  <td>{{ $cliente->cpf ? $cliente->cpf : $cliente->cnpj }}</td>
                  <td>{{ $cliente->created_at->format('d/m/Y H:i:s') }}</td>
                  <td>                      
                      @if(Auth::user()->id_perfil == 1)
                        <a class="btn btn-app no-margin" href="{{ url('manager/clientes/access/' . $cliente->id)  }}">
                          <i class="fa fa-bar-chart"></i> Acessos
                        </a>
                        <a class="btn btn-app no-margin" href="{{ url('manager/clientes/edit', $cliente->id) }}">
                          <i class="fa fa-edit"></i> Editar
                        </a>
                      @endif
                      <a class="btn btn-app no-margin" href="{{ url('manager/clientes/delete', $cliente->id) }}">
                        <i class="fa fa-trash-o"></i> Excluir
                      </a>
                  </td>
                </tr>
                @endforeach                
                </tbody>               
              </table>              
            </div>
            <!-- /.box-body -->           
            </div>
          </div>
          <!-- /.box -->
        </div>      
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
