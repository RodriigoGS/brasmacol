<div class="form-group">
  {!! Form::label('nome', 'Nome:') !!}
    <div class="input-group">
      {!! Form::text('nome', $cliente->nome,  ['id' => 'nome', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'Nome do cliente']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
  <div class="input-group-addon">
    {!! Form::radio('radio_cpf_cnpj', 'cpf', $cliente->cnpj ? $cliente->cnpj : true, ['id' => 'radio_cpf', 'class' => 'js-cpf-cnpj']); !!}
    {!! Form::label('radio_cpf', 'CPF') !!}
  </div>
  <div class="input-group-addon">
    {!! Form::radio('radio_cpf_cnpj', 'cnpj', $cliente->cnpj, ['id' => 'radio_cnpj', 'class' => 'js-cpf-cnpj']); !!}
    {!! Form::label('radio_cnpj', 'CNPJ') !!}
  </div>
  {!! Form::text('cpf', $cliente->cpf,  ['id' => 'cpf', 'class' => $cliente->cpf || !$cliente->cnpj ? 'form-control' : 'form-control hide', 'required' => 'required', 'placeholder' => 'CPF']) !!}
  {!! Form::text('cnpj', $cliente->cnpj,  ['id' => 'cnpj', 'class' => $cliente->cnpj ? 'form-control' : 'form-control hide', 'placeholder' => 'CNPJ']) !!}
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('telefone', 'Telefone:') !!}
    <div class="input-group">
      {!! Form::text('telefone', $cliente->telefone,  ['id' => 'telefone', 'class' => 'form-control', 'required' => 'required', 'placeholder' => '(99) 99999-9999']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('email', 'E-mail:') !!}
    <div class="input-group">
      {!! Form::text('email', $cliente->email,  ['id' => 'email', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'cliente@email.com']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('status', 'Status:') !!}
  {!! Form::select('status', array(1 => 'Ativo', 2 => 'Inativo'), $cliente->status, ['class' => 'form-control', 'required' => 'required']) !!}
</div>
<!-- /.form group -->