@extends('layouts.app')

@section('content')

    <section class="content-header">
      <h1>
        <i class="fa fa-object-group"></i> Categoria     
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-pencil-square-o"></i> Categoria</a></li>
        <li class="active">Novo</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
              <h3 class="box-title"><i class="fa fa-pencil-square-o"></i> Novo</h3>
            </div>
            <div class="box-body">
                    
                @if($errors->any())
                    <ul class="alert">
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

                {!! Form::open(['route' => 'manager.videos.categorias.store', 'method' => 'post']) !!}
                    <input type="hidden" name="tipo" value="video">

                    @include('categoria._form')

                    <div class="box-footer">
                      <div class="box-tools pull-right">
                        <a class="btn btn-app btn-flat" href="{{ route('manager.videos.categorias.index') }}">
                          <i class="fa fa-reply"></i> Voltar
                        </a>
                        <button type="submit" class="btn btn-app bg-green btn-flat">
                          <i class="fa fa-save"></i> Salvar
                        </button>
                      </div>
                    </div>

                {!! Form::close() !!}

            </div>        
          </div>
        </div>
      
      </div>
      <!-- /.row -->      
    </section>

@endsection
