@extends('layouts.app')

@section('content')
    <section class="content-header">
      <h1>
        <i class="fa fa-youtube-play"></i> Vídeos
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-youtube-play"></i> Vídeos</a></li>
        <li class="active">Listar</li>
      </ol>
      @if( Auth::user()->temRota('manager.videos.create') )        
        <a class="btn btn-block btn-primary btn-sm btn-adicionar" href="{{ url('manager/videos/create') }}">
          <i class="fa fa-edit"></i> Adicionar
        </a>         
      @endif
    </section>
   @if (session('status')) 
      <section class="content-header">
        <div class="alert alert-{{ session('status') }}">
            {{ session('retornomensagem') }}
        </div>
      </section>
    @endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">        
          <div class="box">

            <!-- /.box-header -->
            <div class="box-body">
               <table id="datatable" class="table table-bordered table-hover row-border">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Título</th>
                  <th>Idioma</th>
                  <th>Gerado em</th>
                  <th>Status</th>
                  <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                @foreach($videos as $video)            
                <tr>
                  <td>{{ $video->id }}</td>
                  <td>{{ $video->titulo }}</td>
                  <td>{{ config('location.model')[$video->idioma_id] }}</td>
                  <td>{{ $video->created_at->format('d/m/Y H:i:s') }}</td>
                  <td><span class="badge {{ ($video->status == '1' ? 'bg-green' : 'bg-yellow') }}">{{ $controlador->exibeStatus($video->status) }}</span></td>  
                  <td>                      
                      @if(Auth::user()->id_perfil == 1)
                        <a class="btn btn-app no-margin" href="{{ url('manager/videos/edit', $video->id) }}">
                          <i class="fa fa-edit"></i> Editar
                        </a>
                      @endif
                      <a class="btn btn-app no-margin" href="{{ url('manager/videos/delete', $video->id) }}">
                        <i class="fa fa-trash-o"></i> Excluir
                      </a>
                  </td>
                </tr>
                @endforeach                
                </tbody>               
              </table>              
            </div>
            <!-- /.box-body -->           
            </div>
          </div>
          <!-- /.box -->
        </div>      
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
