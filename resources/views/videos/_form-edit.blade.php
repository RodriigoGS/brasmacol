<div class="form-group">
  {!! Form::label('titulo', 'Título:') !!}
    <div class="input-group">
      {!! Form::text('titulo', $video->titulo,  ['id' => 'titulo', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'Título do vídeo']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
    {!! Form::label('link', 'URL:') !!}
    <div class="input-group">
      {!! Form::text('link', $video->link,  ['id' => 'link', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'https://www.youtube.com']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group" id="listagemcategorias">    
  {!! Form::label('', 'Categorias:') !!}
  @foreach($categorias as $categoria)   
    <div class="form-check">
      <div class="checkbox">
          <label for="cat{{ $categoria->id }}">
            {!! Form::checkbox('categorias[]', $categoria->id, in_array($categoria->id, $categoriasVideos)? true : null, ['id' => 'cat'.$categoria->id, 'class' => 'form-check-input flat-blue']) !!} {{ $categoria->titulo }} - {{ config('location.model')[$categoria->idioma_id] }}
          </label>
      </div>      
    </div>
  @endforeach
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('idioma_id', 'Status:') !!}
  {!! Form::select('idioma_id', config('location.model'), $video->idioma_id, ['class' => 'form-control', 'required' => 'required']) !!}
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('status', 'Status:') !!}
  {!! Form::select('status', array(1 => 'Ativo', 2 => 'Inativo'), $video->status, ['class' => 'form-control', 'required' => 'required']) !!}
</div>
<!-- /.form group -->