@extends('layouts.start')

@section('content')
 <form role="form" method="POST" action="{{ url('auth/login') }}">
    {{ csrf_field() }}
    <div class="form-group has-feedback">        

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">                            
            <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email/Nome">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
        </div>
    </div>
    <div class="form-group has-feedback">        
        <div class="form-group{{ $errors->has('senha') ? ' has-error' : '' }}">
            <input id="senha" type="password" class="form-control" name="senha" placeholder="Senha">

            @if ($errors->has('senha'))
                <span class="help-block">
                    <strong>{{ $errors->first('senha') }}</strong>
                </span>
            @endif
                          
        </div>
                  
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
    </div>
    <div class="row">
    <div class="col-xs-8">
      {{-- <div class="checkbox icheck">
        <label>
          <input type="checkbox"> Remember Me
        </label>
      </div> --}}
    </div>
    <!-- /.col -->
    <div class="col-xs-4">
      <button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>
    </div>
    <!-- /.col -->
    </div>
</form>
@endsection
