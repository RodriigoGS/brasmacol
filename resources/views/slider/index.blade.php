@extends('layouts.app')

@section('content')
    <section class="content-header">
      <h1>
        <i class="fa fa-file-image-o"></i> Slider
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-file-image-o"></i> Slider</a></li>
        <li class="active">Listar</li>
      </ol>
    </section>
   @if (session('status')) 
      <section class="content-header">
        <div class="alert alert-{{ session('status') }}">
            {{ session('retornomensagem') }}
        </div>
      </section>
    @endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">        
          <div class="box">

            <!-- /.box-header -->
            <div class="box-body">
               <table id="datatable" class="table table-bordered table-hover row-border">
                <thead>
                <tr>
                    <th>Imagem</th>                                    
                    <th>Título</th>
                    <th>Data</th>
                    <th>Status</th>
                    <th>Ordem</th>
                    <th>Ações</th>                  
                </tr>
                </thead>
                <tbody>
                @foreach($sliders as $slider)            
                <tr>                    
                    <td><img src="{{ asset('upload').'/'.$slider->imagem }}" height="140" width="200" /></td>
                    <td>{{ $slider->titulo }}</td>
                    <td>{{ $slider->data_br }}</td>
                    <td><span class="badge {{ ($slider->status == '1' ? 'bg-green' : 'bg-yellow') }}">{{ $controlador->exibeStatus($slider->status) }}</span></td>                    
                    <td>{{ $slider->ordem }}</td>
                    <td>
                        <a class="btn btn-app no-margin" href="{{ url('manager/slider/edit', $slider->id) }}">
                          <i class="fa fa-edit"></i> Editar
                        </a>
                        <a class="btn btn-app no-margin" href="{{ url('manager/slider/delete', $slider->id) }}">
                          <i class="fa fa-trash-o"></i> Excluir
                        </a>
                    </td>
                </tr>
                @endforeach                
                </tbody>               
              </table>              
            </div>
            <!-- /.box-body -->           
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
