<div class="form-group">
  {!! Form::label('titulo', 'Título:') !!}
    <div class="input-group">
      {!! Form::text('titulo', null,  ['id' => 'titulo', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'Título']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<div class="form-group">
  {!! Form::label('url', 'Link:') !!}
    <div class="input-group">
      {!! Form::text('url', null,  ['id' => 'url', 'class' => 'form-control', 'placeholder' => 'http://']) !!}
      <div class="input-group-addon">
        <i class="fa fa-link"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<div class="form-group">
  {!! Form::label('texto', 'Texto:') !!}
    <div class="input-group">
      {!! Form::textarea('texto', null,  ['id' => 'editor1', 'class' => 'form-control']) !!}
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('titulo_botao', 'Título para o botão:') !!}
    <div class="input-group">
      {!! Form::text('titulo_botao', null,  ['id' => 'titulo_botao', 'class' => 'form-control', 'placeholder' => 'Botão']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<div class="form-group">
	{!! Form::label('imagem', 'Nova imagem:') !!}
	{!! Form::file('imagem') !!}
</div>
@if($slider->id_imagem != null)
	<div class="form-group">
		{!! Form::label('imagem-antiga', 'Imagem inserida:') !!}
		<td><img src="{{ asset('upload').'/'.$imagem->arquivo }}" height="180" width="240" /></td>
	</div>
@endif()
<div class="form-group">
    {!! Form::label('ordem', 'Ordem:') !!}
    {!! Form::selectRange('ordem', 1, 20, null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('status', 'Status:') !!}	
	{!! Form::select('status', array(1 => 'Ativo', 2 => 'Inativo'), null, ['class' => 'form-control', 'required' => 'required']) !!}
</div>