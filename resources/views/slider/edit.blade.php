@extends('layouts.app')

@section('content')
    <section class="content-header">
      <h1>
        <i class="fa fa-file-image-o"></i> Editando Imagem        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-file-image-o"></i> Editando Imagem</a></li>
        <li class="active">Editar</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
              <h3 class="box-title"><i class="fa fa-edit"></i> Editar</h3>
            </div>
            <div class="box-body">
                    
                @if (session('status')) 
                    <div class="alert alert-{{ session('status') }}">
                        {{ session('retornomensagem') }}
                    </div>
                @endif
                
                    @if($errors->any())
                        <ul class="alert">
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                {!! Form::model($slider, ['route' => ['manager.slider.update', $slider->id], 'method' => 'put', 'files' => true]) !!}


                    @include('slider._form')                               
                  

                    <div class="box-footer">
                      <div class="box-tools pull-right">
                        <a class="btn btn-app btn-flat" href="{{ route('manager.slider.index') }}">
                          <i class="fa fa-reply"></i> Voltar
                        </a>
                        <button type="submit" class="btn btn-app bg-green btn-flat">
                          <i class="fa fa-save"></i> Salvar
                        </button>
                      </div>
                    </div>

                {!! Form::close() !!}

            </div>            
          </div>
        </div>
        <div class="col-md-3">
          <div class="box box-danger">
            <div class="box-header">
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
              <h3 class="box-title"><i class="fa fa-info"></i> Informações</h3>
            </div>
            <div class="box-body no-padding">
              <table class="table table-bordered">
                <tr>
                  <td><big>Adicionado em:</big></td>
                  <td>{{ $slider->created_at->format('d/m/Y H:i:s') }}</td>
                </tr>
                <tr>
                  <td><big>Alterado em:</big></td>
                  <td>{{ $slider->updated_at->format('d/m/Y H:i:s') }}</td>
                </tr>
                <tr>
                  <td><big>Alterado por:</big></td>
                  <td>{{ $usuarioCadastro->nome }}</td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>      
    </section>
@endsection