@extends('layouts.app')

@section('content')

    <section class="content-header">
      <h1>
        <i class="fa fa-shopping-basket"></i> Produto
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-envelope-o"></i> Produto</a></li>
        <li class="active">Novo</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
              <h3 class="box-title"><i class="fa fa-pencil-square-o"></i> Novo</h3>
            </div>
            <div class="box-body">
                    
                @if($errors->any())
                    <ul class="alert">
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

                {!! Form::open(['route' => 'manager.produtos.store', 'method' => 'post', 'files' => true]) !!}

                    @include('produtos._form-create')

                    <div class="box-footer">
                      <div class="box-tools pull-right">
                        <a class="btn btn-app btn-flat" href="{{ route('manager.produtos.index') }}">
                          <i class="fa fa-reply"></i> Voltar
                        </a>
                        <button type="submit" class="btn btn-app bg-green btn-flat">
                          <i class="fa fa-save"></i> Salvar
                        </button>
                      </div>
                    </div>

                {!! Form::close() !!}

            </div>        
          </div>
        </div>
      
      </div>
      <!-- /.row -->      
    </section>

@endsection