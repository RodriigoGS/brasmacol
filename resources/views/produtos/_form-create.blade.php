<div class="form-group">
  {!! Form::label('titulo', 'Título:') !!}
    <div class="input-group">
      {!! Form::text('titulo', old('titulo'),  ['id' => 'titulo', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'Título']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
  	{!! Form::label('link', 'Vídeo:') !!}
    <div class="input-group">
      {!! Form::text('link', old('video'),  ['id' => 'link', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'Título']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
  	{!! Form::label('slug', 'URL:') !!}
    <div class="input-group">
      {!! Form::text('slug', old('slug'),  ['id' => 'slug', 'class' => 'form-control', 'placeholder' => 'http://projetodesignio.com.br']) !!}
      <div class="input-group-addon">
        <i class="fa fa-link"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
  	{!! Form::label('descricao', 'Texto:') !!}
    {!! Form::textarea('descricao', old('descricao'),  ['id' => 'editor1', 'class' => 'form-control']) !!}
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('galeria_id', 'Galeria:') !!}
  {!! Form::select('galeria_id', $galerias, old('galeria_id'), ['class' => 'form-control', 'required' => 'required']) !!}
</div>
<!-- /.form group -->

<div class="form-group" id="listagemcategorias">    
  {!! Form::label('', 'Categorias:') !!}
  @foreach($categorias as $categoria)    
    <div class="form-check">
      <div class="checkbox">
          <label for="cat{{ $categoria->id }}">
            {!! Form::checkbox('categorias[]', $categoria->id, null, ['id' => 'cat'.$categoria->id, 'class' => 'form-check-input flat-blue']) !!} {{ $categoria->titulo }} - {{ config('location.model')[$categoria->idioma_id] }}
          </label>
      </div>      
    </div>
  @endforeach
</div>
<!-- /.form group -->

<div class="form-group">
	{!! Form::label('idioma_id', 'Status:') !!}
	{!! Form::select('idioma_id', config('location.model'), old('idioma_id'), ['class' => 'form-control', 'required' => 'required']) !!}
</div>
<!-- /.form group -->

<div class="form-group">
	{!! Form::label('status', 'Status:') !!}
	{!! Form::select('status', array(1 => 'Ativo', 2 => 'Inativo'), old('status'), ['class' => 'form-control', 'required' => 'required']) !!}
</div>
<!-- /.form group -->

<script>
$( document ).ready(function() {  
    CKEDITOR.basePath = '{{ asset('assets/plugins/ckeditor/').'/' }}';
    CKEDITOR.plugins.basePath = '{{ asset('assets/plugins/ckeditor/plugins/') }}' + '/';
    CKEDITOR.replace( 'editor1', {
        customConfig: '{{ asset('assets/plugins/ckeditor/config.js') }}'
    });         
});
</script>