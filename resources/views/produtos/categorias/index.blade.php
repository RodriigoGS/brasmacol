@extends('layouts.app')

@section('content')
    <section class="content-header">
      <h1>
        <i class="fa fa-tasks"></i> Categorias
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-tasks"></i> Categorias</a></li>
        <li class="active">Listar</li>
      </ol>
      @if( Auth::user()->temRota('manager.categoria.create') )        
        <a class="btn btn-block btn-primary btn-sm btn-adicionar" href="{{ url('manager/produtos/categorias/create') }}">
          <i class="fa fa-edit"></i> Adicionar
        </a>         
      @endif
    </section>
   @if (session('status')) 
      <section class="content-header">
        <div class="alert alert-{{ session('status') }}">
            {{ session('retornomensagem') }}
        </div>
      </section>
    @endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">        
          <div class="box">

            <!-- /.box-header -->
            <div class="box-body">
               <table id="datatable" class="table table-bordered table-hover row-border">
                <thead>
                <tr>
                  <th>ID</th>
                    <th>Nome</th>
                    <th>Idioma</th>
                    <th>Status</th>
                    <th>Ações</th>                  
                </tr>
                </thead>
                <tbody>
                @foreach($categorias as $categoria)            
                <tr>
                    <td>{{ $categoria->id }}</td>
                    <td>{{ $categoria->titulo }}</td>   
                    <td>{{ config('location.model')[$categoria->idioma_id] }}</td>                  
                    <td><span class="badge {{ ($categoria->status == '1' ? 'bg-green' : 'bg-yellow') }}">{{ $controlador->exibeStatus($categoria->status) }}</span></td>                    
                    <td>
                            <a class="btn btn-app no-margin" href="{{ url('manager/produtos/categorias/edit', $categoria->id) }}">
                              <i class="fa fa-edit"></i> Editar
                            </a>
                            <a class="btn btn-app no-margin" href="{{ url('manager/produtos/categorias/delete', $categoria->id) }}">
                              <i class="fa fa-trash-o"></i> Excluir
                            </a>
                        </td>
                </tr>
                @endforeach                
                </tbody>               
              </table>              
            </div>
            <!-- /.box-body -->           
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection