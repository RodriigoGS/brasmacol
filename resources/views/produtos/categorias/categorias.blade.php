{!! Form::label('', 'Categorias:') !!}
@foreach($categorias as $categoria)    
  <div class="form-check">
    <div class="checkbox">
        <label for="cat{{ $categoria->id }}">
          {!! Form::checkbox('categorias[]', $categoria->id, ( (in_array($categoria->id, $categoriasProdutos)) ? true : null), ['id' => 'cat'.$categoria->id, 'class' => 'form-check-input flat-blue']) !!} {{ $categoria->titulo }}
        </label>
    </div>      
  </div>
@endforeach