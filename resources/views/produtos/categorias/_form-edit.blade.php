<div class="form-group">
  {!! Form::label('titulo', 'Título:') !!}
    <div class="input-group">
      {!! Form::text('titulo', $categoria->titulo,  ['id' => 'titulo', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'Título']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
    {!! Form::label('thumb', 'Thumbnail:') !!}
    <div class="input-file-fake">
      <label for="thumb" class="thumb-text">{{ $categoria->thumb }}</label>
      <span class="thumb-img" style="background-image: url(' {{ asset('upload/categoria/' . $categoria->thumb)  }} ')"></span>
      {!! Form::file('thumb', null,  ['id' => 'thumb', 'class' => 'form-control input-file js-input-file', 'required' => 'required']) !!}
    </div>
    <!-- /.input-fake -->
</div>
<!-- /.form group -->

<div class="form-group">
    {!! Form::label('descricao', 'Texto:') !!}
    {!! Form::textarea('descricao', $categoria->descricao,  ['id' => 'editor1', 'class' => 'form-control']) !!}
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('idioma_id', 'Idioma:') !!}
  {!! Form::select('idioma_id', config('location.model'), $categoria->idioma_id, ['class' => 'form-control', 'required' => 'required']) !!}
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('status', 'Status:') !!}  
  {!! Form::select('status', array(1 => 'Ativo', 2 => 'Inativo'), $categoria->status, ['class' => 'form-control', 'required' => 'required']) !!}
</div>

<script>
$( document ).ready(function() {  
    CKEDITOR.basePath = '{{ asset('assets/plugins/ckeditor/').'/' }}';
    CKEDITOR.plugins.basePath = '{{ asset('assets/plugins/ckeditor/plugins/') }}' + '/';
    CKEDITOR.replace( 'editor1', {
        customConfig: '{{ asset('assets/plugins/ckeditor/config.js') }}'
    });         
});
</script>