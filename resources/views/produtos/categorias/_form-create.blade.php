<div class="form-group">
  {!! Form::label('titulo', 'Título:') !!}
    <div class="input-group">
      {!! Form::text('titulo', old('titulo'),  ['id' => 'titulo', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'Título']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
    {!! Form::label('thumb', 'Thumbnail:') !!}
    <div class="input-file-fake">
      <label for="thumb" class="thumb-text">Thumbnail</label>
      <span class="thumb-img"></span>
      {!! Form::file('thumb', null,  ['id' => 'thumb', 'class' => 'form-control input-file js-input-file', 'required' => 'required']) !!}
    </div>
    <!-- /.input-fake -->
</div>
<!-- /.form group -->

<div class="form-group">
    {!! Form::label('descricao', 'Texto:') !!}
    {!! Form::textarea('descricao', old('descricao'),  ['id' => 'editor1', 'class' => 'form-control']) !!}
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('idioma_id', 'Idioma:') !!}
  {!! Form::select('idioma_id', config('location.model'), old('idioma_id'), ['class' => 'form-control', 'required' => 'required']) !!}
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('status', 'Status:') !!}  
  {!! Form::select('status', array(1 => 'Ativo', 2 => 'Inativo'), old('status'), ['class' => 'form-control', 'required' => 'required']) !!}
</div>