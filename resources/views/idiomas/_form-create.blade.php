<div class="form-group">
  {!! Form::label('nome', 'Nome:') !!}
    <div class="input-group">
      {!! Form::text('nome', old('nome'),  ['id' => 'nome', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'Nome']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
    {!! Form::label('format', 'Formatado de Data:') !!}
    <div class="input-group">
      {!! Form::text('format', old('format'),  ['id' => 'format', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'Formatado']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
    {!! Form::label('locale', 'Sigla:') !!}
    <div class="input-group">
      {!! Form::text('locale', old('locale'),  ['id' => 'locale', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'Sigla']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('status', 'Status:') !!}
  {!! Form::select('status', array(1 => 'Ativo', 2 => 'Inativo'), old('status'), ['class' => 'form-control', 'required' => 'required']) !!}
</div>
<!-- /.form group -->