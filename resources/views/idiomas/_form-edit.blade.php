<div class="form-group">
  {!! Form::label('nome', 'Nome:') !!}
    <div class="input-group">
      {!! Form::text('nome', $idioma->nome,  ['id' => 'nome', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'Nome']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
    {!! Form::label('format', 'Formatado de Data:') !!}
    <div class="input-group">
      {!! Form::text('format', $idioma->format,  ['id' => 'format', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'Formatado']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
    {!! Form::label('locale', 'Sigla:') !!}
    <div class="input-group">
      {!! Form::text('locale', $idioma->locale,  ['id' => 'locale', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'Sigla']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('status', 'Status:') !!}
  {!! Form::select('status', array(1 => 'Ativo', 2 => 'Inativo'), $idioma->status, ['class' => 'form-control', 'required' => 'required']) !!}
</div>
<!-- /.form group -->