@extends('layouts.app')

@section('content')
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Dashboard
          <small>Painel de Controle</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green custom-box-dash small-box-custom">
              <div class="inner">
                <h3>{{ $pageviews }}</h3>

                <p>Acessos ao Site</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>              
            </div>
          </div>
          
          <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow custom-box-dash small-box-custom">
            <div class="inner">
              <h3>{{ $visitasunicas }}</h3>

              <p>Visitas Únicas</p>
            </div>
            <div class="icon">
              <i class="ion ion-person"></i>
            </div>            
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua custom-box-dash small-box-custom">
            <div class="inner">
              <h3 style="font-size: 20px;margin-bottom: 30px;">
                @if($maisvisitado == null)<a href="" style="color:#fff">sem visitas</a>@endif
                @if($maisvisitado != null)
                  <a href="{{ URL::to('/').$maisvisitado->caminho }}" target="_blank" style="color:#fff">
                  @if($maisvisitado->caminho == '/'){{ '/home' }}@endif
                  @if($maisvisitado->caminho != '/'){{ $maisvisitado->caminho }}@endif
                  </a>
                @endif
              </h3>

              <p>Página mais acessada</p>
            </div>
            <div class="icon">
              <i class="ion ion-document-text"></i>
            </div>            
          </div>
        </div>      

        </div>
        <!-- /.row (main row) -->
        <div class="row">
          <div class="lg-6 col-xs-12">
            <div class="box box-info">
              <div class="box-header">
                <h3 class="box-title">Visitas ao Site</h3>
              </div>
              <form name="filtroacessos" id="filtroacessos" class="form-horizontal">
              <div class="box-body">
                <div class="form-group">                

                  <label for="mesacesso" class="col-md-1 col-sm-2 control-label">Mês</label>
                  <div class="col-sm-2">
                    <select name="mesacesso" id="mesacesso" class="form-control">
                    @for($i=1; $i <= 12; $i++)
                      <option value="{{ $i }}"{{ (date('m') == $i? ' selected="selected"' : null) }}>{{ $i }}</option>
                    @endfor
                    </select>
                  </div>
                  <label for="anoacesso" class="col-md-1 col-sm-2 control-label">Ano</label>
                  <div class="col-sm-2">
                    <select name="anoacesso" id="anoacesso" class="form-control">
                    @for($i=2017; $i < 2056; $i++)
                      <option value="{{ $i }}"{{ (date('Y') == $i? ' selected="selected"' : null) }}>{{ $i }}</option>
                    @endfor
                    </select>
                  </div>

                  <label for="" class="col-md-1 col-sm-2 control-label"></label>
                  <div class="col-sm-2">
                    <button type="submit" class="btn btn-default enviarfiltroacessos"><i class="fa fa-sliders"></i> Atualizar</button>
                  </div>
                </div>                                 
              </div>                        
            </form>
            <script>
              $(document).ready(function(event) {
                $( "#filtroacessos" ).submit(function( event ) {                
                  var ano = $('select[name=anoacesso]').val();
                  var mes = $('select[name=mesacesso]').val();
                  $(".enviarfiltroacessos").html('<i class="fa fa-spinner fa-spin"></i> Atualizando');                

                  event.preventDefault();                  
                  $.ajax({
                    headers: {
                      'X-CSRF-Token': '{{ Session::getToken() }}'
                    },
                    type: "POST",
                    data: { mes: mes, ano: ano, ajax:'true' },
                    url: '{{ route('manager.grafico.acessos') }}',
                    success: function(data) {
                      var obj = jQuery.parseJSON(data);
                      areaChartData.datasets[0].data = obj;
                      barChart.Bar(barChartData, barChartOptions);
                      $(".enviarfiltroacessos").html('<i class="fa fa-spinner fa-sliders"></i> Atualizar');
                    },
                    error: function() {
                      $(".enviarfiltroacessos").html('<i class="fa fa-spinner fa-sliders"></i> Atualizar');
                    }
                    });
                  });
              });
            </script>
              <div class="box-body">
                <div class="chart">
                  <canvas width="488" height="229" id="barChart" style="height: 229px; width: 488px;"></canvas>
              </div>
            </div>
            </div>
          <!-- /.box -->
          </div>
        </div>
        </div>
        <!-- /.row (main row) -->

      </section>
      <!-- /.content -->
@endsection
