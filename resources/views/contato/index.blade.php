@extends('layouts.app')

@section('content')
    <section class="content-header">
      <h1>
        <i class="fa fa-comment-o"></i> Fale Conosco
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-comment-o"></i> Fale Conosco</a></li>
        <li class="active">Listar</li>
      </ol>
      @if( Auth::user()->temRota('manager.contato.create') )        
        <a class="btn btn-block btn-primary btn-sm btn-adicionar" href="{{ url('manager/contato/create') }}">
          <i class="fa fa-edit"></i> Adicionar
        </a>         
      @endif
    </section>
   @if (session('status')) 
      <section class="content-header">
        <div class="alert alert-{{ session('status') }}">
            {{ session('retornomensagem') }}
        </div>
      </section>
    @endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">        
          <div class="box">

            <!-- /.box-header -->
            <div class="box-body">
               <table id="datatable" class="table table-bordered table-hover row-border">
                <thead>
                <tr>
                  <th>ID</th>
                    <th>Nome</th>
                    {{-- <th>Área de interesse</th> --}}
                    <th>Lido</th>
                    {{-- <th>Instrutor?</th> --}}
                    <th>Em</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                @foreach($contatos as $contato)            
                <tr>
                    <td>{{ $contato->id }}</td>
                    <td>{{ $contato->nome }}</td>
                    {{-- <td>{{ ($contato->id_area != null)? $contato->area->descricao : 'Nenhum' }}</td> --}}
                    <td><span class="badge {{ ($contato->lido == '1' ? 'bg-green' : 'bg-red') }}">{{ ($contato->lido == 1)? 'Sim': 'Não' }}</span></td>
                    {{-- <td><span class="badge {{ ($contato->instrutor == 1 ? 'bg-green' : 'bg-red') }}">{{ ($contato->instrutor == 1)? 'Sim': 'Não' }}</span></td> --}}
                    <td>{{ $contato->created_at->format('d/m/Y H:i:s') }}</td>
                    <td>
                        <a class="btn btn-app no-margin" href="{{ url('manager/contato/show', $contato->id) }}">
                          <i class="fa fa-eye"></i> Ver
                        </a>                        
                        @if(Auth::user()->id_perfil == 1)
                          <a class="btn btn-app no-margin" href="{{ url('manager/contato/edit', $contato->id) }}">
                            <i class="fa fa-edit"></i> Editar
                          </a>
                        @endif
                        <a class="btn btn-app no-margin" href="{{ url('manager/contato/delete', $contato->id) }}">
                          <i class="fa fa-trash-o"></i> Excluir
                        </a>
                    </td>
                </tr>
                @endforeach                
                </tbody>               
              </table>              
            </div>
            <!-- /.box-body -->           
            </div>
          </div>
          <!-- /.box -->
        </div>      
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
