<a href="#" class="dropdown-toggle" data-toggle="dropdown">
  <i class="fa fa-envelope-o"></i>
  @if($mensagemcontato->nao_lidos > 0)
    <span class="label label-danger">{{ $mensagemcontato->nao_lidos }}</span>
  @endif              
</a>
<ul class="dropdown-menu">
  @if($mensagemcontato->nao_lidos > 1)
    <li class="header">{{ $mensagemcontato->nao_lidos }} novas mensagens</li>
  @elseif($mensagemcontato->nao_lidos == 1)
    <li class="header">{{ $mensagemcontato->nao_lidos }} nova mensagem</li>
  @else
    <li class="header">Sem novas mensagens</li>
  @endif
@if($mensagemcontato->nao_lidos > 0)
    <li>
      <ul class="menu">
      <!-- inner menu: contains the actual data -->
  @foreach($mensagens as $mensagem)                  
  <?php
      $dataInicio = new DateTime($mensagem->created_at);
      $dataFim = $dataInicio->diff(new DateTime('now'));
      $minutos = $dataFim->days * 24 * 60;
      $minutos += $dataFim->h * 60;
      $minutos += $dataFim->i;
      $horas = floor($minutos / 60);
  ?>
        <li><!-- start message -->
          <a href="{{ url('manager/contato/show', $mensagem->id) }}">
            <h4 class="oxy-margin-0">
              {{ $mensagem->nome }}
              <small><i class="fa fa-clock-o"></i> {{ ($minutos <= 60)? $minutos. ' mins' : $horas.' horas' }}</small>
            </h4>
          </a>
        </li>
        <!-- end message -->                  
  @endforeach                
      </ul>
    </li>
@endif                
  </ul>