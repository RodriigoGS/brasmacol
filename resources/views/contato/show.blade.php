@extends('layouts.app')

@section('content')

    <section class="content-header">
      <h1>
        <i class="fa fa-comment-o"></i> Contatos
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-comment-o"></i> Contatos</a></li>
        <li class="active">Visualização</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
              <h3 class="box-title"><i class="fa fa-eye"></i> Visualização</h3>
            </div>
            <div class="box-body">                               

                <div class="form-group">
                    {!! Form::label('nome', 'Nome:') !!}                        
                    {{ $contato->nome }}
                </div>
                <div class="form-group">
                    {!! Form::label('email', 'E-mail:') !!}
                    {{ $contato->email }}
                </div>
                <div class="form-group">
                    {!! Form::label('telefone', 'Telefone:') !!}
                    {{ $contato->telefone }}
                </div>

                <div class="form-group">
                    {!! Form::label('mensagem', 'Mensagem enviada:') !!}
                    {!! Form::textarea('mensagem', $contato->mensagem,  ['class' => 'form-control']) !!}

                <div class="box-footer">
                  <div class="box-tools pull-right">
                    <a class="btn btn-app btn-flat" href="{{ route('manager.contato.index') }}">
                      <i class="fa fa-reply"></i> Voltar
                    </a>
                  </div>
                </div>                

            </div>        
          </div>
        </div>     
      </div>
      <!-- /.row -->      
    </section>

@endsection
