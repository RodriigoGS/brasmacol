<div class="form-group">
  {!! Form::label('nome', 'Nome:') !!}
    <div class="input-group">
      {!! Form::text('nome', $soliciteAcesso->nome,  ['id' => 'nome', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'Nome']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('telefone', 'Telefone:') !!}
    <div class="input-group">
      {!! Form::text('telefone', $soliciteAcesso->telefone,  ['id' => 'telefone', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'Telefone']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('email', 'E-mail:') !!}
    <div class="input-group">
      {!! Form::text('email', $soliciteAcesso->email,  ['id' => 'email', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'E-mail']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('empresa', 'Empresa:') !!}
    <div class="input-group">
      {!! Form::text('empresa', $soliciteAcesso->empresa,  ['id' => 'empresa', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'Empresa']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('cargo', 'Cargo:') !!}
    <div class="input-group">
      {!! Form::text('cargo', $soliciteAcesso->cargo,  ['id' => 'cargo', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'Cargo']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
    {!! Form::label('mensagem', 'Mensagem:') !!}
    {!! Form::textarea('mensagem', $soliciteAcesso->mensagem,  ['id' => 'editor1', 'class' => 'form-control']) !!}
</div>
<!-- /.form group -->


<div class="form-group">
  {!! Form::label('status', 'Status:') !!}
  {!! Form::select('status', array(1 => 'Ativo', 2 => 'Inativo'), $soliciteAcesso->status, ['class' => 'form-control', 'required' => 'required']) !!}
</div>
<!-- /.form group -->

<script>
$( document ).ready(function() {  
    CKEDITOR.basePath = '{{ asset('assets/plugins/ckeditor/').'/' }}';
    CKEDITOR.plugins.basePath = '{{ asset('assets/plugins/ckeditor/plugins/') }}' + '/';
    CKEDITOR.replace( 'editor1', {
        customConfig: '{{ asset('assets/plugins/ckeditor/config.js') }}'
    });         
});
</script>