<div class="form-group">
  {!! Form::label('descricao', 'Nome:') !!}
    <div class="input-group">
      {!! Form::text('descricao', null,  ['id' => 'descricao', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'Título']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->