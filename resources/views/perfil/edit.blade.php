@extends('layouts.app')

@section('content')
    <section class="content-header">
      <h1>
        <i class="fa fa-user-secret"></i> Perfil        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user-secret"></i> Perfil</a></li>
        <li class="active">Editar</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
              <h3 class="box-title"><i class="fa fa-edit"></i> Editar</h3>
            </div>
            <div class="box-body">
                    
                @if($errors->any())
                    <ul class="alert">
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

                {!! Form::model($perfil, ['route' => ['manager.perfil.update', $perfil->id], 'method' => 'put']) !!}
        

                    @include('perfil._form')                            
                  
                    <div class="panel-heading"><i class="fa fa-list"></i> Acessos para este perfil</div>
                        <div class="panel-body">  
                            <div class="form-group">

                                @foreach($menus as $menu)

                                    <!-- /.form group -->
                                    <div class="form-group">                                                                                             
                                        <div class="form-check">
                                          <div class="checkbox">
                                              <label>
                                                {!! Form::checkbox('menus[]', $menu->id, in_array($menu->id, $menus_perfil), ['class' => 'checkbox', 'id' => 'menu'.$menu->id, 'class' => 'form-check-input']) !!} {!! (($menu->pai == null)? $menu->descricao : $menu->parent->descricao.' - '.$menu->descricao) !!}
                                              </label>
                                          </div>      
                                        </div>                                      
                                    </div>
                                    <!-- /.form group -->                                                                                    
                                @endforeach                                                     
   
                        </div>
                    </div>                             
                  

                    <div class="box-footer">
                      <div class="box-tools pull-right">
                        <a class="btn btn-app btn-flat" href="{{ route('manager.perfil.index') }}">
                          <i class="fa fa-reply"></i> Voltar
                        </a>
                        <button type="submit" class="btn btn-app bg-green btn-flat">
                          <i class="fa fa-save"></i> Salvar
                        </button>
                      </div>
                    </div>

                {!! Form::close() !!}

            </div>        
          </div>
        </div>        
      </div>
      <!-- /.row -->      
    </section>

@endsection
