@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-11 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-user"></i> Minha Conta</div>
                    <div class="panel-body">                                    

                        @if (session('status')) 
                            <div class="alert alert-{{ session('status') }}">
                                {{ session('retornomensagem') }}
                            </div>
                        @endif

                        {!! Form::model($usuario, ['route' => ['manager.usuario.userupdate', $usuario->id], 'method' => 'put', 'files' => true]) !!}
        

                            @include('usuario._formuser')                             
                          

                            <div class="box-footer">
                                <div class="box-tools pull-right">                    
                                    <button type="submit" class="btn btn-app bg-green btn-flat">
                                        <i class="fa fa-save"></i> Salvar
                                    </button>
                                </div>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
    </div>
</div>
@endsection
