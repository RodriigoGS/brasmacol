<div class="form-group{{ $errors->has('nome') ? ' has-error' : '' }}">
  {!! Form::label('nome', 'Nome Completo:') !!}
    <div class="input-group">
      {!! Form::text('nome', null,  ['id' => 'nome', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'Nome', 'value' => "{{ old('nome') }}" ]) !!}
      <div class="input-group-addon">
        <i class="fa fa-user"></i>
      </div>
    </div>
    @if ($errors->has('nome'))
        <span class="help-block">
            <strong>{{ $errors->first('nome') }}</strong>
        </span>
    @endif
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group{{ $errors->has('usuario') ? ' has-error' : '' }}">
    {!! Form::label('usuario', 'Login:') !!}
    <div class="input-group">
      {!! Form::text('usuario', null,  ['id' => 'usuario', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'Login', 'value' => "{{ old('usuario') }}" ]) !!}
      <div class="input-group-addon">
        <i class="fa fa-sign-in"></i>
      </div>        
    </div>
    @if ($errors->has('usuario'))
        <span class="help-block">
            <strong>{{ $errors->first('usuario') }}</strong>
        </span>
    @endif
    <!-- /.input group -->   
</div>

<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    {!! Form::label('email', 'E-mail:') !!}
    <div class="input-group">
      {!! Form::email('email', null,  ['id' => 'email', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'E-mail', 'value' => "{{ old('email') }}" ]) !!}
      <div class="input-group-addon">
        <i class="fa fa-envelope-o"></i>
      </div>
    </div>
    @if ($errors->has('nome'))
        <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
    @endif
    <!-- /.input group -->
</div>

<div class="form-group{{ $errors->has('senha') ? ' has-error' : '' }}">
    {!! Form::label('senha', 'Senha (mínimo 6 dígitos):') !!}
    <div class="input-group">
      {!! Form::password('senha',  ['id' => 'senha', 'class' => 'form-control', 'required' => 'required' ]) !!}
      <div class="input-group-addon">
        <i class="fa fa-key"></i>
      </div>
    </div>
    @if ($errors->has('senha'))
        <span class="help-block">
            <strong>{{ $errors->first('senha') }}</strong>
        </span>
    @endif
    <!-- /.input group -->
</div>

<div class="form-group{{ $errors->has('senha_confirmation') ? ' has-error' : '' }}">
    {!! Form::label('senha_confirmation', 'Confirmar Senha:') !!}
    <div class="input-group">
      {!! Form::password('senha_confirmation',  ['id' => 'senha_confirmation', 'class' => 'form-control', 'required' => 'required']) !!}
      <div class="input-group-addon">
        <i class="fa fa-key"></i>
      </div>
    </div>
    @if ($errors->has('senha_confirmation'))
        <span class="help-block">
            <strong>{{ $errors->first('senha_confirmation') }}</strong>
        </span>
    @endif
    <!-- /.input group -->  
</div>

<div class="form-group{{ $errors->has('imagem') ? ' has-error' : '' }}">
    {!! Form::label('imagem', 'Foto (redimensionamento automático para 160x160, máximo 2Mb):') !!}
    <div class="input-group">
      {!! Form::file('imagem', null,  ['id' => 'imagem', 'class' => 'form-control']) !!}     
    </div>
    @if ($errors->has('imagem'))
        <span class="help-block">
            <strong>{{ $errors->first('imagem') }}</strong>
        </span>
    @endif
    <!-- /.input group -->
</div>

{!! Form::hidden('senha_antiga', (isset($usuario->id)? 1 : null) ) !!}