@extends('layouts.app')

@section('content')

    <section class="content-header">
      <h1>
        <i class="fa fa-user"></i> Usuário
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i> Usuário</a></li>
        <li class="active">Editar</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
              <h3 class="box-title"><i class="fa fa-pencil-square-o"></i> Editar</h3>
            </div>
            <div class="box-body">
                    
                @if($errors->any())
                    <ul class="alert">
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

               {!! Form::model($usuario, ['route' => ['manager.usuario.update', $usuario->id], 'method' => 'put', 'files' => true]) !!}
                                    
                  @include('usuario._form')  
                  
                  <div class="form-group">                   
                      {!! Form::submit('Salvar',  ['class' => 'btn btn-primary']) !!}
                      <a href="{{ route('manager.usuario.index') }}" class="btn btn-default">Voltar</a>
                  </div>

              {!! Form::close() !!}

            </div>        
          </div>
        </div>      
      </div>
      <!-- /.row -->      
    </section>

@endsection
