{!! Form::label('', 'Categorias:') !!}
@foreach($listaCategorias as $cat)    
  <div class="form-check">
    <div class="checkbox">
        <label>
          {!! Form::checkbox('categorias[]', $cat->id, ( (in_array($cat->id, $listaCategoriasMarcadas))? true: null), ['id' => 'cat'.$cat->id, 'class' => 'form-check-input flat-blue']) !!} {{ $cat->titulo }}
        </label>
    </div>      
  </div>
@endforeach