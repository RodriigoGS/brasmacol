@extends('layouts.app')

@section('content')

<section class="content-header">
      <h1>
        <i class="fa fa-newspaper-o"></i> Artigos
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-newspaper-o"></i> Artigos</a></li>
        <li class="active">Listar</li>
      </ol>
      @if( Auth::user()->temRota('manager.artigo.create') )        
        <a class="btn btn-block btn-primary btn-sm btn-adicionar" href="{{ url('manager/artigo/create') }}">
          <i class="fa fa-edit"></i> Adicionar
        </a>         
      @endif
    </section>
   @if (session('status')) 
      <section class="content-header">
        <div class="alert alert-{{ session('status') }}">
            {{ session('retornomensagem') }}
        </div>
      </section>
    @endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">        
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="datatable" class="table table-bordered table-hover row-border">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Título</th>
                  <th>Idioma</th>
                  <th>Criado em</th>
                  <th>Status</th>
                  <th>Ações</th>                  
                </tr>
                </thead>
                <tbody>
                @foreach($artigos as $artigo)            
                <tr>
                    <td>{{ $artigo->id }}</td>
                    <td>{{ $artigo->titulo }}</td>
                    <td>{{ config('location.model')[ $artigo->idioma_id ] }}</td>
                    <td>{{ $artigo->created_at->format('d/m/Y H:i:s') }}</td>
                    <td><span class="badge {{ ($artigo->status == '1' ? 'bg-green' : 'bg-yellow') }}">{{ $controlador->exibeStatus($artigo->status) }}</span></td>                    
                    <td>
                      <a class="btn btn-app no-margin" href="{{ url('manager/artigo/edit', $artigo->id) }}">
                        <i class="fa fa-edit"></i> Editar
                      </a>
                      <a class="btn btn-app no-margin" href="{{ url('manager/artigo/delete', $artigo->id) }}">
                        <i class="fa fa-trash-o"></i> Excluir
                      </a>
                    </td>
                </tr>
                @endforeach                
                </tbody>               
              </table>    
            </div>
            <!-- /.box-body -->           
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
