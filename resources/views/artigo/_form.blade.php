<div class="form-group">
  {!! Form::label('titulo', 'Título:') !!}
    <div class="input-group">
      {!! Form::text('titulo', $artigo->titulo,  ['id' => 'titulo', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'Título']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('data', 'Data:') !!}
    <div class="input-group">
      {!! Form::text('data', date('d/m/Y', strtotime($artigo->data)),  ['id' => 'datepicker', 'class' => 'form-control', 'placeholder' => 'dd/mm/aaaa', 'required' => 'required']) !!}
      <div class="input-group-addon">
        <i class="fa fa-calendar"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="bootstrap-timepicker">
  <div class="form-group">
    {!! Form::label('hora', 'Hora:') !!}
    <div class="input-group">
       {!! Form::text('hora', null,  ['id' => 'hora', 'class' => 'form-control timepicker']) !!}
      <div class="input-group-addon">
        <i class="fa fa-clock-o"></i>
      </div>
    </div>
  </div>
</div>
    <!-- /.input group -->
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('url', 'URL:') !!}
    <div class="input-group">
      {!! Form::text('url', $artigo->slug,  ['id' => 'url', 'class' => 'form-control', 'placeholder' => 'http://projetodesignio.com.br']) !!}
      <div class="input-group-addon">
        <i class="fa fa-link"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('resumo', 'Resumo:') !!}
    <div class="input-group">
      {!! Form::textarea('resumo', null,  ['id' => 'resumo', 'class' => 'form-control', 'cols' => 96]) !!}
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('texto', 'Texto:') !!}
    <div class="input-group">
      {!! Form::textarea('texto', null,  ['id' => 'editor1', 'class' => 'form-control']) !!}
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
    <div class="input-group">
       <div class="input-group-btn">
        <a href="#" target="_blank" class="btn btn-default op_preview"><i class="fa fa-eye"> Pré-visualizar a postagem</i></a>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->


<div class="form-group">
  {!! Form::label('imagem', 'Imagem:') !!}
    <div class="input-group">
      {!! Form::file('imagem', null,  ['id' => 'imagem', 'class' => 'form-control']) !!}
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group" id="listagemcategorias">    
  {!! Form::label('', 'Categorias:') !!}
  @foreach($listaCategorias as $cat)    
    <div class="form-check">
      <div class="checkbox">
          <label>
            {!! Form::checkbox('categorias[]', $cat->id, null, ['id' => 'cat'.$cat->id, 'class' => 'form-check-input flat-blue']) !!} {{ $cat->titulo }}
          </label>
      </div>      
    </div>
  @endforeach
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('nova_cat', 'Adicionar Categoria:') !!}
    <div class="input-group">
      {!! Form::text('nova_cat', null,  ['id' => 'nova_cat', 'class' => 'form-control', 'placeholder' => 'Categoria']) !!}
      <div class="input-group-btn">
        <a href="{{ route('manager.artigo.adicionacategoria') }}" class="btn btn-info op_add"><i class="fa fa-plus"></i></a>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('idioma_id', 'Idioma:') !!}
  {!! Form::select('idioma_id', config('location.model'), $artigo->idioma_id, ['class' => 'form-control', 'required' => 'required']) !!}
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('status', 'Status:') !!}
  {!! Form::select('status', array(1 => 'Ativo', 2 => 'Inativo'), null, ['id' => 'status', 'class' => 'form-control select', 'required' => 'required']) !!}
</div>
<!-- /.form group -->

<script>
$( document ).ready(function() {  
    CKEDITOR.basePath = '{{ asset('assets/plugins/ckeditor/').'/' }}';
    CKEDITOR.plugins.basePath = '{{ asset('assets/plugins/ckeditor/plugins/') }}' + '/';
    CKEDITOR.replace( 'editor1', {
        customConfig: '{{ asset('assets/plugins/ckeditor/config.js') }}'
    });    
  });
  $('body').on('click', '.op_add', function (event){ 
    event.preventDefault();
    var opcaovalue = $("#nova_cat").val(); 
    if(opcaovalue == "") return false;
    $.ajax({
      headers: {
              'X-CSRF-Token': "{{ Session::getToken() }}"
          },
          type: "POST",
          data: { novacategoria: opcaovalue, id_artigo: 0, ajax:'true' },
          url: $(this).attr('href'),      
          success: function(data) {
            $("#listagemcategorias").html(data);
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
              checkboxClass: 'icheckbox_flat-blue',
              radioClass: 'iradio_flat-blue'
            });                    
          },
          error: function() {
                      
          }
    });
  });
  $('body').on('click', '.op_preview', function (event){    
    event.preventDefault();   
    var content = CKEDITOR.instances['editor1'].getData();
    var newWindow = window.open("","_blank");       
    $.ajax({
        headers: {
            'X-CSRF-Token': "{{ Session::getToken() }}"
        },
        type: "POST",
        data: { conteudo: content, ajax:'true' },
        url: '{{ route('manager.artigo.preview') }}',
        success: function(data) {               
          newWindow.document.write(data);
        },
        error: function() {
          alert('Função preview não disponível no momento');
        }
    });                  
  });
</script>