@extends('layouts.app')

@section('content')

    <section class="content-header">
      <h1>
        <i class="fa fa-newspaper-o"></i> Artigos        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-newspaper-o"></i> Artigos</a></li>
        <li class="active">Editar</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
              <h3 class="box-title"><i class="fa fa-edit"></i> Editar</h3>
            </div>
            <div class="box-body">
                    
                @if($errors->any())
                    <ul class="alert">
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

                {!! Form::model($artigo, ['route' => ['manager.artigo.update', $artigo->id], 'method' => 'put', 'files' => true]) !!}


                    @include('artigo._form')                             
                  

                    <div class="box-footer">
                      <div class="box-tools pull-right">
                        <a class="btn btn-app btn-flat" href="{{ URL::previous() }}">
                          <i class="fa fa-reply"></i> Voltar
                        </a>
                        <button type="submit" class="btn btn-app bg-green btn-flat">
                          <i class="fa fa-save"></i> Salvar
                        </button>
                      </div>
                    </div>

                {!! Form::close() !!}

            </div>        
          </div>
        </div>
        <div class="col-md-3">
          <div class="box box-danger">
            <div class="box-header">
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
              <h3 class="box-title"><i class="fa fa-info"></i> Informações</h3>
            </div>
            <div class="box-body no-padding">
              <table class="table table-bordered">
                <tr>
                  <td><big>Adicionado em:</big></td>
                  <td>{{ $artigo->created_at->format('d/m/Y H:i:s') }}</td>
                </tr>
                <tr>
                  <td><big>Alterado em:</big></td>
                  <td>{{ $artigo->updated_at->format('d/m/Y H:i:s') }}</td>
                </tr>
                <tr>
                  <td><big>Alterado por:</big></td>
                  <td>{{ $usuarioCadastro->nome }}</td>
                </tr>
              </table>
            </div>
          </div>
          <div class="box box-danger">
            <div class="box-header">
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
              <h3 class="box-title"><i class="fa fa-image"></i> Imagem</h3>
            </div>
            <div class="box-body no-padding">
                @if($artigo->imagem != null)
                    <img src="{{ asset('upload').'/artigos/'.$artigo->imagem }}" alt="{{ $artigo->titulo }}" class="img-responsive pad">
                @endif
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->      
    </section>

@endsection
