<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
	<channel>
		<atom:link href="{{ url('/artigos/rss') }}" rel="self" type="application/rss+xml" />
		<title><?=$site['site_title'];?></title>
		<link><?=$site['site_url'];?></link>
		<description><?=$site['site_description'];?></description>
		<language>pt-br</language>
		<?php
			foreach($artigos as $artigo){ ?>
			<item>
			  <title><?=$artigo['titulo'];?></title>
			  <link><?=$site['site_url'].'artigo/'.$artigo['slug'];?></link>			  
			  <pubDate><?=$artigo['data']?> GMT</pubDate>
			  <description><?=$artigo['resumo'];?></description>
			  <?php
			  foreach($artigo['categorias'] as $categoria){ ?>			  	
			  	<category domain="<?=$site['site_url'].'categorias/'.$categoria->slug;?>"><?=$categoria->titulo;?></category>
			  <?php } ?>
			</item>
		<?php }	?>		
	</channel>
</rss>