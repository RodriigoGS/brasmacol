@extends('site.layouts.app')

@section('content')

  <section class="intro sec-slide" id="interna_intro">
  	<div class="w-container">
  		<h2 class="intro-titulo">{{ $categoria->titulo }}</h2> <!-- Titulo -->
  		<p>{!! $categoria->descricao !!}</p>
  	</div>
  </section>

  <section id="interna">
  	<div class="w-container">
      <div class="tab w-tabs" data-duration-in="300" data-duration-out="100">
        <div class="tab w-tab-menu interna-rel w-container">

              @foreach( $produtos AS $prod )

                <a class="{{ $prod->slug == $produto->slug ? 'ActiveTabs ' : '' }}tabs w-inline-block w--current tab-link" data-w-tab="Tab-1">{{ $prod->titulo }}</a>
              
              @endforeach
          
          </div>
      </div>

  		<section class="w-tab-content">
  		  <div class="tab-pane1 w-tab-pane w--tab-active" id="Tab-1">
           <div class="w-col w-col-6">
            <div class="slide-produto w-slider" data-animation="slide" data-duration="500" data-infinite="1">
              <div class="mask w-slider-mask">
                  
                  @foreach( $galeria AS $imagem )
                    
                    <div class="w-slide"><img class="img-produto" src="{{ asset('upload/' . $imagem->imagem) }}"></div>
                  
                  @endforeach

              </div>
                <div class="w-slider-arrow-left">
                  <span class="seta w-icon-slider-left"></span>
                </div>
                        
                <div class="w-slider-arrow-right">
                  <span class="seta w-icon-slider-right"></span>
                </div>    
                <span class="w-round w-slider-nav"></span>
              </div>
            </div>

            <div class="w-col w-col-6">
              <h2 class="tit-produto">{{ $produto->titulo }}</h2>
              <p class="desc-produto">{!! $produto->descricao !!}</p>
            </div>
        </div>
  		</section>
    </div>

    <div class="w-container">
  		<div class="interna-videos">
  			<div class="w-container">
  				<h2 class="section-title">Vídeos</h2>
  				<div class="w-col w-col-12">
  					<iframe src="https://www.youtube.com/embed/{{ $produto->link }}" frameborder="0" allowfullscreen></iframe>
  				</div>
  			</div>	
  		</div>

  		<div class="sec-produtos">
    
      <div class="con-produtos w-container">
        <h2 class="section-title">Mais produtos</h2>
      <!-- PRODUTOS RELACIONADOS -->
      <div class="div-produtos">   
        <div class="div-todos">
          <div class="w-row">
            
            @foreach( $categorias AS $cat )

              <div class="bgproduto gavetas-1 w-col w-col-4" style="background-image: url({{ asset('upload/categoria/' . $cat->thumb) }})">
                <a href="{{ route('produtos.index', $cat->slug) }}" style="text-decoration:none; color:#FFF">
                  <h4 class="h3-produtos" data-ix="hover-produtos">{{ $cat->titulo }}</h4>
                </a>
              </div>
            
            @endforeach
                           
          </div>
        </div>
      </div>
    </div>
  	</div><!--Fim container-->
  </section>

@endsection