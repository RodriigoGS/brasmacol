@extends('site.layouts.app')

@section('content')

  <section class="intro sec-slide">
  	<h2 class="intro-titulo">Nossos produtos</h2>
  </section>

  <section id="produtos">
  <div class="w-container">

      <div class="tab w-tabs" data-duration-in="300" data-duration-out="100">
          <div class="tab w-tab-menu interna_produtos-rel w-container">

                @foreach( $categorias AS $cat )

                  <a href="{{ route('produtos.index', $cat->slug) }}" class="{{ $categoria == $cat->slug ? 'ActiveTabs ' : '' }}tabs w-inline-block w--current tab-link">{{ $cat->titulo }}</a>

                @endforeach
                
                <a href="{{ route('produtos.index') }}" class="{{ is_null($categoria) ? 'ActiveTabs' : '' }} tabs w-inline-block tab-link" data-w-tab="Tab-all">Listar todos</a>

            </div>
        </div>
      
      <section class="w-tab-content">
        
        @if( is_null($categoria) )

          <div class="tab-pane1 w-tab-pane w--tab-active" id="Tab-1"> <!-- Tab-->

        @endif

        @foreach( $categorias AS $cat )
          
          @if( !is_null($categoria) )

            <div class="tab-pane1 w-tab-pane {{ $categoria == $cat->slug ? 'w--tab-active' : '' }}" id="Tab-{{ $cat->id }}"> <!-- Tab -->
          
          @endif
            
            @foreach( $cat->produtos AS $produto )
              
              <div class="w-col w-col-4"> <!-- INICIO da estrutura de um produto -->
                <div class="video">
                  <a href="{{ route('produtos.show', ['categoria' => $cat->slug, 'slug' => $produto->slug]) }}">
                    <figure class="catalogo-img produtos" style="background-image: url({{ asset('assets/images/catalogos1.jpg') }});">
                      <figcaption class="catalogo-categoria">
                        <h4 class="titulo">{{ $produto->titulo }}</h4>
                      </figcaption>
                    </figure> 
                  </a>
                </div>
              </div> <!-- FIM da estrutura de um produto -->

            @endforeach
        
          @if( !is_null($categoria) )

            </div> <!-- FIM Tab -->

          @endif

        @endforeach 

        @if( is_null($categoria) )

          </div> <!-- FIM Tab -->

        @endif 
      
      </section>

    </div>  
  	
  </section>

@endsection