@extends('site.layouts.app')

@section('content')

	<section class="intro sec-slide">
		<h2 class="intro-titulo">Vídeos</h2>
	</section>

	<section class="section" id="videos">
		<div class="w-container">

			<div class="tab w-tabs" data-duration-in="300" data-duration-out="100">
		      <div class="tab w-tab-menu interna-rel w-container">
		            
					@foreach( $categorias AS $cat )
					  	<a href="{{ route('videos.index', $cat->slug) }}" class="{{ $categoria == $cat->slug ? 'ActiveTabs ' : '' }}tabs w-inline-block w--current tab-link">{{ $cat->titulo }}</a>
					@endforeach

		            <a href="{{ route('videos.index') }}" class="{{ is_null($categoria) ? 'ActiveTabs' : '' }} tabs w-inline-block tab-link" data-w-tab="Tab-all">Listar todos</a>
		            
		        </div>
		    </div>
			
			<section class="w-tab-content">
				
				@if( is_null($categoria) )

					<div class="tab-pane1 w-tab-pane w--tab-active" id="Tab-1"> <!-- Tab-->

				@endif

				@foreach( $categorias AS $cat )
					
					@if( !is_null($categoria) )

						<div class="tab-pane1 w-tab-pane {{ $categoria == $cat->slug ? 'w--tab-active' : '' }}" id="Tab-{{ $cat->id }}"> <!-- Tab-->
					
					@endif

						@foreach( $cat->videos AS $video )

							<div class="w-col w-col-4"> <!-- INICIO da estrutura de um video -->
				                <div class="video">
				                  <h4 class="uppercase">{{ $video->titulo }}</h4>
				                  <iframe class="embedly-embed" src="https://www.youtube.com/embed/{{ $video->link }}" scrolling="no" frameborder="0" allowfullscreen=""></iframe>
				                </div>
			              	</div> <!-- FIM da estrutura de um video -->

		              	@endforeach
					
					@if( !is_null($categoria) )

						</div> <!-- FIM Tab -->

					@endif

				@endforeach

				@if( is_null($categoria) )

					</div> <!-- FIM Tab -->

				@endif
			
			</section>

		</div>	
	</section>

@endsection