@extends('site.layouts.app')

@section('content')

  <section class="intro sec-slide" id="interna_news">
  	<div class="w-container">
  		<h2 class="intro-titulo">Notícia</h2> <!-- Titulo -->
  	</div>
  </section>

  <section class="news-interna">
    <div class="w-container">
      <h2 class="news-title">{{ $novidade->titulo }}</h2>

      <div class="w-col w-col-12">
        {!! $novidade->texto !!}
      </div>
    </div>
  </section>

@endsection