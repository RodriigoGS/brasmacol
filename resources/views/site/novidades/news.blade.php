@foreach( $novidades AS $novidade )

	<div class="w-col w-col-4"> <!--Inicio de uma noticia -->
		<a href="{{ $novidade->slug }}" class="nov-item"> <!-- Link -->
			<h4 class="uppercase">{{ $novidade->titulo }}</h4>
			<p class="data">{{ $novidade->formatDate($novidade->data, '%B de %Y') }}</p>
			<img class="img-novidades" sizes="(max-width: 479px) 96vw, (max-width: 767px) 29vw, (max-width: 991px) 229.328125px, 299.28125px" src="{{ asset('upload/artigos/' . $novidade->imagem) }}" srcset="{{ asset('upload/artigos/' . $novidade->imagem) }} 500w, {{ asset('upload/artigos/' . $novidade->imagem) }} 800w">
		</a>
	</div> <!--fim de uma noticia -->

@endforeach