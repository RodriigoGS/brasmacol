@extends('site.layouts.app')

@section('content')

	<section class="intro sec-slide">
		<h2 class="intro-titulo">Novidades</h2>
	</section>

	<section class="section" id="novidades">
		<div class="text-center">
			<form action="{{ route('novidades.index') }}">
				<label for="filtro">
					<span class="uppercase pesquisar">Pesquisar</span>
					<input type="text" id="filtro" name="filtro" value="{{ $filtro }}">
				</label>
			</form>
		</div>

		<div class="w-container">
			<div id="novidades-conteudo" class="padding">

				<div class="w-row news-content"> <!--Inicio de uma linha de 3 colunas com noticias -->

					@foreach($novidades AS $novidade)

						<div class="w-col w-col-4"> <!--Inicio de uma noticia -->
							<a href="{{ route('novidades.show', ['slug' => $novidade->slug]) }}" class="nov-item"> <!-- Link -->
								<h4 class="uppercase">{{ $novidade->titulo }}</h4>
								<p class="data">{{ $novidade->formatDate($novidade->data, '%B de %Y') }}</p>
								<img class="img-novidades" sizes="(max-width: 479px) 96vw, (max-width: 767px) 29vw, (max-width: 991px) 229.328125px, 299.28125px" src="{{ asset('upload/artigos/' . $novidade->imagem) }}" srcset="{{ asset('upload/artigos/' . $novidade->imagem) }} 500w, {{ asset('upload/artigos/' . $novidade->imagem) }} 800w">
							</a>
						</div> <!--fim de uma noticia -->

					@endforeach

				</div> <!--Fim de uma linha de 3 colunas com noticias -->
				
				<div class="w-row text-center">
					<div class="w-col w-col-12 text-center">
						<button class="btn transparent load-more" data-paginate="{{ $paginate }}">Carregar mais</button>
					</div>
				</div>

			</div>

			<div class="newsletter">
				<div class="w-row text-center">
					<div class="w-col w-col-12">
						<h3>Assine nossa newsletter e fique por dentro das novidades da BRASMACOL</h3>
				        <form action="{{ route('formulario.newsletter') }}" method="post"> <!-- formulario da newsletter -->
								
							{{ csrf_field() }}

				          	<label for="email-newsletter">
				            	<input type="email" id="email-newsletter" name="email" required="required">
				          	</label>
				          	<button class="btn">@lang('button.button6')</button>
				        </form>
					</div>
				</div>
			</div>
		</div>
	</section>

@endsection

@section('scripts')

<script type="text/javascript">
$('body').on('click', '.load-more', function (event)
{   
	var paginate = $(this).data('paginate');
	var filtro = '{{ $filtro }}';

	if( paginate == '' || paginate == 0 )
		return false;

  	$.ajax({
    	headers: {
            'X-CSRF-Token': "{{ Session::getToken() }}"
    	},
        type: "GET",
        data: { limit: paginate, filtro: filtro, ajax:'true' },
        url: "{{ route('novidades.ajax') }}",
        success: function(data) {
          	$(".news-content").html(data);                   
        }
  	});
});
</script>

@endsection