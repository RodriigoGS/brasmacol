@extends('site.layouts.app')

@section('style')
<style>
	body{
		margin: 0;
	}

	#visualizar-pdf{
		width: 100%;
		height: 200px;
		border: none;
	}
</style>
@endsection

@section('content')

	<iframe id="visualizar-pdf" src="{{ asset('assets/js/vendor/ViewerJS') }}/#{{asset('upload/catalogo/' . $catalogo->catalogo) }}"></iframe>

@endsection

@section('scripts')
	<script type="text/javascript" src="{{ asset('assets/js/vendor/ViewerJS/pdf.js') }}"></script>
@endsection