@extends('site.layouts.app')

@section('content')

	<section class="intro sec-slide">
		<h2 class="intro-titulo">Área restrita</h2>
	</section>

	<section class="section catalogos">
		<!--Verificar se usuário está logado-->
		
		@if( Auth::guard('cliente')->check() )

			<div class="w-container">
				<div class="w-row text-center">
					<h2>Selecione abaixo um dos catálogos para visualização.</h2>
				</div>

				<div class="w-row">
		
					@foreach($catalogos AS $catalogo)

						<div class="w-col w-col-3">
							<a href="{{ route('catalogos.show', ['catalogo' => $catalogo->slug]) }}" target="_blank">
								<figure class="catalogo-img" style="background-image: url({{ asset('upload/catalogo/'. $catalogo->thumb) }});">
									<figcaption class="catalogo-categoria">
										<h4 class="titulo">{{ $catalogo->nome }}</h4>
									</figcaption>
								</figure>	
							</a>
						</div>

					@endforeach

				</div>	
			</div>

		@else

			<div class="w-container">
				<div class="w-col w-col-">
					<!-- Colocar action nesse form! -->
					 <form class="form" action="{{ route('cliente.login') }}" method="post">
						
						{{ csrf_field() }}

			            <label for="password">
			            	<span class="label">@lang('label.label1')</span>
			            	<input type="text" id="password" name="password" value="{{ session('login') ? old('password') : '' }}">
		            	</label>
			            <label for="nome">
			            	<span class="label">@lang('label.label2')</span>
			            	<input type="text" id="nome" name="nome" value="{{ session('login') ? old('nome') : '' }}">
		            	</label>
			            <label for="cargo">
			            	<span class="label">@lang('label.label3')</span>
			            	<input type="text" id="cargo" name="cargo" value="{{ session('login') ? old('cargo') : '' }}">
		            	</label>
		            	
			            <input type="submit" class="btn transparent" />
	          		</form>
				</div>
			</div>

		@endif

	</section>

@endsection