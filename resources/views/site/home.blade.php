@extends('site.layouts.app')

@section('content')

	<!-- Slide -->
	<div class="sec-slide" id="idempresa"><img class="img-slide" sizes="(max-width: 1920px) 100vw, 1920px" src="{{ asset('assets/images/capa-brasmacol.jpg') }}" srcset="{{ asset('assets/images/capa-brasmacol-p-500x77.jpeg') }} 500w, {{ asset('assets/images/capa-brasmacol-p-800x123.jpeg') }} 800w, {{ asset('assets/images/capa-brasmacol-p-1080x166.jpeg') }} 1080w, {{ asset('assets/images/capa-brasmacol-p-1600x246.jpeg') }} 1600w, {{ asset('assets/images/capa-brasmacol.jpg') }} 1920w"></div>
	<!-- Fim Slide -->
	 
	<!-- Produtos -->
    <section class="sec-produtos">
	    
		<div class="con-produtos w-container">
			<h2 class="section-title">@lang('title.title1') {{ session('contato') }}</h2>
		</div>
	      
      	<div class="div-produtos">    
          	<div class="w-tab-content">
              	<div class="div-todos">
                    <div class="w-row">
						
						@foreach( $produtos AS $produto )
					
	                      	<div class="bgproduto w-col w-col-4" style="background-image: url({{ asset('upload/categoria/' . $produto->thumb) }})">
		                      	<a href="{{ route('produtos.index', $produto->slug) }}" style="text-decoration:none; color:#FFF">
		                      		<h4 class="h3-produtos" data-ix="hover-produtos">{{ $produto->titulo }}</h4>
		                  		</a>
	                  		</div>  

						@endforeach

                    </div>
              	</div>           
          	</div>
            <div class="row text-center padding">
              <a href="{{ route('produtos.index') }}" class="btn">@lang('button.button1')</a>  
            </div>
        </div>
	</section>
	 
	<section class="section sec-novidade">
	  
	    <div class="w-container">
	    	<h2 class="section-title">@lang('title.title2')</h2>
	    </div>
	    
	    <div class="cont-novidade w-container">
        	<div class="slidevideos w-slider" data-animation="slide" data-duration="500" data-infinite="1">
		        <div class="w-slider-mask">
		          	<div class="w-slide">
			            <div class="w-row">
			
							@foreach( $videos AS $video )

								<div class="w-col w-col-4">
									<div class="tit-novidade">{{ $video->titulo }}</div>
									<div class="w-embed w-video" style="padding-top: 56.17021276595745%;">
										<iframe class="embedly-embed" src="https://www.youtube.com/embed/{{ $video->link }}" scrolling="no" frameborder="0" allowfullscreen=""></iframe>
									</div>
								</div>

			              	@endforeach

			            </div>
		          	</div>
		        </div>
	        </div>
	    </div>

	    <div class="row text-center padding">
	        <a href="{{ route('videos.index') }}" class="btn">@lang('button.button2')</a>  
	    </div>
	</section>

	<section class="section" id="home-catalogos">

	    <div class="w-row">
	      	<div class="con-produtos w-container">
	          	<h2 class="section-title">@lang('title.title3')</h2>
	          	<p>@lang('text.text1')</p>
	      	</div>
	    </div>

	    <div class="w-container">
	      	<div class="w-row padding">
		        <div class="w-col w-col-6">
		          	<img src="{{ asset('assets/images/home-catalogos.jpg') }}" alt="Catálogos da Brasmacol">
		        </div>
	        	<div class="w-col w-col-6">
	          		<form class="form" action="{{ route('cliente.login') }}" method="post">
						
						{{ csrf_field() }}

			            <label for="password">
			            	<span class="label">@lang('label.label1')</span>
			            	<input type="text" id="password" name="password" value="{{ session('login') ? old('password') : '' }}">
		            	</label>
			            <label for="nome">
			            	<span class="label">@lang('label.label2')</span>
			            	<input type="text" id="nome" name="nome" value="{{ session('login') ? old('nome') : '' }}">
		            	</label>
			            <label for="cargo">
			            	<span class="label">@lang('label.label3')</span>
			            	<input type="text" id="cargo" name="cargo" value="{{ session('login') ? old('cargo') : '' }}">
		            	</label>
			            <input type="submit" class="btn transparent" />
	          		</form>
	        	</div>
	      	</div>
	      	<div class="w-row text-center">
	        	<div class="w-col w-col-12">

	          		<p>@lang('text.text2.1') <strong><a href="#modal" class="link modalAcesso">@lang('button.button3')</a></strong> @lang('text.text2.2')</p>
	        	</div>
	      	</div>
	    </div>
	</section>

	<section class="sec-brasmacol section" id="idbrasmacol">
	    <div class="w-container">
            <div class="w-row">
            	<h2 class="section-title">@lang('title.title4')</h2>
              	<div class="w-col w-col-6">
                	<div class="font14-5 font14-5-2">
                		<p>@lang('text.text4.1')</p>
                  		<p>@lang('text.text4.2')</p>
                  	</div>
              	</div>
              	<div class="w-col w-col-6">
                	<div class="sliderempresa w-slider" data-animation="cross" data-autoplay="1" data-delay="3000" data-duration="500" data-infinite="1">
                  		<div class="w-slider-mask">
                    		<div class="slideempresa1 w-slide"></div>
                  		</div>
                	</div>
              	</div>
            </div>
	        <div class="row text-center padding">
	          	<a href="sobre" class="btn">@lang('button.button4')</a>  
	        </div>
	    </div>
	</section>
	<!-- Fecha sobre a empresa --> 
	 
	<section class="sec-novidade">
		<div class="w-container">
		  	<h2 class="section-title">@lang('title.title5')</h2>
		</div>
		<div class="cont-novidade w-container">
		  	<div class="slidenovidades w-slider" data-animation="slide" data-duration="500" data-infinite="1">
			    <div class="w-slider-mask">
			      <!-- INICIO SLIDE -->
			      	<div class="w-slide">
				        <div class="w-row">  
		              	
							@foreach( $novidades AS $novidade )

								<div class="w-col w-col-4 w-col-small-4">
									<a href="{{ $novidade->slug }}" class="fancybox fancybox.iframe" style="text-decoration:none;">
										<div class="tit-novidade">{{ $novidade->titulo }}</div>
										<div class="datanovidade">{{ $novidade->formatDate($novidade->data, '%B de %Y') }}</div>
										<img class="img-novidades" sizes="(max-width: 479px) 96vw, (max-width: 767px) 29vw, (max-width: 991px) 229.328125px, 299.28125px" src="{{ asset('upload/artigos/' . $novidade->imagem) }}" srcset="{{ asset('upload/artigos/' . $novidade->imagem) }} 500w, {{ asset('upload/artigos/' . $novidade->imagem) }} 800w">
									</a>
								</div>

							@endforeach             

				      	</div> <!-- FECHA ROW --> 
			      	</div> <!-- FECHA SLIDE -->
			      <!-- FIM SLIDE -->
			    </div> <!-- FECHA SLIDE MASK -->
		    	<div class="nav-novidades w-round w-slider-nav w-slider-nav-invert"></div>
		  	</div>
		</div>
		<div class="clear"></div>
	</section>

	<div class="row text-center">
		<a href="novidades" class="btn">@lang('button.button5')</a>  
	</div>

	<!-- Comeca newsletter -->

	<section class="newsletter padding">
	  	<div class="w-container">
		    <div class="w-row text-center">
		      	<div class="w-col w-col-12">
			        <h3>@lang('text.text3')</h3>
			        <form action="{{ route('formulario.newsletter') }}" method="post"> <!-- formulario da newsletter -->
							
						{{ csrf_field() }}

			          	<label for="email-newsletter">
			            	<input type="email" id="email-newsletter" name="email" required="required">
			          	</label>
			          	<button class="btn">@lang('button.button6')</button>
			        </form>
		      	</div>
		    </div>
	  	</div>
	</section>

@endsection