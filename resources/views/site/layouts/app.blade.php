<!DOCTYPE html>
<html data-wf-page="57640c4f8c5f55a428ed1a53" data-wf-site="575ec281ad1958ea3cd9a3fa">
  <head>
    <meta charset="utf-8">

    <title> @yield('title', 'Brasmacol - Componentes Para Móveis') </title>

    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta name="googlebot" content="noindex, nofollow" />
    <meta name="robots" content="noindex, nofollow" />
    <meta name="theme-color" content="#ffffff">
    
    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicons/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicons/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('favicons/manifest.json') }}">
    <link rel="mask-icon" href="{{ asset('favicons/safari-pinned-tab.svg') }}" color="#5bbad5">
    
    {{-- Font --}}
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

    <link href="{{ asset('assets/css/normalize.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/css.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/brasmacol.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/vendor/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/sprite.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet" type="text/css">

    @yield('style')

  </head>
  <body class="body">

    <!-- Topo -->
    <header>
      <div class="sec-topo">
        <div class="menu w-nav" data-animation="default" data-collapse="medium" data-duration="400" id="idhome">
          <div class="w-container">

            <a class="logo-menu w-nav-brand" href="{{ route('home.index') }}"><img class="logo-menu" src="{{ asset('assets/images/logo-250-5.jpg') }}"></a>

            <nav class="bgmenumobile w-nav-menu" role="navigation">
              <a class="nav-menu w-nav-link" href="{{ route('produtos.index') }}">@lang('button.button7')</a>
              <a class="nav-menu w-nav-link" href="{{ route('videos.index') }}">@lang('button.button8')</a>
              <a class="nav-menu w-nav-link" href="{{ route('catalogos.index') }}">@lang('button.button9')</a>
              <a class="nav-menu w-nav-link" href="{{ route('sobre.index') }}">@lang('button.button10')</a>
              <a class="nav-menu w-nav-link" href="{{ route('novidades.index') }}">@lang('button.button11')</a>
              <a class="nav-menu w-nav-link" href="#idcontato">@lang('button.button12')</a>
              <a class="sociais2 w-nav-link" href="https://www.facebook.com/brasmacol/" target="_blank" style="width:40px">b</a>
                &nbsp; &nbsp; &nbsp;
              <a href="http://www.brasmacol.com.br/en" target="_blank"><img class="en2" src="{{ asset('assets/images/en2.jpg') }}"></a>
            </nav>

            <div class="menumobile w-nav-button">
              <div class="w-icon-nav-menu"></div>
            </div> 

          </div>
        </div>
      </div>
    </header>
    <!-- Fim Topo -->

    {{-- MAIN --}}
    
    <main>
      
      @yield('content')

    </main>

    {{-- /MAIN --}}
    
    <section class="sec-endereco" id="idcontato">
      <div class="con-edereco w-container">
        <h2 class="section-title">@lang('title.title6')</h2>
        <div class="w-tabs" data-duration-in="300" data-duration-out="100">
          <div class="tab w-tab-menu">
            <a class="tabs w-inline-block w-tab-link {{ !session('trabalheConosco') ? 'w--current' : '' }}" data-w-tab="Tab 4">
              <h6 class="text-center">@lang('button.button13')</h6>
            </a>
            <a class="tabs w-inline-block w-tab-link" data-w-tab="Tab 2">
              <h6 class="text-center">@lang('button.button14')</h6>
            </a>
            <a class="tabs w-inline-block w-tab-link {{ session('trabalheConosco') ? 'w--current' : '' }}" data-w-tab="Tab 3">
              <h6 class="text-center">@lang('button.button15')</h6>
            </a>
          </div>
          <div class="w-tab-content">
            <div class="tab-content w-tab-pane {{ !session('trabalheConosco') ? 'w--tab-active' : '' }}" data-w-tab="Tab 4">
              <div class="w-row">
                <div class="col-contato col-esq w-col w-col-7">
                  <div class="w-form">
                     <form class="w-clearfix validate" data-name="Email Form" id="email-form" name="email-form" action="{{ route('formulario.faleconosco') }}" method="post">
                      
                      {{ csrf_field() }}

                      <label class="field-form font10" for="nome-2">@lang('label.label2')</label>
                      <input class="input-form w-input" data-name="Nome 2" id="nome" maxlength="256" name="nome" type="text" value="{{ old('nome') }}">
                      <label class="field-form font10" for="email-2">@lang('label.label4')</label>
                      <input class="input-form w-input" data-name="email" id="email-2" maxlength="256" name="email" required type="email" value="{{ old('email') }}">
                      <label class="field-form font10" for="telefone-3" >@lang('label.label5')</label>
                      <input class="input-form w-input telefone" data-name="Telefone" id="telefone" maxlength="256" name="telefone" required type="text" value="{{ old('telefone') }}">
                      <label class="field-form font10" for="mensagem-2">@lang('label.label6')</label>
                      <textarea class="input-form w-input" data-name="Mensagem" id="mensagem" maxlength="5000" name="mensagem">{{ old('mensagem') }}</textarea>
                      <label class="field-form font10" for="nome-2">1 + 2 =</label>
                      <input class="input-form w-input" data-name="soma" id="soma"  name="soma" type="text" style="width:40px;"><br>
                      <input class="btnsubmit w-button" data-wait="aguarde..." type="submit" value="Enviar" wait="aguarde...">
                    
                    </form>
                    <div class="w-form-done">
                      <div>Thank you! Your submission has been received!</div>
                    </div>
                    <div class="w-form-fail">
                      <div>Oops! Something went wrong while submitting the form</div>
                    </div>
                  </div>
                </div>
                <div class="w-col w-col-5">
                  <div class="font10"><strong>+55 (46) 3242-8000</strong>
                    <br><strong>contato@brasmacol.com.br</strong>
                  </div>
                  <div class="clear-contato"></div>
                  <div class="font10">Av. XV de Novembro, 6584, Cristo Rei
                    <br>Chopinzinho - PR - Brasil - CEP&nbsp;85560-000&nbsp;</div>
                </div>
              </div>
            </div>
            <div class="tab-content tab2 w-tab-pane" data-w-tab="Tab 2">
              <div class="w-row">
                <ul class="w-col w-col-4">
                  <li class="font12-1">@lang('title.title7')</li>
                  <li class="end-representantes">Oeste de Santa Catarina e<br>Rio Grande do Sul</li>
                  <li class="representantes">A.Z.A REPRESENTAÇÕES LTDA</li>
                  <li class="tel-representantes">+55 54 3021-0707</li>
                  <li class="clear-representantes"></li>
                  <li class="end-representantes">Minas Gerais e região</li>
                  <li class="representantes">COMPOMOVEIS REPRESENTAÇÕES LTDA</li>
                  <li class="tel-representantes">+55 32 3532 8337 / 32 8888-6777</li>
                  <li class="clear-representantes"></li>
                  <li class="end-representantes">Pernambuco e região</li>
                  <li class="representantes">CASA LIMA REPRESENTAÇÕES</li>
                  <li class="tel-representantes">+55 81 3268-2674 / 81 9606-8842</li>
                  <li class="clear-representantes"></li>
                  <li class="end-representantes">Espirito Santo e região</li>
                  <li class="representantes">RIGONI JR REPRESENTAÇÕES LTDA</li>
                  <li class="tel-representantes">+55 27 98811-7804</li>
                </ul>

                <ul class="w-col w-col-3">
                  <li class="font12-1">@lang('title.title8')</li>
                  <li class="end-representantes">São Paulo</li>
                  <li class="representantes">ANDRÉ MENEZES DE FRANÇA</li>
                  <li class="tel-representantes">+55 46 3242 8000 / 46 99901 6000</li>
                  <li class="clear-representantes"></li>
                  <li class="end-representantes">Paraná e Litoral de<br>Santa Catarina</li>
                  <li class="representantes">IVONEI RALDI</li>
                  <li class="tel-representantes">+55 46 3242 8000 / 46 99972 1805</li>
                  <li class="clear-representantes"></li>
                  <li class="usa-map">
                    <div class="end-representantes">Estados Unidos</div>
                    <div class="representantes">SEBASTIAN CHASKIELBERG</div>
                    <div class="tel-representantes">+1 412216 8622</div><img class="usa-map" src="{{ asset('assets/images/usa-map.png') }}">
                  </li>
                  <li class="clear-representantes"></li>
                </ul>

                <div class="w-col w-col-5"><img class="mapacontato" src="{{ asset('assets/images/mapatransp.png') }}">

                </div>
              </div>
            </div>

            <div class="tab-content w-tab-pane {{ session('trabalheConosco') ? 'w--tab-active' : '' }}" data-w-tab="Tab 3">
              <div class="form-trabalhe w-form">
                <form class="w-clearfix" data-name="Email Form" id="email-form" name="email-form" action="{{ route('formulario.trabalheconosco') }}" method="post" enctype="multipart/form-data">
                  
                  {{ csrf_field() }}                
  
                  <label class="field-form" for="nome">@lang('label.label2')</label>
                  <input class="input-form w-input" data-name="nome" id="nome" maxlength="256" name="nome" type="text" value="{{ old('nome') }}">
                  <label class="field-form" for="email-2">@lang('label.label4')</label>
                  <input class="input-form w-input" data-name="email" id="email-2" maxlength="256" name="email" required type="email" value="{{ old('email') }}">
                  <label class="field-form" for="telefone">@lang('label.label5')</label>
                  <input class="input-form w-input" data-name="telefone" id="telefone" maxlength="256" name="telefone" required type="text" value="{{ old('telefone') }}">
                  <label class="field-form" for="curriculo">@lang('label.label7')</label>
                  <input class="input-form w-input" data-name="curriculo" id="curriculo" maxlength="256" name="curriculo" required type="file">
                  <label class="field-form" for="mensagem">@lang('label.label8')</label>
                  <textarea class="input-form w-input" data-name="mensagem" id="mensagem" maxlength="5000" name="mensagem">{{ old('mensagem') }}</textarea>
                  <label class="field-form font10" for="nome-2">1 + 2 =</label>
                  <input class="input-form w-input" data-name="soma" id="soma"  name="soma" type="text" style="width:40px;"><br>
                  <input class="btnsubmit btntrabalhe w-button" data-wait="aguarde..." type="submit" value="Enviar" wait="aguarde...">
                
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <footer id="footer">
      <div class="w-container">
        <div class="w-col w-col-4">
          <p class="footer-sobre">
            @lang('text.text5')
          </p>
          <span class="icon-logo-brasmacol"></span>
        </div>

        <div class="w-col w-col-4">
          <ul class="footer-menu">
            <li><a class="f-menu-item" href="{{ route('home.index') }}">@lang('button.button16')</a></li>
            <li><a class="f-menu-item" href="{{ route('produtos.index') }}">@lang('button.button7')</a></li>
            <li><a class="f-menu-item" href="{{ route('videos.index') }}">@lang('button.button8')</a></li>
            <li><a class="f-menu-item" href="{{ route('catalogos.index') }}">@lang('button.button9')</a></li>
            <li><a class="f-menu-item" href="{{ route('sobre.index') }}">@lang('button.button10')</a></li>
            <li><a class="f-menu-item" href="{{ route('novidades.index') }}">@lang('button.button11')</a></li>
            <li><a class="f-menu-item" href="">@lang('button.button12')</a></li>
          </ul>
        </div>

        <div class="w-col w-col-4 social">
          <h4>@lang('text.text6')</h4>
          <ul class="social">
            <li class="social-icon youtube"><a href="#" target="_blank"><span class="icon-logo-youtube"></span></a></li>
            <li class="social-icon facebook"><a href="https://www.facebook.com/" target="_blank"><span class="icon-logo-facebook"></span></a></li>
            <li class="social-icon linkedin"><a href="#" target="_blank"><span class="icon-logo-linkedin"></span></a></li>
          </ul>
        </div>
      </div>
    </footer>

    <!-- Modal solicitar acesso -->
    <div id="modal" class="{{ session('soliciteContato') ? 'active' : '' }}" >
      <div id="modalContent">
        <div id="modalClose">
          <span class="btn-close"></span>
        </div>

        <div id="modalForm">
          <h3 class="titulo text-center">Solicitar acesso</h3>

          <form action="{{ route('formulario.soliciteacesso') }}" method="POST" id="solicitar-contato">

              {{ csrf_field() }}

              <input type="text" class="input" name="nome" placeholder="Seu nome:" value="{{ session('soliciteContato') ? old('nome') : '' }}">
              <input type="email" class="input" name="email" placeholder="Seu e-mail:" value="{{ session('soliciteContato') ? old('email') : '' }}">
              <input type="text" class="input" name="telefone" placeholder="Seu telefone:" value="{{ session('soliciteContato') ? old('telefone') : '' }}">
              <input type="text" class="input" name="empresa" placeholder="Empresa:" value="{{ session('soliciteContato') ? old('empresa') : '' }}">
              <input type="text" class="input" name="cargo" placeholder="Cargo/Função:" value="{{ session('soliciteContato') ? old('cargo') : '' }}">
              <textarea name="mensagem" class="input textarea" placeholder="Mensagem:">{{ session('soliciteContato') ? old('mensagem') : '' }}</textarea>
              
              <button class="btn send">Enviar</button>

          </form>

        </div>
      </div>
    </div>

    {{-- Scripts --}}
    
    <script type="text/javascript" src="{{ asset('assets/js/modernizr.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/vendor/jquery-3.2.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/vendor/jquery.mousewheel-3.0.6.pack.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/vendor/jquery.maskedinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/script.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/app.js') }}"></script>

    @yield('scripts')

    <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->

  </body>
</html>