@extends('site.layouts.app')

@section('content')

	<section class="intro sec-slide">
		<div class="w-container">
			<h2 class="intro-titulo">Sobre a empresa</h2> <!-- Titulo -->
		</div>
	</section>

	<section class="section" id="sobre-conteudo">
		<div class="w-container">
			<div class="w-row">
				<div class="w-col w-col-6">
					<figure class="sobre-img" style="background-image: url('{{ asset('assets/images/brasmacol-sobre-empresa.jpg') }}');">
					</figure>
					<figure class="sobre-img" style="background-image: url('{{ asset('assets/images/brasmacol-sobre-empresa-fotografia.jpg') }}');">
					</figure>
				</div>
			
				<div class="w-col w-col-6">
					<p>Fundada em 28 de janeiro de 1992, a BRASMACOL atua no segmento de componentes em MDF para móveis, atendendo as necessidades das indústrias moveleiras no Brasil e no exterior.</p>
					<p>Sempre aberta ao crescimento e inovações, hoje a BRASMACOL possui a maior estrutura do Brasil em sua categoria, atende pedidos de indústrias de todo o país, em função principalmente da seriedade com que trabalha e do excelente produto que oferece.</p>
				</div>
			</div>

			<div class="w-row">
	        	<h2 class="section-title">Tradição no segmento</h2>
	     	</div>
			
			<div class="w-row">
				<div class="w-col w-col-6 lg-direita">
					<img src="{{ asset('assets/images/brasmacol-sobre-historia.jpg') }}" alt="História da Brasmacol">
				</div>

				<div class="w-col w-col-6 historia-texto">
					<p>Há 25 anos a BRASMACOL atua no segmento de componentes em MDF para móveis, atendendo as necessidades das indústrias moveleiras no Brasil e no exterior.</p>
					<p>Tendo como base a qualidade, alta tecnologia, profissionalismo, competência e atualização, desenvolve toda sua produção com foco na satisfação de seus clientes e colaboradores.</p> 
				</div>
			</div>

			<div class="w-row">
				<h2 class="section-title">Tecnologia que gera competitividade</h2>
			</div>

			<div class="w-row">
				<div class="w-col w-col-6">
					<ul class="sobre-lista">
						<li>
							<span class="icon"><svg><use xlink:href="{{ asset('assets/images/icons.svg#pincel') }}"> </use></svg></span>
							<span class="texto">Departamento de design da empresa;</span>
						</li>

						<li>
							<span class="icon"><svg><use xlink:href="{{ asset('assets/images/icons.svg#network') }}"> </use></svg></span>
							<span class="texto">Equipamentos de alta tecnologia <br>e matrizaria própria;</span>
						</li>

						<li>
							<span class="icon"><svg><use xlink:href="{{ asset('assets/images/icons.svg#lapis-regua') }}"></use></svg></span>
							<span class="texto">Projetos customizados;</span>
						</li>

						<li>
							<span class="icon"><svg><use xlink:href="{{ asset('assets/images/icons.svg#acabamento') }}"> </use></svg></span>
							<span class="texto">Acabamentos diferenciados;</span>
						</li>

						<li>
							<span class="icon"><svg><use xlink:href="{{ asset('assets/images/icons.svg#custo') }}"></use></svg></span>
							<span class="texto">Relação custo-benefício;</span>
						</li>
					</ul>
				</div>

				<div class="w-col w-col-6 padding padding-horizontal">
					<p>A BRASMACOL possui um moderno parque fabril com máquinas atualizadas que permitem o desenvolvimento completo de molduras, portas e gavetas</p>
					<p>Dispomos de ferramentaria interna, que permite agilidade e customização das molduras para usinagem. Recobrimos o MDF usinado nas moldureiras com papéis FX, FF, PET ou PVC desenvolvidos de acordo com os projetos dos clientes</p>
					<p>Temos tecnologia para cortes, furação, aplicação de cavilhas, acabamento de topo e montagem das portas e quadros.</p>
				</div>	
			</div>
			
			<div class="w-row">
				<h2 class="section-title">Qualidade certificada</h2>
			</div>

			<div class="w-row">
				<div class="w-col w-col-6">
				<p>A Brasmacol na busca do aperfeiçoamento constante investiu na melhoria de seus processos e no atendimento dos requisitos de seus clientes através da certificação ISO 9001, conquistada em 2003. Desde então, trabalhando para o atendimento dos requisitos normativos, a Brasmacol aprimorou seus processos visando garantir a satisfação das partes interessadas.</p>
				<p>Usado como ferramenta de gerenciamento e tendo como meta a melhoria continua de seus processos o “Sistema de Qualidade Brasmacol” (SQB) proporciona avanços significativos em toda a organização e, consequentemente, a melhoria nas relações com os clientes, através do atendimento de seus requisitos.</p>
				<p>Sempre atenta às mudanças, a Brasmacol está se preparando para a migração da nova versão da norma ISO 9001:2015 que entrará em vigor a partir de setembro de 2018.</p>
				</div>

				<div class="w-col w-col-6">
				<div class="image-contain">
					<span class="icon-iso9001"></span>	
				</div>
				</div>
			</div>

			<div class="w-row">
				<h2 class="section-title">Conheça nossa história</h2>
			</div>

			<div class="w-row sobre-timeline">
				<div class="btn-group text-center">
						
					@foreach( $empresas AS $empresa )
						
						<button class="timeline-btn btn transparent {{ $empresa->ano == $empresas->last()->ano ? 'active' : '' }}" data-target="#historia{{ $empresa->ano }}">{{ $empresa->ano }}</button>

					@endforeach
					
				</div>
			</div>

			@foreach( $empresas AS $empresa )
				
				<div class="historia_tab w-col w-col-12 {{ $empresa->ano == $empresas->last()->ano ? 'active' : '' }}" id="historia{{ $empresa->ano }}">
					{!! $empresa->descricao !!}
				</div>

			@endforeach
			
		</div>
	</section>

@endsection