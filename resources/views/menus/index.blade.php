@extends('layouts.app')

@section('content')
    <section class="content-header">
      <h1>
        <i class="fa fa-list"></i> Menus        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-list"></i> Menus</a></li>
        <li class="active">Listar</li>
      </ol>
      @if( Auth::user()->temRota('manager.menus.create') )        
        <a class="btn btn-block btn-primary btn-sm btn-adicionar" href="{{ url('manager/menus/create') }}">
          <i class="fa fa-edit"></i> Adicionar
        </a>         
      @endif
    </section>
   @if (session('status')) 
      <section class="content-header">
        <div class="alert alert-{{ session('status') }}">
            {{ session('retornomensagem') }}
        </div>
      </section>
    @endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">        
          <div class="box">

            <!-- /.box-header -->
            <div class="box-body">
               <table id="datatable" class="table table-bordered table-hover row-border">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Item</th>
                  <th>Status</th>
                  <th>Ações</th>                  
                </tr>
                </thead>
                <tbody>
                @foreach($menus as $menu)            
                <tr>
                    <td>{{ $menu->id }}</td>
                    <td>{{ ($menu->pai == null)? $menu->descricao : $menu->parent()->first()->descricao.' - '.$menu->descricao }}</td>
                    <td><span class="badge {{ ($menu->status == '1' ? 'bg-green' : 'bg-yellow') }}">{{ $controlador->exibeStatus($menu->status) }}</span></td>                    
                    <td>
                        <a class="btn btn-app no-margin" href="{{ url('manager/menus/edit', $menu->id) }}">
                          <i class="fa fa-edit"></i> Editar
                        </a>
                        <a class="btn btn-app no-margin" href="{{ url('manager/menus/delete', $menu->id) }}">
                          <i class="fa fa-trash-o"></i> Excluir
                        </a>
                    </td>
                </tr>
                @endforeach                
                </tbody>               
              </table>              
            </div>
            <!-- /.box-body -->           
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection