<div class="form-group">
  {!! Form::label('pai', 'Menu Pai:') !!}
  {!! Form::select('pai', $itens, null, ['id' => 'pai', 'class' => 'form-control']) !!} 
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('descricao', 'Título:') !!}
    <div class="input-group">
      {!! Form::text('descricao', null,  ['id' => 'descricao', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'Título']) !!}
      <div class="input-group-addon">
        <i class="fa fa-header"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>

<div class="form-group">
  {!! Form::label('rota', 'Rota:') !!}
    <div class="input-group">
      {!! Form::text('rota', null,  ['id' => 'rota', 'class' => 'form-control', 'placeholder' => 'manager.item.index / manager.item.create / manager.item.edit']) !!}
      <div class="input-group-addon">
        <i class="fa fa-link"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>

<div class="form-group">  
  <label for="icone">Icone (<small><a href="http://fontawesome.io/icons/" target="_blank">referências</a></small>)</label>
    <div class="input-group">
      {!! Form::text('icone', null,  ['id' => 'icone', 'class' => 'form-control', 'required' => 'required', 'placeholder' => 'fa-something']) !!}
      <div class="input-group-addon">
        <i class="fa fa-image"></i>
      </div>
    </div>
    <!-- /.input group -->
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('grupo_ordem', 'Grupo/Conjunto do Dropdown:') !!}
  {!! Form::selectRange('grupo_ordem', 1, 50, (isset($menu->grupo_ordem)? $menu->grupo_ordem : null),   ['id' => 'grupo_ordem', 'class' => 'form-control']) !!}
</div>
<!-- /.form group -->

<div class="form-group">
  {!! Form::label('ordem_interna', 'Ordem interna (subgrupo):') !!}
  {!! Form::selectRange('ordem_interna', 0, 50, (isset($menu->ordem_interna)? $menu->ordem_interna : null),   ['id' => 'ordem_interna', 'class' => 'form-control']) !!}
</div>
<!-- /.form group -->

<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::select('status', array(1 => 'Ativo', 2 => 'Inativo'), null, ['id' => 'status', 'class' => 'form-control', 'required' => 'required']) !!}
</div>
<!-- /.form group -->