<?php

return [

	'button1' => 'View all products',
	'button2' => 'See all videos',
	'button3' => 'Request',
	'button4' => 'Meet the company',
	'button5' => 'See more news',
	'button6' => 'Sign',
	'button7' => 'Products',
	'button8' => 'Videos',
	'button9' => 'Catalogs',
	'button10' => 'Company',
	'button11' => 'News',
	'button12' => 'Contact',
	'button13' => 'Contact Us',
	'button14' => 'Business Contacts',
	'button15' => 'Work With Us',
	'button16' => 'Home'

];