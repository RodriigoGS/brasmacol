<?php

return [

	'text1' => 'Brasmacol has a modern industrial park with machines that allow the complete development of frames, doors and drawers.',
	'text2' => [
		1 => 'Do not have access yet?',
		2 => 'access to our team.'
	],
	'text3' => 'Subscribe to our newsletter and stay on the news of BRASMACOL',
	'text4' => [
		1 => 'For 25 years, BRASMACOL has been working in the segment of MDF components for furniture, meeting the needs of the furniture industries in Brazil and abroad.',
		2 => 'Based on quality, high technology, professionalism, competence and updating, it develops all its production focused on the satisfaction of its customers and employees.'
	],

];