<?php

return [

	'text1' => 'A Brasmacol possui um moderno parque fabril com máquinas que permitem o desenvolvimento completo de molduras, portas e gavetas.',

	'text2' => [
		1 => 'Ainda não possui acesso ?',
		2 => 'acesso para nossa equipe.'
	],

	'text3' => 'Assine nossa newsletter e fique por dentro das novidades da BRASMACOL',

	'text4' => [
		1 => 'Há 25 anos a BRASMACOL atua no segmento de componentes em MDF para móveis, atendendo as necessidades das indústrias moveleiras no Brasil e no exterior.',
		2 => 'Tendo como base a qualidade, alta tecnologia, profissionalismo, competência e atualização, desenvolve toda sua produção com foco na satisfação de seus clientes e colaboradores.'
	],
	'text5' => '<strong>Sobre a Brasmacol:</strong> Há mais de duas décadas, a Brasmacol é referência em componentes para móveis, atendendo as necessidades das grandes indústrias moveleiras nacionais no desenvolvimento completo de molduras, portas e gavetas.',
	'text6' => 'Curta a <strong>Brasmacol</strong> nas <strong>Redes Sociais!</strong>'

];