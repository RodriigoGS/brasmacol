<?php

return [

	'label1' => 'CPF ou CNPJ',
	'label2' => 'Nome',
	'label3' => 'Cargo/Função',
	'label4' => 'E-mail',
	'label5' => 'Telefone',
	'label6' => 'Mensagem',
	'label7' => 'Curriculo',
	'label8' => 'Resumo'

];