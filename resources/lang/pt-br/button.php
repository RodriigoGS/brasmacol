<?php

return [

	'button1' => 'Ver todos os produtos',
	'button2' => 'Ver todos os vídeos',
	'button3' => 'Solicite',
	'button4' => 'Conheça a empresa',
	'button5' => 'Ver mais notícias',
	'button6' => 'Assinar',
	'button7' => 'Produtos',
	'button8' => 'Vídeos',
	'button9' => 'Catálogos',
	'button10' => 'Empresa',
	'button11' => 'Novidades',
	'button12' => 'Contato',
	'button13' => 'Fale Conosco',
	'button14' => 'Contatos Comerciais',
	'button15' => 'Trabalhe Conosco',
	'button16' => 'Home'
];