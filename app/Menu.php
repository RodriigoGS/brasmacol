<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'manager_menus';
    protected $fillable = ['pai', 'descricao', 'rota', 'icone','grupo_ordem','ordem_interna', 'status', 'id_usuario'];


    public function parent()
    {
    	return $this->belongsTo(Menu::class, 'pai');
    }

    public function children()
    {
    	return $this->hasMany('Menu', 'pai');
    }


    public function perfil()
    {
        return $this->belongsToMany('App\Perfil', 'manager_perfil_menu', 'id_menu', 'id_perfil');
    }



    public function opcoesSelect()
    {    	
    	$data = $this->all();
    	$opcoes = array(); 
    	$opcoes[0] = "Nenhum";

		foreach ($data as $menu) {
			$opcoes[$menu->id] = ($menu->pai == null)? $menu->descricao.' (Grupo '.$menu->grupo_ordem.')' : $menu->parent()->first()->descricao.' - '.$menu->descricao.' (Grupo '.$menu->grupo_ordem.')';
			
		}	
    	return $opcoes;
    }


    public function opcoesSelectOrdemGrupo()
    {

    }
}
