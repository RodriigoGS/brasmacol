<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogPesquisa extends Model
{
    protected $table = 'manager_log_pesquisas';
    public $timestamps = false;
}
