<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SoliciteAcesso extends Model
{
    /**
	 * [$fillable description]
	 * @var [type]
	 */
    protected $fillable = [
    	'nome','email', 'telefone', 'empresa', 'cargo', 'mensagem', 'status', 'lido'
	];
}
