<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contato extends Model
{    
	/**
	 * [$fillable description]
	 * @var [type]
	 */
    protected $fillable = ['nome','email', 'telefone', 'mensagem', 'status', 'lido'];
}
