<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Sitemap extends Model
{
    public function comboPrioridades()
    {
    	$prioridades = array();
    	$prioridades['null'] = 'Nenhum';
    	for($i=1; $i < 10; $i++){
    		$prioridades[''.($i/10).''] = ($i/10);
    		
    	}
    	$prioridades['1.0'] = 1.0;
    	return $prioridades;
    }

    public function comboFrequencias()
    {
    	$frequencias = array(
    		'null' => 'Nenhum',
    		'always' => 'Always',
			'hourly' => 'Hourly',
			'daily' => 'Daily',
			'weekly' => 'Weekly',
			'monthly' => 'Monthly',
			'yearly' => 'Yearly',
			'never' => 'Never'
    	);
    	return $frequencias;
    }


    public function getLocalizacoes()
    {
    	$entradas = Sitemap::all();
    	$localizacoes = array();
    	foreach ($entradas as $entrada) {
    		$item = array();
    		$item['loc'] = $entrada->loc;    		
    		$filename = __DIR__ .'/Http/Controllers/'.$entrada->arquivo;
    		if (File::exists($filename)){
			    
			    $timestamp = File::lastModified($filename);
			    if ($timestamp !== false) $item['lastmod'] = date('Y-m-d', $timestamp);
			}
    		$item['changefreq'] = $entrada->changefreq;
			$item['priority'] = $entrada->priority;

			$localizacoes[] = $item;    		
    	}
    	return $localizacoes;
    }
}
