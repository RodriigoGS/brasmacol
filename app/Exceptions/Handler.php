<?php

namespace App\Exceptions;

use App\LogPesquisa;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
        if(method_exists($e, 'getStatusCode')){
            if($e->getStatusCode() >= 400) $this->logPesquisa(Request::url(), $e->getStatusCode());
        }
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        return parent::render($request, $e);
    }

    /**
     * Versão do log de pesquisas para paginas nao encontradas
     * @param string $pagina 
     * @param int $httpStatusCode      
     */
    private function logPesquisa($pagina, $httpStatusCode)
    {        
        $logbusca = new LogPesquisa();
        $logbusca->resultados = $httpStatusCode;
        $logbusca->busca = $pagina;

        if(Auth::check()){ //criar validacoes para cada guard de autenticacao, este e usuario do manager
            $logbusca->id_usuario = Auth::user()->id;
            $logbusca->perfil = Auth::user()->perfil->descricao;        
        }
        $logbusca->save();
    }
}
