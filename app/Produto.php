<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'titulo', 'descricao', 'link', 'slug', 'status', 'idioma_id', 'galeria_id', 'produto_categoria_id'
	];

    /**
	 * Get the category that owns the product.
	 */
	public function categorias()
	{
        return $this->belongsToMany('App\Categoria', 'produto_categorias', 'produto_id', 'categoria_id');
	}

	/**
     * Get the language record associated with the product.
     */
    public function idioma()
    {
        return $this->belongsTo('App\Idioma');
    }

    /**
     * Get the language record associated with the product.
     */
    public function galeria()
    {
        return $this->belongsTo('App\Galeria');
    }

    /**
     * [publish description]
     * @return [type] [description]
     */
    public static function publish()
    {
        return Produto::where('status', 1)->get();
    }

    /**
     * [categoriasProdutos description]
     * @return [type] [description]
     */
    public static function categoriasProdutos()
    {
        return Produto::select()
                ->join('produto_categorias', 'produtos.id', '=', 'produto_categorias.produto_id')
                ->join('categorias', 'categorias.id', '=', 'produto_categorias.categoria_id')
                ->get();
    }

    /**
     * [scopeIdioma description]
     * @param  [type] $query  [description]
     * @param  [type] $idioma [description]
     * @return [type]         [description]
     */
    public function scopeIdioma($query, $idioma = null)
    {
        /**
         * [$idioma_id description]
         * @var [type]
         */
        $idioma_id = $idioma ? $idioma : config('location.modelLocale')[ config('app.locale') ];

        /**
         * Return Scope
         */
        return $query->where('idioma_id', $idioma_id);
    }
}
