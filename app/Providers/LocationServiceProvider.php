<?php

namespace App\Providers;

use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

use Schema;

use App\Idioma;

class LocationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {   
        /**
         * Verify LOCATION.LOCALES in start application
         */
        if( session()->has('idiomas') || Schema::hasTable('idiomas') )
        {
            /**
             * Setting SESSION - IDIOMAS
             */
            if( !session()->has('idiomas') )
                session(['idiomas' => true]);

            /**
             * Check CONFIG - LOCATION
             */
            if( empty(config('location.locales')) )
            {
                /**
                 * PARAMETERS
                 * @var [type]
                 */
                $modelId        = [];
                $modelLocale    = [];
                $modelFormat    = [];

                /**
                 * [$idiomas description]
                 * @var [type]
                 */
                $idiomas = Idioma::select('id', 'locale', 'format', 'nome')->get();

                /**
                 * Config IDIOMA model
                 * @var [type]
                 */
                foreach ($idiomas as $key => $value) 
                {
                    $modelId[ $value->id ] = $value->nome;
                    $modelLocale[ $value->locale ] = $value->id;
                    $modelFormat[ $value->locale ] = $value->format;
                }

                /**
                 * Setting Config
                 */
                config([
                    'location.locales'      => $idiomas->pluck('locale')->toArray(),
                    'location.model'        => $modelId,
                    'location.modelLocale'  => $modelLocale
                ]);
            }

            /**
             * Setting LOCALE
             */
            if( in_array($this->app->request->segment(1), config('location.locales')) )
            {
                config([
                    'app.locale' => $this->app->request->segment(1),
                    'location.locale' => $this->app->request->segment(1),
                    'location.format' => $modelFormat[ $this->app->request->segment(1) ]
                ]);

            } else 
            {
                /**
                 * DEFAULT
                 */
                config([
                    'app.locale' => 'pt-BR',
                    'location.locale' => '/',
                    'location.format' => 'pt_BR'
                ]);
            }
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
