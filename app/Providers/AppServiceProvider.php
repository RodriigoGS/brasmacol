<?php

namespace App\Providers;

use App\Contato;
use App\Menu;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layouts.app', function($view) {            
            if(auth()->user() != null){                                
                $menusacesso = Menu::join('manager_perfil_menu', 'manager_perfil_menu.id_menu', '=', 'manager_menus.id')
                                    ->where('manager_perfil_menu.id_perfil',auth()->user()->id_perfil)
                                    ->where('manager_menus.status',1)
                                    ->orderBy('grupo_ordem', 'asc')
                                    ->orderBy('ordem_interna', 'asc')
                                    ->orderBy('pai', 'asc')
                                    ->get();
                                                                                    
                view()->share('menusacesso', $menusacesso);  
                session(array('menus_usuario' => $menusacesso));

                $mensagemContato = Contato::select(DB::raw('count(id) as nao_lidos'))
                                             ->where('status', '<>', 3)
                                             ->where('lido', '=', 0)
                                             ->first();
                $mensagens = Contato::select(DB::raw('id,nome,created_at'))
                                             ->where('status', '<>', 3)
                                             ->where('lido', '=', 0)
                                             ->get();                                           
                view()->share('mensagemContato', $mensagemContato);
                view()->share('mensagens', $mensagens);
            }
            
        });
            
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
