<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdutoCategoria extends Model
{
    /**
     * [$table description]
     * @var string
     */
    protected $table = 'produto_categorias';

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'categoria_id', 'produto_id'
	];

    /**
     * Get the products for the Category.
     */
    public function produtos()
    {
        return $this->hasMany('App\Produto');
    }

    /**
     * [categorias description]
     * @return [type] [description]
     */
    public function categorias()
    {
        return $this->belongsToMany('App\Categoria');
    }
}
