<?php

namespace App;

use App\Perfil;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuario extends Authenticatable
{
    protected $table = 'manager_usuarios';
    protected $fillable = [
    	'nome','senha','usuario','email', 'id_perfil', 'imagem'
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];



    public function getAuthPassword() {
    	return $this->senha;
	}


    public function perfil()
    {
        return $this->hasOne('App\Perfil', 'id', 'id_perfil');
    }

    /**
     * Verifica se o usuario possui acesso a uma rota, baseado no seu perfil
     * @param string $rota nome da rota, ver arquivo de rotas e rotas nomeadas do Laravel
     * @return boolean true se possui acesso, false do contrario
     */
    public function temRota($rota)
    {        
        $possuiRota = Perfil::where('id', $this->id_perfil)->first()->menu()->where('rota', $rota)->first();        
        if($possuiRota != null) return true;
        return false;

    }    


}
