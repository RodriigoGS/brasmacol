<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Arquivo extends Model
{
    protected $table = 'manager_arquivos';
    protected $fillable = ['titulo','arquivo','extensao', 'status'];
}
