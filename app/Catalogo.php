<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catalogo extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'nome', 'catalogo', 'thumb', 'slug', 'status', 'idioma_id'
	];

	/**
     * [publish description]
     * @return [type] [description]
     */
    public static function publish()
    {
        return Catalogo::where('status', 1)->get();
    }

    /**
     * Get the language record associated with the catalog.
     */
    public function idioma()
    {
        return $this->hasOne('App\Idioma');
    }

    /**
     * [scopeSlug description]
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeSlug( $query, $slug )
    {
        return $query->where('slug', $slug);
    }

    /**
     * [scopeIdioma description]
     * @param  [type] $query  [description]
     * @param  [type] $idioma [description]
     * @return [type]         [description]
     */
    public function scopeIdioma($query, $idioma = null)
    {
        /**
         * [$idioma_id description]
         * @var [type]
         */
        $idioma_id = $idioma ? $idioma : config('location.modelLocale')[ config('app.locale') ];

        /**
         * Return Scope
         */
        return $query->where('idioma_id', $idioma_id);
    }
}
