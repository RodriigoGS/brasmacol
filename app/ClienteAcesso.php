<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClienteAcesso extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'cliente_id', 'ip'
	];

	/**
     * Get the cliente that owns the acessos.
     */
    public function cliente()
    {
        return $this->belongsTo('App\Cliente');
    }
}
