<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UsuarioRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required|max:255',
            'usuario' => 'required|max:255|unique:manager_usuarios',
            'email' => 'required|email|max:255|unique:manager_usuarios',
            'senha' => 'required|min:6|confirmed',
            'imagem' => 'sometimes|required|image|max:2000'
        ];
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nome.required'=>'Campo nome é obrigatório',
            'email.required'=>'Campo e-mail é obrigatório',
            'email.unique'=>'Já existe este e-mail em nossa base de dados',
            'usuario.unique'=>'Já existe este usuário em nossa base de dados',
            'usuario.required'=>'Campo login é obrigatório',
            'senha.required' => 'Campo senha é obrigatório',
            'senha.min'=>'Deve possuir no mínimo 6 caracteres',
            'senha.confirmed'=>'As senhas não conferem',
            'imagem.image' => 'Formato de imagem deve ser JPG, PNG ou GIF',
            'imagem.max' => 'Imagem não pode ser maior que 2Mb'
        ];
    }
}
