<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Video;

class VideoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo'    => 'required|max:255',
            'link'      => 'required|max:255',
            'status'    => 'required|min:1|numeric', 
            'idioma_id' => 'required|numeric|exists:idiomas,id'
        ];
    }

    /**
     * [videoCreate description]
     * @return [type] [description]
     */
    public function videoCreate()
    {
        /**
         * Create VIDEO
         * @var [type]
         */
        $video = Video::create(

            $this->only(['titulo', 'link', 'status', 'idioma_id'])

        );

        /**
         * Sync Categorias
         */
        $video->categorias()->sync( $this->categorias ? $this->categorias : [] );

        /**
         * Return
         * @var [video]
         */
        return $video;
    }

    /**
     * [videoUpdate description]
     * @return [type] [description]
     */
    public function videoUpdate( $id )
    {
        /**
         * GET Video
         * @var [type]
         */
        $video = Video::find( $id );

        /**
         * Update VIDEO
         * @var [type]
         */
        $video->update(

            $this->only(['titulo', 'link', 'status', 'idioma_id'])

        );

        /**
         * Sync Categorias
         */
        $video->categorias()->sync( $this->categorias ? $this->categorias : [] );

        /**
         * Return
         * @var [video]
         */
        return $video;
    }
}
