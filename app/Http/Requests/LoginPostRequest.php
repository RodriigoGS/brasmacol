<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class LoginPostRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
         
            'email' => 'required',
            'senha' => 'required|min:6' 

        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.required' => 'Nome de usuário ou E-mail é obrigatório',
            'senha.required'  => 'Senha é obrigatória e precisa ter no mínimo 6 caracteres',
            'senha.min'  => 'Senha precisa ser maior que 6 caracteres'
        ];
    }
}
