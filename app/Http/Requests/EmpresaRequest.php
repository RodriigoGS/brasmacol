<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

use App\Empresa;

class EmpresaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ano'       => 'required|min:4|max:4',
            'descricao' => 'required',
            'status'    => 'required|min:1|numeric',
            'idioma_id' => 'required|numeric|exists:idiomas,id'
        ];
    }

    /**
     * [empresaCreate description]
     * @return [type] [description]
     */
    public function empresaCreate()
    {
        /**
         * Update Empresa
         */
        $empresa = Empresa::create(

            $this->only(['ano', 'descricao', 'status', 'idioma_id'])

        );

        return $empresa;
    }

    /**
     * [empresaUpdate description]
     * @return [type] [description]
     */
    public function empresaUpdate( $id )
    {
        /**
         * Find Empresa
         * @var [type]
         */
        $empresa = Empresa::find( $id );

        /**
         * Update Empresa
         */
        $empresa->update(

            $this->only(['ano', 'descricao', 'status', 'idioma_id'])

        );

        return $empresa;
    }
}
