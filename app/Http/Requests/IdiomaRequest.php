<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

use App\Idioma;

class IdiomaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required',
            'format' => 'required|min:2|max:5',
            'locale' => 'required|min:2|max:5',
            'status' => 'required|min:1|numeric' 
        ];
    }

    /**
     * [idiomaCreate description]
     * @return [type] [description]
     */
    public function idiomaCreate()
    {
        $idioma = Idioma::create(

            $this->only(['nome', 'format', 'locale', 'status'])

        );

        return $idioma;
    }

    /**
     * [idiomaUpdate description]
     * @return [type] [description]
     */
    public function idiomaUpdate( $id )
    {
        $idioma = Idioma::find( $id )->update(

            $this->only(['nome', 'format', 'locale', 'status'])

        );

        return $idioma;
    }
}
