<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

use App\Repositories\ImageRepository;

use App\Categoria;

class CategoriaRequest extends Request
{
    /**
     * [$ImageRepository description]
     * @var [type]
     */
    protected $ImageRepository;

    /**
     * [__construct description]
     * @param ImageRepository $ImageRepository [description]
     */
    public function __construct(ImageRepository $ImageRepository)
    {
        $this->ImageRepository = $ImageRepository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo'    => 'required',
            'tipo'      => 'sometimes|required',
            'descricao' => 'sometimes|required',
            'thumb'     => 'sometimes|mimetypes:image/png,image/jpeg,image/pjpeg,image/gif',
            'status'    => 'required|min:1|numeric',
            'idioma_id' => 'required|numeric|exists:idiomas,id',
        ];
    }

    /**
     * [categoriaCreate description]
     * @return [type] [description]
     */
    public function categoriaCreate()
    {
        /**
         * Create Categoria
         * @var [type]
         */
        $categoria = Categoria::create([
            'titulo'        => $this->titulo,
            'slug'          => str_slug($this->titulo, '-'),
            'tipo'          => $this->tipo,
            'status'        => $this->status,
            'id_usuario'    => Auth::user()->id,
            'idioma_id'     => $this->idioma_id
        ]);

        /**
         * Verify DESCRICAO
         */
        if( $this->descricao )
            $categoria->update(['descricao' => $this->descricao]);

        /**
         * Verify FILE request
         */
        if( $this->hasFile('thumb') )
        {
            /**
             * [$proccessImage description]
             * @var [type]
             */
            $proccessImage = $this->ImageRepository->imageModel( 'thumb', 'categoria', 310, 310 );

            /**
             * Verify categoria AND upload image
             */
            if( $categoria && $proccessImage != false )
            {
                /**
                 * Insert IMAGE to CATEGORIA
                 */
                $categoria->update(['thumb' => $proccessImage]);
            }
        } 
        
        /**
         * [$categorias description]
         * @var [type]
         */
        $categorias = Categoria::select('id', 'titulo', 'slug')
                                ->where('tipo', $this->tipo)
                                ->where('status', 1)
                                ->get();

        return $categorias;
    }

    /**
     * [categoriaUpdate description]
     * @return [type] [description]
     */
    public function categoriaUpdate( $id )
    {
        /**
         * Find Categoria
         * @var [type]
         */
        $categoria = Categoria::find( $id );

        /**
         * Update categoria
         */
        $categoria->update(

            $this->only(['titulo', 'status', 'idioma_id']) 
            +
            [
                'slug'          => str_slug($this->titulo, '-'),
                'id_usuario'    => Auth::user()->id
            ]

        );

        /**
         * Verify DESCRICAO
         */
        if( $this->descricao )
            $categoria->update(['descricao' => $this->descricao]);


        /**
         * Verify FILE request
         */
        if( $this->hasFile('thumb') )
        {
            /**
             * [$proccessImage description]
             * @var [type]
             */
            $proccessImage = $this->ImageRepository->imageModel( 'thumb', 'categoria', 310, 310 );

            /**
             * Verify categoria AND upload image
             */
            if( $categoria && $proccessImage != false )
            {
                /**
                 * DELETE thumb from UPDATE
                 */
                if( !empty($categoria->thumb) )
                    \File::delete(public_path() . '/upload/categoria/' . $categoria->thumb);

                /**
                 * Insert IMAGE to CATEGORIA
                 */
                $categoria->update(['thumb' => $proccessImage]);
            }
        } 

        return $categoria;
    }
}
