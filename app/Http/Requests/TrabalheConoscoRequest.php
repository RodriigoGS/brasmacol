<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Repositories\ImageRepository;

use Storage;

use App\TrabalheConosco;

class TrabalheConoscoRequest extends Request
{
    /**
     * [$ImageRepository description]
     * @var [type]
     */
    protected $ImageRepository;

    /**
     * [__construct description]
     * @param ImageRepository $ImageRepository [description]
     */
    public function __construct(ImageRepository $ImageRepository)
    {
        $this->ImageRepository = $ImageRepository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'      => 'required',
            'telefone'  => 'required',
            'email'     => 'required|email',
            'status'    => 'required|min:1|numeric' ,
            'curriculo' => 'sometimes|required|mimetypes:application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/vnd.ms-powerpoint',
        ];
    }

    /**
     * [clienteCreate description]
     * @return [type] [description]
     */
    public function trabalheConoscoCreate()
    {
        /**
         * Create - TRABALHE CONOSCO
         * @var [type]
         */
        $trabalheConosco = TrabalheConosco::create(

            $this->only(['nome','email', 'telefone', 'mensagem', 'curriculo', 'status'])

        );

        return $trabalheConosco;
    }

    /**
     * [trabalheConoscoUpdate description]
     * @return [type] [description]
     */
    public function trabalheConoscoUpdate( $id )
    {
        /**
         * Find - TRABALHE CONOSCO
         * @var [type]
         */
        $trabalheConosco = TrabalheConosco::find( $id );

        /**
         * Update - TRABALHE CONOSCO
         */
        $trabalheConosco->update(

            $this->only(['nome', 'telefone', 'email', 'empresa', 'cargo', 'mensagem', 'status'])

        );

        return $trabalheConosco;
    }
}
