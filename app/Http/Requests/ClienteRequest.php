<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

use App\Cliente;

class ClienteRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'      => 'required',
            'cpf'       => 'required_without:cnpj|min:11|max:20|unique:clientes,cpf,NULL',
            'cnpj'      => 'required_without:cpf|min:14|max:20|unique:clientes,cnpj,NULL',
            'telefone'  => 'required',
            'email'     => 'required|email',
            'status'    => 'required|min:1|numeric' 
        ];
    }

    /**
     * [clienteCreate description]
     * @return [type] [description]
     */
    public function clienteCreate()
    {
        /**
         * PREPARE
         * @var [type]
         */
        $cpf    = str_replace(['-', '.'], '', $this->cpf);
        $cnpj   = str_replace(['-', '.'], '', $this->cnpj);

        /**
         * Create Cliente
         * @var [type]
         */
        $cliente = Cliente::create(

            $this->only(['nome', 'telefone', 'email', 'status'])
            +
            ['cpf' => $cpf, 'cnpj' => $cnpj]

        );

        return $cliente;
    }

    /**
     * [clienteUpdate description]
     * @return [type] [description]
     */
    public function clienteUpdate( $id )
    {
        /**
         * PREPARE
         * @var [type]
         */
        $cpf    = str_replace(['-', '.'], '', $this->cpf);
        $cnpj   = str_replace(['-', '.'], '', $this->cnpj);
        
        /**
         * Find Cliente
         * @var [type]
         */
        $cliente = Cliente::find( $id );

        /**
         * Update Cliente
         */
        $cliente->update(

            $this->only(['nome', 'telefone', 'email', 'status'])
            +
            ['cpf' => $cpf, 'cnpj' => $cnpj]

        );

        return $cliente;
    }
}
