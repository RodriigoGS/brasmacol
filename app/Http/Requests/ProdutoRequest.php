<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Repositories\ImageRepository;
use App\Produto;

class ProdutoRequest extends Request
{
    /**
     * [$ImageRepository description]
     * @var [type]
     */
    protected $ImageRepository;

    /**
     * [__construct description]
     * @param ImageRepository $ImageRepository [description]
     */
    public function __construct(ImageRepository $ImageRepository)
    {
        $this->ImageRepository = $ImageRepository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo'     => 'required',
            'link'       => 'required',
            'slug'       => 'required',
            'descricao'  => 'required',
            'status'     => 'required|min:1|numeric',
            'thumb'      => 'sometimes|mimetypes:image/png,image/jpeg,image/pjpeg,image/gif',
            'idioma_id'  => 'required|numeric|exists:idiomas,id',
            'galeria_id' => 'required|numeric|exists:galerias,id'
        ];
    }

    /**
     * [produtoCreate description]
     * @return [type] [description]
     */
    public function produtoCreate()
    {
        /**
         * PREPARE
         */
        $slug = str_slug($this->slug, '-');

        /**
         * Update Produto
         */
        $produto = Produto::create(

            $this->only(['titulo', 'link', 'descricao', 'thumb', 'status', 'idioma_id', 'galeria_id'])
            +
            ['slug' => $slug]

        );

        /**
         * Sync Categorias
         */
        $produto->categorias()->sync( $this->categorias ? $this->categorias : [] );   

        /**
         * Verify FILE request
         */
        if( $this->hasFile('thumb') )
        {
            /**
             * [$proccessImage description]
             * @var [type]
             */
            $proccessImage = $this->ImageRepository->imageModel( 'thumb', 'produto', 890, 400 );

            /**
             * Verify produto AND upload image
             */
            if( $produto && $proccessImage != false )
            {
                /**
                 * Insert IMAGE to PRODUTO
                 */
                $produto->update(['thumb' => $proccessImage]);
            }
        }

        return $produto;
    }

    /**
     * [produtoUpdate description]
     * @return [type] [description]
     */
    public function produtoUpdate( $id )
    {
        /**
         * PREPARE
         */
        $slug = str_slug($this->slug, '-');

        /**
         * Find Produto
         * @var [type]
         */
        $produto = Produto::find( $id );

        /**
         * Update Produto
         */
        $produto->update(

            $this->only(['titulo', 'link', 'descricao', 'thumb', 'status', 'idioma_id', 'galeria_id'])
            +
            ['slug' => $slug]

        );

        /**
         * Sync Categorias
         */
        $produto->categorias()->sync( $this->categorias ? $this->categorias : [] );

        /**
         * Verify FILE request
         */
        if( $this->hasFile('thumb') )
        {
            /**
             * [$proccessImage description]
             * @var [type]
             */
            $proccessImage = $this->ImageRepository->imageModel( 'thumb', 'produto', 890, 400 );

            /**
             * Verify produto AND upload image
             */
            if( $produto && $proccessImage != false )
            {
                /**
                 * DELETE thumb from UPDATE
                 */
                if( !empty($produto->thumb) )
                    \File::delete(public_path() . '/upload/produto/' . $produto->thumb);

                /**
                 * Insert THUMB to PRODUTO
                 */
                $produto->update(['thumb' => $proccessImage]);

            }
        }

        return $produto;
    }
}
