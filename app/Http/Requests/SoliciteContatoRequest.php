<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

use App\SoliciteAcesso;

class SoliciteAcessoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'      => 'required',
            'telefone'  => 'required',
            'email'     => 'required|email',
            'status'    => 'required|min:1|numeric' 
        ];
    }

    /**
     * [clienteCreate description]
     * @return [type] [description]
     */
    public function soliciteAcessoCreate()
    {
        /**
         * Create SoliciteAcesso
         * @var [type]
         */
        $soliciteAcesso = SoliciteAcesso::create(

            $this->only(['nome', 'telefone', 'email', 'empresa', 'cargo', 'mensagem', 'status'])

        );

        return $soliciteAcesso;
    }

    /**
     * [soliciteAcessoUpdate description]
     * @return [type] [description]
     */
    public function soliciteAcessoUpdate( $id )
    {
        /**
         * Find SoliciteAcesso
         * @var [type]
         */
        $soliciteAcesso = SoliciteAcesso::find( $id );

        /**
         * Update SoliciteAcesso
         */
        $soliciteAcesso->update(

            $this->only(['nome', 'telefone', 'email', 'empresa', 'cargo', 'mensagem', 'status'])

        );

        return $soliciteAcesso;
    }
}
