<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Repositories\ImageRepository;
use App\Catalogo;

class CatalogoRequest extends Request
{
    /**
     * [$ImageRepository description]
     * @var [type]
     */
    protected $ImageRepository;

    /**
     * [__construct description]
     * @param ImageRepository $ImageRepository [description]
     */
    public function __construct(ImageRepository $ImageRepository)
    {
        $this->ImageRepository = $ImageRepository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'      => 'required',
            'catalogo'  => 'sometimes|mimetypes:application/pdf',
            'thumb'     => 'sometimes|mimetypes:image/png,image/jpeg,image/pjpeg,image/gif',
            'slug'      => 'required',
            'status'    => 'required|min:1|numeric',
            'idioma_id' => 'required|numeric|exists:idiomas,id'
        ];
    }

    /**
     * [catalogoCreate description]
     * @return [type] [description]
     */
    public function catalogoCreate()
    {
        /**
         * Update Catalogo
         */
        $catalogo = Catalogo::create(

            $this->only(['nome', 'status', 'idioma_id'])
            +
            [ 'slug' => str_slug($this->slug) ]

        );

        /**
         * Verify FILE request
         */
        if( $this->hasFile('thumb') )
        {
            /**
             * [$proccessImage description]
             * @var [type]
             */
            $proccessImage = $this->ImageRepository->imageModel( 'thumb', 'catalogo', 220, 220 );

            /**
             * Verify catalogo AND upload thumb
             */
            if( $catalogo && $proccessImage != false )
            {
                /**
                 * Insert IMAGE to CATALOGO
                 */
                $catalogo->update(['thumb' => $proccessImage]);
            }
        }

        /**
         * Verify FILE request
         */
        if( $this->hasFile('catalogo') )
        {
            $file = $this->ImageRepository->fileName( $this->file('catalogo') );

            /**
             * Storage CATALOGO
             */
            $this->catalogo->move('upload/catalogo/', $file);

            /**
             * Verify catalogo AND upload catalogo
             */
            if( $catalogo && \File::exists( 'upload/catalogo/' . $file ) )
            {
                /**
                 * Insert IMAGE to CATALOGO
                 */
                $catalogo->update(['catalogo' => $file]);
            }
        }

        return $catalogo;
    }

    /**
     * [catalogoUpdate description]
     * @return [type] [description]
     */
    public function catalogoUpdate( $id )
    {
        /**
         * Find Catalogo
         * @var [type]
         */
        $catalogo = Catalogo::find( $id );

        /**
         * Update Catalogo
         */
        $catalogo->update(

            $this->only(['nome', 'status', 'idioma_id'])
            +
            [ 'slug' => str_slug($this->slug) ]

        );

        /**
         * Verify FILE request
         */
        if( $this->hasFile('thumb') )
        {
            /**
             * [$proccessImage description]
             * @var [type]
             */
            $proccessImage = $this->ImageRepository->imageModel( 'thumb', 'catalogo', 220, 220 );

            /**
             * Verify catalogo AND upload thumb
             */
            if( $catalogo && $proccessImage != false )
            {
                /**
                 * DELETE Image from UPDATE
                 */
                if( !empty($catalogo->thumb) )
                    \File::delete(public_path() . '/upload/catalogo/' . $catalogo->thumb);

                /**
                 * Insert THUMB to CATALOGO
                 */
                $catalogo->update(['thumb' => $proccessImage]);

            }
        }

        /**
         * Verify FILE request
         */
        if( $this->hasFile('catalogo') )
        {
            $file = $this->ImageRepository->fileName( $this->file('catalogo') );

            /**
             * Storage CATALOGO
             */
            $this->catalogo->move('upload/catalogo/', $file);

            /**
             * Verify catalogo AND upload catalogo
             */
            if( $catalogo && \File::exists( 'upload/catalogo/' . $file ) )
            {
                /**
                 * DELETE Image from UPDATE
                 */
                if( !empty($catalogo->catalogo) )
                    \File::delete(public_path() . '/upload/catalogo/' . $catalogo->catalogo);

                /**
                 * Insert IMAGE to CATALOGO
                 */
                $catalogo->update(['catalogo' => $file]);
            }
        }

        return $catalogo;
    }
}
