<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Repositories\ImageRepository;

use Storage;

use App\Contato;
use App\SoliciteAcesso;
use App\TrabalheConosco;
use App\Newsletter;

class FormularioRequest extends Request
{
    /**
     * [$ImageRepository description]
     * @var [type]
     */
    protected $ImageRepository;

    /**
     * [__construct description]
     * @param ImageRepository $ImageRepository [description]
     */
    public function __construct(ImageRepository $ImageRepository)
    {
        $this->ImageRepository = $ImageRepository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'          => 'sometimes|required|max:255',
            'email'         => 'sometimes|required|max:255|email',
            'telefone'      => 'sometimes|required|max:255',
            'empresa'       => 'sometimes|required|max:255',
            'cargo'         => 'sometimes|required|max:255',
            'mensagem'      => 'sometimes|required|max:255',
            'resumo'        => 'sometimes|required|max:255',
            'curriculo'     => 'sometimes|required|mimetypes:application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/vnd.ms-powerpoint',
        ];
    }

    /**
     * [contatoCreate description]
     * @return [type] [description]
     */
    public function contatoCreate()
    {
        /**
         * Create - CONTATO
         */
        $contato = Contato::create(

            $this->only(['nome', 'email', 'telefone', 'mensagem'])
            +
            ['status' => 1]
        
        );

        return $contato;
    }

    /**
     * [trabalheConoscoCreate description]
     * @return [type] [description]
     */
    public function trabalheConoscoCreate()
    {
        /**
         * File NAME
         * @var [type]
         */
        $file = $this->ImageRepository->fileName( $this->file('curriculo') );

        /**
         * Storage CURRICULO
         */
        Storage::disk('local')->put(
            'upload/trabalhe_conosco/' . $file,
            file_get_contents($this->file('curriculo')->getRealPath())
        );

        /**
         * Check FILE
         */
        if( !Storage::exists('upload/trabalhe_conosco/' . $file) )
            return false;

        /**
         * Create - TRABALHE CONOSCO
         */
        $trabalheConosco = TrabalheConosco::create(

            $this->only(['nome', 'email', 'telefone', 'mensagem'])
            +
            ['curriculo' => $file, 'status' => 1]

        );

        return $trabalheConosco;
    }

    /**
     * [trabalheConoscoUpdate description]
     * @return [type] [description]
     */
    public function trabalheConoscoUpdate( $id )
    {
        $trabalheConosco = TrabalheConosco::find( $id );

        /**
         * Create - TRABALHE CONOSCO
         */
        $trabalheConosco->update(

            $this->only(['nome', 'email', 'telefone', 'mensagem'])
            +
            ['status' => 1]

        );

        if( $this->file('curriculo') )
        {
            /**
             * File NAME
             * @var [type]
             */
            $file = $this->ImageRepository->fileName( $this->file('curriculo') );

            /**
             * Storage CURRICULO
             */
            Storage::disk('local')->put(
                'upload/trabalhe_conosco/' . $file,
                file_get_contents($this->file('curriculo')->getRealPath())
            );

            /**
             * Check FILE
             */
            if( !Storage::exists('upload/trabalhe_conosco/' . $file) )
                return false;

            /**
             * Update - CURRICULO
             */
            $trabalheConosco->update([
                'curriculo' => $file
            ]);

        }

        return $trabalheConosco;
    }

    /**
     * [newsletterCreate description]
     * @return [type] [description]
     */
    public function newsletterCreate()
    {
        /**
         * Create - NEWSLETTER
         */
        $newsletter = Newsletter::firstOrCreate([

            'email' => $this->email
            
        ]);
        
        return $newsletter;
    }

    /**
     * [soliciteAcessoCreate description]
     * @return [type] [description]
     */
    public function soliciteAcessoCreate()
    {
        /**
         * Create - SOLICITE CONTATO
         */
        $soliciteAcesso = SoliciteAcesso::create(

            $this->only(['nome', 'email', 'telefone', 'empresa', 'cargo', 'mensagem'])
            +
            ['status' => 1]
        
        );

        return $soliciteAcesso;   
    }

    /**
     * [soliciteAcessoUpdate description]
     * @return [type] [description]
     */
    public function soliciteAcessoUpdate( $id )
    {
        /**
         * Find SoliciteAcesso
         * @var [type]
         */
        $soliciteAcesso = SoliciteAcesso::find( $id );

        /**
         * Update SoliciteAcesso
         */
        $soliciteAcesso->update(

            $this->only(['nome', 'telefone', 'email', 'empresa', 'cargo', 'mensagem', 'status'])

        );

        return $soliciteAcesso;
    }
}
