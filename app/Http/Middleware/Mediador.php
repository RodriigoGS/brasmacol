<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;


class Mediador
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    
        $acessos = $request->session()->get('menus_usuario');
        $rota = Route::current()->getName();
        if( strripos($rota, 'index') || strripos($rota, 'create') ){

            $permissao = $acessos->contains(function ($key, $value) {
                return $value->getAttributes()['rota'] == Route::current()->getName();                
            });

            if($permissao) $response = $next($request);     
            if(!$permissao) return Redirect::back();   

        }else{
            
            $response = $next($request);
            $this->handleLog($rota, $request);                
        }

        return $response;     
    }


    /**
     * Registra log no banco de dados.
     * @param  $rota de Route::current()->getName() da facade Illuminate\Support\Facades\Route
     * @param  $request \Illuminate\Http\Request $request     
     */
    public function handleLog($rota, $request)
    {
        if( strripos($rota, 'store') || strripos($rota, 'update') || strripos($rota, 'delete') || strripos($rota, 'destroy') ){
            $exploded = explode("/",$request->path());                    
            DB::table('manager_log')->insert(
                    array(
                        'id_usuario' => Auth::user()->id,
                        'id_item' => (isset($exploded[3]))? $exploded[3] : 0,
                        'tabela' => 'manager_'.$exploded[1],
                        'acao' => $exploded[2]
                    )
                );
        }
    }
}
