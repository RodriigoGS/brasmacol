<?php

namespace App\Http\Middleware;

use App\Acesso;
use Carbon\Carbon;
use Closure;

class PageView
{
    /**
     * Handle an incoming request. Contabiliza pageviews
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->getMethod() != 'GET') return $next($request);
        $periodoacesso = Carbon::now();
        $acesso = new Acesso();
        //verifica se no mesmo segundo ou no anterior o usuario ja 
        //visitou a pagina (evita duplicacao nivel 1)
        $registrado = Acesso::where('data', $periodoacesso->toDateTimeString())
                              ->orWhere('data', $periodoacesso->subSeconds(1))
                              ->where('ip', $request->ip())
                              ->first();                            
        if($registrado == null){
           
            $acesso->ip = $request->ip();
            if(!isset($request->slug)) $acesso->caminho = $request->route()->getUri();
            if(isset($request->slug)) $acesso->caminho = str_replace('{slug}', $request->slug, $request->route()->getUri());
            $acesso->useragent = $request->header('User-Agent'); 
            $acesso->save(); 
        }        
     
        return $next($request);
    }
}
