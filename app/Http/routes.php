<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
 * SITE
 * 
 */

Route::group(['prefix' => config('location.locale')], function () 
{
	/**
	 * Home
	 */
	Route::get('/', ['as' => 'home.index', 'uses' => 'HomeController@index']);

	/**
	 * Sobre
	 */
	Route::get('/sobre', ['as' => 'sobre.index', 'uses' => 'SobreController@index']);

	/**
	 * Novidades
	 */
	Route::group(['prefix' => 'novidades'], function ()
	{
		Route::get('/', ['as' => 'novidades.index', 'uses' => 'NovidadesController@index']);
		Route::get('/{slug}', ['as' => 'novidades.show', 'uses' => 'NovidadesController@show']);
		Route::get('/ajax', ['as' => 'novidades.ajax', 'uses' => 'NovidadesController@ajax']);
	});

	/**
	 * Videos
	 */
	Route::group(['prefix' => 'videos'], function ()
	{
		Route::get('/{categoria?}', ['as' => 'videos.index', 'uses' => 'VideoController@index']);
	});

	/**
	 * Catalogos
	 */
	Route::group(['prefix' => 'catalogos'], function ()
	{
		Route::get('/', ['as' => 'catalogos.index', 'uses' => 'CatalogoController@index']);
		Route::get('/{catalogo}', ['as' => 'catalogos.show', 'uses' => 'CatalogoController@show']);
	});

	/**
	 * Produtos
	 */
	Route::group(['prefix' => 'produtos'], function ()
	{
		Route::get('/{categoria?}', ['as' => 'produtos.index', 'uses' => 'ProdutoController@index']);
		Route::get('/{categoria}/{slug}', ['as' => 'produtos.show', 'uses' => 'ProdutoController@show']);
	});

	/**
	 * Cliente
	 */
	Route::group(['prefix' => 'clientes'], function ()
	{
		Route::post('/login', ['as' => 'cliente.login', 'uses' => 'ClienteController@login']);
		Route::get('/logout', ['as' => 'cliente.logout', 'uses' => 'ClienteController@logout']);
	});

	/**
	 * Formulários
	 */
	Route::post('/newsletter', ['as' => 'formulario.newsletter', 'uses' => 'FormularioController@newsletter']);
	Route::post('/fale-conosco', ['as' => 'formulario.faleconosco', 'uses' => 'FormularioController@contato']);
	Route::post('/trabalhe-conosco', ['as' => 'formulario.trabalheconosco', 'uses' => 'FormularioController@trabalheConosco']);
	Route::post('/solicite-acesso', ['as' => 'formulario.soliciteacesso', 'uses' => 'FormularioController@soliciteacesso']);

});

/**
 * MANAGER
 * 
 */

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController'
]);

Route::group(array('before' => 'auth'), function(){
    
    Route::get('/manager', 'Auth\AuthController@getLogin');
});

//rotas que precisam de autenticacao, usar dentro deste grupo
Route::group(['prefix'=>'manager', 'middleware' => ['auth','auth.mediador']], function () {

	Route::get('/', 'DashboardController@index');
	Route::get('dashboard', 'DashboardController@index');
	Route::post('/grafico/acessos',['as' => 'manager.grafico.acessos', 'uses' =>'DashboardController@graficoAcessos']);
	
	Route::get('artigo', ['as' => 'manager.artigo.index', 'uses' => 'ArtigoController@index']); //rotas nomeadas
	Route::get('artigo/create', ['as' => 'manager.artigo.create', 'uses' => 'ArtigoController@create']);
	Route::post('artigo/store', ['as' => 'manager.artigo.store', 'uses' => 'ArtigoController@store']);
	Route::get('artigo/edit/{id}', ['as' => 'manager.artigo.edit', 'uses' => 'ArtigoController@edit']);
	Route::put('artigo/update/{id}', ['as' => 'manager.artigo.update', 'uses' => 'ArtigoController@update']);	
	Route::get('artigo/delete/{id}', ['as' => 'manager.artigo.delete', 'uses' => 'ArtigoController@delete']);	
	Route::post('artigo/adicionarcategoria', ['as' => 'manager.artigo.adicionacategoria', 'uses' => 'ArtigoController@criarCategoria']);
	Route::post('artigo/preview', ['as' => 'manager.artigo.preview', 'uses' => 'ArtigoController@preview']);

	Route::get('menus', ['as' => 'manager.menus.index', 'uses' => 'MenusController@index']); //rotas nomeadas named routes
	Route::get('menus/create', ['as' => 'manager.menus.create', 'uses' => 'MenusController@create']);
	Route::post('menus/store', ['as' => 'manager.menus.store', 'uses' => 'MenusController@store']);
	Route::get('menus/edit/{id}', ['as' => 'manager.menus.edit', 'uses' => 'MenusController@edit']);
	Route::put('menus/update/{id}', ['as' => 'manager.menus.update', 'uses' => 'MenusController@update']);
	Route::get('menus/delete/{id}', ['as' => 'manager.menus.delete', 'uses' => 'MenusController@delete']);
	

	Route::get('perfil', ['as' => 'manager.perfil.index', 'uses' => 'PerfilController@index']); //rotas nomeadas
	Route::get('perfil/create', ['as' => 'manager.perfil.create', 'uses' => 'PerfilController@create']);
	Route::post('perfil/store', ['as' => 'manager.perfil.store', 'uses' => 'PerfilController@store']);
	Route::get('perfil/edit/{id}', ['as' => 'manager.perfil.edit', 'uses' => 'PerfilController@edit']);
	Route::put('perfil/update/{id}', ['as' => 'manager.perfil.update', 'uses' => 'PerfilController@update']);	
	Route::get('perfil/delete/{id}', ['as' => 'manager.perfil.delete', 'uses' => 'PerfilController@delete']);
	//destroy = excluir registro, use delete para alterar status
	//Route::get('perfil/destroy/{id}', 'PerfilController@destroy'); //no restful o verbo delete eh a referencia, nao get

	Route::get('usuario', ['as' => 'manager.usuario.index', 'uses' => 'UsuarioController@index']); //rotas nomeadas
	Route::get('usuario/create', ['as' => 'manager.usuario.create', 'uses' => 'UsuarioController@create']);
	Route::post('usuario/store', ['as' => 'manager.usuario.store', 'uses' => 'UsuarioController@store']);
	Route::get('usuario/edit/{id}', ['as' => 'manager.usuario.edit', 'uses' => 'UsuarioController@edit']);
	Route::put('usuario/update/{id}', ['as' => 'manager.usuario.update', 'uses' => 'UsuarioController@update']);	
	Route::get('usuario/delete/{id}', ['as' => 'manager.usuario.delete', 'uses' => 'UsuarioController@delete']);

	Route::put('usuario/userupdate/{id}', ['as' => 'manager.usuario.userupdate', 'uses' => 'UsuarioController@userupdate']);	
	Route::get('usuario/useredit/{id}', ['as' => 'manager.usuario.useredit', 'uses' => 'UsuarioController@useredit']);


	Route::get('galeria', ['as' => 'manager.galeria.index', 'uses' => 'GaleriaController@index']); //rotas nomeadas
	Route::get('galeria/create', ['as' => 'manager.galeria.create', 'uses' => 'GaleriaController@create']);
	Route::post('galeria/store', ['as' => 'manager.galeria.store', 'uses' => 'GaleriaController@store']);
	Route::get('galeria/edit/{id}', ['as' => 'manager.galeria.edit', 'uses' => 'GaleriaController@edit']);
	Route::put('galeria/update/{id}', ['as' => 'manager.galeria.update', 'uses' => 'GaleriaController@update']);	
	Route::get('galeria/delete/{id}', ['as' => 'manager.galeria.delete', 'uses' => 'GaleriaController@delete']);
	Route::post('/manager/galeria/carregarimagem', ['as' => 'manager.galeria.upload', 'uses' => 'GaleriaController@uploadImagemGaleria']);
	Route::post('/manager/galeria/imagensgaleria', ['as' => 'manager.galeria.imagens', 'uses' => 'GaleriaController@imagensGaleria']);
	Route::get('/manager/galeria/excluirimagem/{id_galeria}/{id}', ['as' => 'manager.galeria.imagemexcluir', 'uses' => 'GaleriaController@deleteImagem']);


	Route::get('categoria', ['as' => 'manager.categoria.index', 'uses' => 'CategoriaController@index']); //rotas nomeadas
	Route::get('categoria/create', ['as' => 'manager.categoria.create', 'uses' => 'CategoriaController@create']);
	Route::post('categoria/store', ['as' => 'manager.categoria.store', 'uses' => 'CategoriaController@store']);
	Route::get('categoria/edit/{id}', ['as' => 'manager.categoria.edit', 'uses' => 'CategoriaController@edit']);
	Route::put('categoria/update/{id}', ['as' => 'manager.categoria.update', 'uses' => 'CategoriaController@update']);	
	Route::get('categoria/delete/{id}', ['as' => 'manager.categoria.delete', 'uses' => 'CategoriaController@delete']);

	Route::get('slider', ['as' => 'manager.slider.index', 'uses' => 'SliderController@index']); //rotas nomeadas
	Route::get('slider/create', ['as' => 'manager.slider.create', 'uses' => 'SliderController@create']);
	Route::post('slider/store', ['as' => 'manager.slider.store', 'uses' => 'SliderController@store']);
	Route::get('slider/edit/{id}', ['as' => 'manager.slider.edit', 'uses' => 'SliderController@edit']);
	Route::put('slider/update/{id}', ['as' => 'manager.slider.update', 'uses' => 'SliderController@update']);	
	Route::get('slider/delete/{id}', ['as' => 'manager.slider.delete', 'uses' => 'SliderController@delete']);

	Route::get('newsletter', ['as' => 'manager.newsletter.index', 'uses' => 'NewsletterController@index']); //rotas nomeadas
	Route::get('newsletter/create', ['as' => 'manager.newsletter.create', 'uses' => 'NewsletterController@create']);
	Route::post('newsletter/store', ['as' => 'manager.newsletter.store', 'uses' => 'NewsletterController@store']);
	Route::get('newsletter/edit/{id}', ['as' => 'manager.newsletter.edit', 'uses' => 'NewsletterController@edit']);
	Route::put('newsletter/update/{id}', ['as' => 'manager.newsletter.update', 'uses' => 'NewsletterController@update']);	
	Route::get('newsletter/delete/{id}', ['as' => 'manager.newsletter.delete', 'uses' => 'NewsletterController@delete']);

	Route::get('contato', ['as' => 'manager.contato.index', 'uses' => 'ContatoController@index']);
	Route::get('contato/instrutor', ['as' => 'manager.contato.indexinstrutor', 'uses' => 'ContatoController@indexInstrutor']);
	Route::get('contato/create', ['as' => 'manager.contato.create', 'uses' => 'ContatoController@create']);
	Route::post('contato/store', ['as' => 'manager.contato.store', 'uses' => 'ContatoController@store']);
	Route::get('contato/edit/{id}', ['as' => 'manager.contato.edit', 'uses' => 'ContatoController@edit']);
	Route::put('contato/update/{id}', ['as' => 'manager.contato.update', 'uses' => 'ContatoController@update']);	
	Route::get('contato/delete/{id}', ['as' => 'manager.contato.delete', 'uses' => 'ContatoController@delete']);
	Route::post('contato/atualizacao', ['as' => 'manager.contato.atualizacao', 'uses' => 'ContatoController@atualizacoes']);
	Route::get('contato/show/{id}', ['as' => 'manager.contato.show', 'uses' => 'ContatoController@show']);

	Route::get('sitemap', ['as' => 'manager.sitemap.index', 'uses' => 'SiteMapController@index']); //rotas nomeadas
	Route::get('sitemap/create', ['as' => 'manager.sitemap.create', 'uses' => 'SiteMapController@create']);
	Route::post('sitemap/store', ['as' => 'manager.sitemap.store', 'uses' => 'SiteMapController@store']);
	Route::get('sitemap/edit/{id}', ['as' => 'manager.sitemap.edit', 'uses' => 'SiteMapController@edit']);
	Route::put('sitemap/update/{id}', ['as' => 'manager.sitemap.update', 'uses' => 'SiteMapController@update']);	
	Route::get('sitemap/destroy/{id}', ['as' => 'manager.sitemap.destroy', 'uses' => 'SiteMapController@destroy']);

	Route::get('arquivo', ['as' => 'manager.arquivo.index', 'uses' => 'ArquivoController@index']);
	Route::get('arquivo/create', ['as' => 'manager.arquivo.create', 'uses' => 'ArquivoController@create']);
	Route::post('arquivo/store', ['as' => 'manager.arquivo.store', 'uses' => 'ArquivoController@store']);
	Route::get('arquivo/edit/{id}', ['as' => 'manager.arquivo.edit', 'uses' => 'ArquivoController@edit']);
	Route::put('arquivo/update/{id}', ['as' => 'manager.arquivo.update', 'uses' => 'ArquivoController@update']);	
	Route::get('arquivo/delete/{id}', ['as' => 'manager.arquivo.delete', 'uses' => 'ArquivoController@delete']);
	Route::post('arquivo/uploadext', ['as' => 'manager.arquivo.uploadext', 'uses' => 'ArquivoController@uploadExterno']);

	Route::get('relatorio', ['as' => 'manager.relatorio.index', 'uses' => 'RelatorioController@index']);
	Route::post('relatorio/grafico/acessosunico', ['as' => 'manager.relatorio.graficoacessosunicos', 'uses' => 'RelatorioController@getAcessosUnicos']);
	Route::post('relatorios/grafico/pornavegador', ['as' => 'manager.relatorio.graficonavegador', 'uses' => 'RelatorioController@acessosNavegador']);
	Route::post('relatorios/grafico/porsistema', ['as' => 'manager.relatorio.graficoporsistema', 'uses' => 'RelatorioController@acessosSistemaOperacional']);

	/**
	 * Manager
	 */
	Route::group(['namespace' => 'Manager'], function()
	{
		/**
		 * Produtos
		 */
		Route::group(['prefix' => 'produtos'], function ()
		{
			Route::get('/', ['as' => 'manager.produtos.index', 'uses' => 'ProdutoController@index']);
			Route::get('/create', ['as' => 'manager.produtos.create', 'uses' => 'ProdutoController@create']);
			Route::post('/store', ['as' => 'manager.produtos.store', 'uses' => 'ProdutoController@store']);
			Route::get('/edit/{produto}', ['as' => 'manager.produtos.edit', 'uses' => 'ProdutoController@edit']);
			Route::put('/update/{id}', ['as' => 'manager.produtos.update', 'uses' => 'ProdutoController@update']);
			Route::get('/delete/{produto}', ['as' => 'manager.produtos.delete', 'uses' => 'ProdutoController@delete']);

			Route::group(['prefix' => 'categorias'], function ()
			{
				Route::get('/', ['as' => 'manager.produtos.categorias.index', 'uses' => 'ProdutoCategoriaController@index']);
				Route::get('/create', ['as' => 'manager.produtos.categorias.create', 'uses' => 'ProdutoCategoriaController@create']);
				Route::post('/store', ['as' => 'manager.produtos.categorias.store', 'uses' => 'ProdutoCategoriaController@store']);
				Route::get('/edit/{categoria}', ['as' => 'manager.produtos.categorias.edit', 'uses' => 'ProdutoCategoriaController@edit']);
				Route::put('/update/{id}', ['as' => 'manager.produtos.categorias.update', 'uses' => 'ProdutoCategoriaController@update']);
				Route::get('/delete/{categoria}', ['as' => 'manager.produtos.categorias.delete', 'uses' => 'ProdutoCategoriaController@delete']);

				Route::post('/adicionar', ['as' => 'manager.produtos.categorias.adicionar', 'uses' => 'ProdutoCategoriaController@adicionarAjax']);
			});	
		});

		/**
		 * Idiomas
		 */
		Route::group(['prefix' => 'idiomas'], function ()
		{
			Route::get('/', ['as' => 'manager.idiomas.index', 'uses' => 'IdiomaController@index']);
			Route::get('/create', ['as' => 'manager.idiomas.create', 'uses' => 'IdiomaController@create']);
			Route::post('/store', ['as' => 'manager.idiomas.store', 'uses' => 'IdiomaController@store']);
			Route::get('/edit/{idioma}', ['as' => 'manager.idiomas.edit', 'uses' => 'IdiomaController@edit']);
			Route::put('/update/{id}', ['as' => 'manager.idiomas.update', 'uses' => 'IdiomaController@update']);
			Route::get('/delete/{idioma}', ['as' => 'manager.idiomas.delete', 'uses' => 'IdiomaController@delete']);
		});

		/**
		 * Videos
		 */
		Route::group(['prefix' => 'videos'], function ()
		{
			Route::get('/', ['as' => 'manager.videos.index', 'uses' => 'VideoController@index']);
			Route::get('/create', ['as' => 'manager.videos.create', 'uses' => 'VideoController@create']);
			Route::post('/store', ['as' => 'manager.videos.store', 'uses' => 'VideoController@store']);
			Route::get('/edit/{video}', ['as' => 'manager.videos.edit', 'uses' => 'VideoController@edit']);
			Route::put('/update/{id}', ['as' => 'manager.videos.update', 'uses' => 'VideoController@update']);
			Route::get('/delete/{video}', ['as' => 'manager.videos.delete', 'uses' => 'VideoController@delete']);
			
			Route::group(['prefix' => 'categorias'], function ()
			{
				Route::get('/', ['as' => 'manager.videos.categorias.index', 'uses' => 'VideoCategoriaController@index']);
				Route::get('/create', ['as' => 'manager.videos.categorias.create', 'uses' => 'VideoCategoriaController@create']);
				Route::post('/store', ['as' => 'manager.videos.categorias.store', 'uses' => 'VideoCategoriaController@store']);
				Route::get('/edit/{categoria}', ['as' => 'manager.videos.categorias.edit', 'uses' => 'VideoCategoriaController@edit']);
				Route::put('/update/{id}', ['as' => 'manager.videos.categorias.update', 'uses' => 'VideoCategoriaController@update']);
				Route::get('/delete/{categoria}', ['as' => 'manager.videos.categorias.delete', 'uses' => 'VideoCategoriaController@delete']);

				Route::post('/adicionar', ['as' => 'manager.videos.categorias.adicionar', 'uses' => 'VideoCategoriaController@adicionarAjax']);
			});	
		});

		/**
		 * Clientes
		 */
		Route::group(['prefix' => 'clientes'], function ()
		{
			Route::get('/', ['as' => 'manager.clientes.index', 'uses' => 'ClienteController@index']);
			Route::get('/create', ['as' => 'manager.clientes.create', 'uses' => 'ClienteController@create']);
			Route::post('/store', ['as' => 'manager.clientes.store', 'uses' => 'ClienteController@store']);
			Route::get('/edit/{cliente}', ['as' => 'manager.clientes.edit', 'uses' => 'ClienteController@edit']);
			Route::put('/update/{id}', ['as' => 'manager.clientes.update', 'uses' => 'ClienteController@update']);
			Route::get('/delete/{cliente}', ['as' => 'manager.clientes.delete', 'uses' => 'ClienteController@delete']);

			Route::get('/access/{cliente}', ['as' => 'manager.clientes.access', 'uses' => 'ClienteController@access']);
		});

		/**
		 * Catálogos
		 */
		Route::group(['prefix' => 'catalogos'], function ()
		{
			Route::get('/', ['as' => 'manager.catalogos.index', 'uses' => 'CatalogoController@index']);
			Route::get('/create', ['as' => 'manager.catalogos.create', 'uses' => 'CatalogoController@create']);
			Route::post('/store', ['as' => 'manager.catalogos.store', 'uses' => 'CatalogoController@store']);
			Route::get('/edit/{catalogo}', ['as' => 'manager.catalogos.edit', 'uses' => 'CatalogoController@edit']);
			Route::put('/update/{id}', ['as' => 'manager.catalogos.update', 'uses' => 'CatalogoController@update']);
			Route::get('/delete/{catalogo}', ['as' => 'manager.catalogos.delete', 'uses' => 'CatalogoController@delete']);
		});

		/**
		 * Empresas
		 */
		Route::group(['prefix' => 'empresas'], function ()
		{
			Route::get('/', ['as' => 'manager.empresas.index', 'uses' => 'EmpresaController@index']);
			Route::get('/create', ['as' => 'manager.empresas.create', 'uses' => 'EmpresaController@create']);
			Route::post('/store', ['as' => 'manager.empresas.store', 'uses' => 'EmpresaController@store']);
			Route::get('/edit/{empresa}', ['as' => 'manager.empresas.edit', 'uses' => 'EmpresaController@edit']);
			Route::put('/update/{id}', ['as' => 'manager.empresas.update', 'uses' => 'EmpresaController@update']);
			Route::get('/delete/{empresa}', ['as' => 'manager.empresas.delete', 'uses' => 'EmpresaController@delete']);
		});

		/**
		 * Solicite Acesso
		 */
		Route::group(['prefix' => 'solicite-acesso'], function ()
		{
			Route::get('/', ['as' => 'manager.soliciteacesso.index', 'uses' => 'SoliciteAcessoController@index']);
			Route::get('/create', ['as' => 'manager.soliciteacesso.create', 'uses' => 'SoliciteAcessoController@create']);
			Route::post('/store', ['as' => 'manager.soliciteacesso.store', 'uses' => 'SoliciteAcessoController@store']);
			Route::get('/edit/{soliciteAcesso}', ['as' => 'manager.soliciteacesso.edit', 'uses' => 'SoliciteAcessoController@edit']);
			Route::put('/update/{id}', ['as' => 'manager.soliciteacesso.update', 'uses' => 'SoliciteAcessoController@update']);
			Route::get('/delete/{soliciteAcesso}', ['as' => 'manager.soliciteacesso.delete', 'uses' => 'SoliciteAcessoController@delete']);
		});

		/**
		 * Trabalhe Conosco
		 */
		Route::group(['prefix' => 'trabalhe-conosco'], function ()
		{
			Route::get('/', ['as' => 'manager.trabalheconosco.index', 'uses' => 'TrabalheConoscoController@index']);
			Route::get('/create', ['as' => 'manager.trabalheconosco.create', 'uses' => 'TrabalheConoscoController@create']);
			Route::post('/store', ['as' => 'manager.trabalheconosco.store', 'uses' => 'TrabalheConoscoController@store']);
			Route::get('/edit/{trabalheConosco}', ['as' => 'manager.trabalheconosco.edit', 'uses' => 'TrabalheConoscoController@edit']);
			Route::put('/update/{id}', ['as' => 'manager.trabalheconosco.update', 'uses' => 'TrabalheConoscoController@update']);
			Route::get('/delete/{trabalheConosco}', ['as' => 'manager.trabalheconosco.delete', 'uses' => 'TrabalheConoscoController@delete']);
			Route::get('/download/{curriculo}', ['as' => 'manager.trabalheconosco.download', 'uses' => 'TrabalheConoscoController@download']);
		});
	});
	
});


Route::get('/artigos/rss', 'ArtigoController@feed');

//Route::get('/usuarios/send', 'UsuarioController@sendExemplo');

//exemplo de uso do middleware para contar visitas de paginas:
// Route::group(['middleware' => ['pageview']], function () {
// 	Route::get('/',['as' => 'site.home', 'uses' => 'SiteController@index']);
// });


