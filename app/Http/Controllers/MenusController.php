<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MenusController extends Controller
{
    private $menu;

    public function __construct(Menu $menu)
    {
        $this->middleware('auth');        
        $this->menu = $menu;
    }

    public function index()
    {        
    	$menus = $this->menu->where('status', '<', 3)->get();
    	return view('menus.index', ['menus' => $menus])->with('controlador', new Controller);
    	
    }


    public function create(){

        $menus = new Menu();
        $itensMenu = $menus->opcoesSelect();
        return view('menus.create', ['itens' => $itensMenu] );
    }


    public function store(Request $request)
    {
        
        $menu = new Menu();
        if($request->pai == 0) $request->pai = ''; //forma oldschool de validar, forma intermediaria no update()
        if($request->pai != 0) $menu->pai = $request->pai;
        $menu->id_usuario = Auth::user()->id;
        $menu->descricao = $request->descricao;
        if($request->rota != '') $menu->rota = $request->rota;
        $menu->icone = $request->icone;
        $menu->grupo_ordem = $request->grupo_ordem;
        $menu->ordem_interna = $request->ordem_interna;
        $menu->status = $request->status;

        if($menu->save()){

            DB::table('manager_perfil_menu')->insert(
                [
                'id_perfil' => Auth::user()->id_perfil,
                'id_menu' => $menu->id        
                ]
            );

            $mensagem = array(
                'type' => 'success',
                'retorno' => 'Menu gravado com sucesso'
            );
        }else{

            $mensagem = array(
                'type' => 'error',
                'retorno' => 'Erro ao salvar'
            );

        }
               
        return redirect()->route('manager.menus.index')->with('status', $mensagem['type'])
                                                       ->with('retornomensagem', $mensagem['retorno']);      

    }


    public function edit($id)
    {
    	$menu = $this->menu->find($id);
        $menus = new Menu();    	
    	$itensMenu = $menus->opcoesSelect();
        $usuarioCadastro = parent::infoUsuarioAutor($menu->id_usuario);
        return view('menus.edit', ['menu' => $menu, 'itens' => $itensMenu, 'usuarioCadastro' => $usuarioCadastro] );
    }

    

    public function update($id, Request $request)
    {        
        if($request->pai == 0) $request->merge( array( 'pai' => null ) ); //merge valor no input do request        
        $request->request->add(['id_usuario' => Auth::user()->id]);
              
        $this->menu->find($id)->update($request->all());                
        return redirect()->route('manager.menus.index')->with('status', 'success')
                                                       ->with('retornomensagem', 'Menu editado com sucesso');
    }
    


    public function destroy($id)
    {

    	$this->menu->destroy($id);       
    	return redirect()->route('manager.menus.index')->with('status', 'success')
    										           ->with('retornomensagem', 'Item excluído com sucesso');
    }


    public function delete($id)
    {
        $this->menu->find($id)->update(array('status' => 3));       
        return redirect()->route('manager.menus.index')->with('status', 'success')
                                                       ->with('retornomensagem', 'Item desativado com sucesso');
    }

}