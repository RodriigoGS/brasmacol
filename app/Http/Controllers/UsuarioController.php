<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\UsuarioEditRequest;
use App\Http\Requests\UsuarioRequest;
use App\Perfil;
use App\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\Facades\Image;

class UsuarioController extends Controller
{
    private $usuario;

    public function __construct(Usuario $usuario)
    {
        $this->middleware('auth', ['except' => ['sendExemplo']]);
        $this->usuario = $usuario;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = $this->usuario->where('status', 1)->get();
        return view('usuario.index', ['usuarios' => $usuarios])->with('controlador', new Controller);            
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $perfil = new Perfil();
        $perfis = $perfil->opcoesSelect();
        return view('usuario.create', array('perfis' => $perfis));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\UsuarioRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsuarioRequest $request)
    {
        $request->merge( array('senha' => bcrypt($request->senha )) );

        $usuario = $this->usuario->create($request->all());    

        if($usuario){

            if($request->file('imagem') != null){

                $nomeImagem = $this->uploadImagemUsuario($request, 'imagem', $request->usuario);            
                if($nomeImagem) $this->usuario->find($usuario->id)->update(array('imagem' => $nomeImagem));
                if(!$nomeImagem) $this->usuario->find($usuario->id)->update(array('imagem' => null));
            }

            $mensagem = array(
                'type' => 'success',
                'retorno' => 'Usuário cadastrado com sucesso'
            );
        }else{

            $mensagem = array(
                'type' => 'error',
                'retorno' => 'Erro ao salvar'
            );

        }
        return redirect()->route('manager.usuario.index')->with('status', $mensagem['type'])
                                                         ->with('retornomensagem', $mensagem['retorno']);
    }
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $usuario = $this->usuario->findOrFail($id);
        $perfil = new Perfil();
        $perfis = $perfil->opcoesSelect();    
        return view('usuario.edit', ['usuario' => $usuario, 'perfis' => $perfis] );
    }



    /**
     * Edição de dados pessoais pelo usuario
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function useredit($id)
    {
        
        if(Auth::user()->id != $id) return redirect('manager/');
        
        $usuario = $this->usuario->find($id);        
        return view('usuario.useredit', ['usuario' => $usuario] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {                        

        if( !empty($request->senha) ) $request->merge( array( 'senha' => bcrypt($request->senha) ) );

        if( empty($request->senha) && !empty($request->senha_antiga) ){
            $usuario = $this->usuario->find($id);            
            $request->merge( array( 'senha' => $usuario->senha ) );
        }
        
        
        $this->usuario->find($id)->update($request->all());

        if($request->file('imagem') != null){

            $nomeImagem = $this->uploadImagemUsuario($request, 'imagem', $request->usuario);            
            if($nomeImagem) $this->usuario->find($id)->update(array('imagem' => $nomeImagem));
            if(!$nomeImagem) $this->usuario->find($id)->update(array('imagem' => null));
        }

        return redirect()->route('manager.usuario.index')->with('status', 'success')
                                                         ->with('retornomensagem', 'Usuário editado com sucesso');
    }


    /**
     * Atualização pelo usuario
     *
     * @param  \Http\Requests\UsuarioEditRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function userupdate(UsuarioEditRequest $request, $id)
    {                
        if( !empty($request->senha) ) $request->merge( array( 'senha' => bcrypt($request->senha) ) );

        if( empty($request->senha) && !empty($request->senha_antiga) ){
            $usuario = $this->usuario->find($id);            
            $request->merge( array( 'senha' => $usuario->senha ) );
        }
              
        $this->usuario->find($id)->update($request->all());

        if($request->file('imagem') != null){

            $nomeImagem = $this->uploadImagemUsuario($request, 'imagem', $request->usuario);            
            if($nomeImagem) $this->usuario->find($id)->update(array('imagem' => $nomeImagem));
            if(!$nomeImagem) $this->usuario->find($id)->update(array('imagem' => null));
        }
                      
        return redirect()->route('manager.usuario.useredit', $id)->with('status', 'success')
                                                                 ->with('retornomensagem', 'Dados atualizados!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->usuario->destroy($id);       
        return redirect()->route('manager.usuario.index')->with('status', 'success')
                                                         ->with('retornomensagem', 'Usuário excluído com sucesso');
    }



    public function delete($id)
    {
        $usuario = $this->usuario->find($id);
        $usuario->status = 3;
        $usuario->save();    
        return redirect()->route('manager.usuario.index')->with('status', 'success')
                                                         ->with('retornomensagem', 'Usuário desativado com sucesso');
    }


    /**
     * Carrega imagem do usuario, realiza resize 160x160
     * @param object $request 
     * @param string $fieldName 
     * @param string $nomeUsuario 
     * @return boolean|string
     */
    private function uploadImagemUsuario($request, $fieldName, $nomeUsuario)
    {
        $mime = $request->file($fieldName)->getMimeType();
        $extensoesValidas = array('image/png', 'image/jpeg', 'image/pjpeg', 'image/gif');        
        if( !in_array($mime, $extensoesValidas) ) return false;

        $extensao = pathinfo($request->file($fieldName)->getClientOriginalName(), PATHINFO_EXTENSION);
        $novoNome = str_slug($nomeUsuario, '-');
        $nomeFinal = $novoNome.'.'.$extensao;

        $img = Image::make($request->file($fieldName)->getPathname())->resize(160, 160);
        $img->save(public_path().'/upload/'.$nomeFinal); //segundo parametro de save() e qualidade, opcional o default e 90
        return $nomeFinal;
    }

    // /**
    //  * Exemplo de envio de emails     
    //  */
    // public function sendExemplo()
    // {
    //     //Mail::send('emails.teste', ['variavel' => 'teste'], function ($m) use ($object) {
    //     Mail::send('emails.teste', ['variavel' => 'teste'], function ($m) {
    //         $m->from('hello@app.com', 'Manager 2.0');

    //         $m->to('fabio.aiub@gmail.com', 'Fabio')->subject('Teste Laravel + Switfmailer!');
    //     });
    // }
}
