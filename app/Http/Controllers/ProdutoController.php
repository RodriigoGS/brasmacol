<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Produto;
use App\Categoria;

class ProdutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( $categoria = null )
    {
        /**
         * GET Categorias AND Produtos
         * @var [type]
         */
        $categorias = Categoria::where('status', 1)
                                ->where('tipo', 'produto')
                                ->idioma()
                                ->with(['produtos' => function ($query) {
                                    $query->where('status', '1');
                                    $query->idioma();
                                }])->get();

        /**
         * Return VIEW
         */
        return view('site.produtos.index', [
            'categorias' => $categorias,
            'categoria' => $categoria
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show( $categoria, $slug )
    {
        /**
         * PARAMETERS
         * @var [type]
         */
        $idioma_id = config('location.modelLocale')[ config('app.locale') ];

        /**
         * GET Produto
         * @var [type]
         */
        $produto = Produto::where('slug', $slug)
                            ->where('status', 1)
                            ->idioma()
                            ->with(['categorias' => function ($query) {
                                $query->where('status', '1');
                                $query->idioma();
                            }])
                            ->firstOrFail();

        /**
         * GET Categorias AND Produtos
         * @var [type]
         */
        $categorias = Categoria::where('status', 1)
                                ->where('tipo', 'produto')
                                ->idioma()
                                ->get();

        /**
         * GET Categoria Produto
         * @var [type]
         */
        $categoria = $categorias->where('slug', $categoria)
                                ->first();

        /**
         * GET Galeria Produto
         * @var [type]
         */
        $galeria = $produto->galeria
                            ->galeria_imagens()
                            ->get();

        /**
         * [$produtos description]
         * @var [type]
         */
        
        $produtos = Produto::selectRaw('produtos.slug, produtos.titulo')
                                ->join('produto_categorias', 'produtos.id', '=', 'produto_categorias.produto_id')
                                ->join('categorias', 'categorias.id', '=', 'produto_categorias.categoria_id')
                                ->where('produtos.status', 1)
                                ->where('produtos.idioma_id', $idioma_id)
                                ->where('categorias.slug', $categoria->slug)
                                ->get();

        /**
         * Return VIEW
         */
        return view('site.produtos.show', [
            'produto' => $produto,
            'produtos' => $produtos,
            'categoria' => $categoria,
            'categorias' => $categorias,
            'galeria' => $galeria
        ]);
    }
}
