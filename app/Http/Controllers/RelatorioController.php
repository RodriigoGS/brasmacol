<?php

namespace App\Http\Controllers;

use App\Acesso;
use App\Http\Requests;
use Illuminate\Http\Request;

class RelatorioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $acessos = new Acesso();
        $porpagina = $acessos->getPorPagina();
        $this->acessosNavegador();
        return view('relatorio.index', ['porpagina' => $porpagina]);
    }

    public function getAcessosUnicos(Request $request)
    {
        $acessos = new Acesso();
        $dadosAcessos = $acessos->acessosDiariosUnicos($request->mes, $request->ano);        
        $dadosBrutos = $dadosAcessos->keyBy('dia')->all(); //o numero do dia transformado no indice do conjunto
        $dados = array();
        for($i=1; $i <= 31; $i++){
            if(array_key_exists($i,$dadosBrutos)){
                $dados[] = $dadosBrutos[$i]->total;
            }else{
                $dados[] = 0;
            }
        }        
        return json_encode($dados);
    }


    public function acessosNavegador()
    {
        $acessos = new Acesso();
        $dados = $acessos->porNavegador();
        return json_encode($dados);
    }

    public function acessosSistemaOperacional()
    {
        $acessos = new Acesso();
        $dados = $acessos->porSistemaOperacional();
        return json_encode($dados);
    }
}
