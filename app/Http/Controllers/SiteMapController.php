<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Sitemap;
use Illuminate\Http\Request;

class SiteMapController extends Controller
{

    private $sitemap;

    public function __construct(Sitemap $sitemap)
    {
        $this->middleware('auth');
        $this->sitemap = $sitemap;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sitemaps = $this->sitemap->get();
        return view('sitemap.index', ['sitemaps' => $sitemaps]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sitemap = new Sitemap();
        $comboPrioridades = $sitemap->comboPrioridades();
        $comboFrequencias = $sitemap->comboFrequencias();
        return view('sitemap.create', ['priority' => $comboPrioridades, 'changefreq' => $comboFrequencias]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sitemap = new Sitemap();        
        $sitemap->loc = $request->loc;
        $sitemap->arquivo = $request->arquivo;
        $sitemap->changefreq = $request->changefreq;
        $sitemap->priority = $request->priority;
                
        if($sitemap->save()){

            $mensagem = array(
                'type' => 'success',
                'retorno' => 'Entrada de sitemap gravada com sucesso'
            );
            
        }else{

            $mensagem = array(
                'type' => 'error',
                'retorno' => 'Erro ao salvar'
            );


        }
        return redirect()->route('manager.sitemap.index')
                         ->with('status', $mensagem['type'])
                         ->with('retornomensagem', $mensagem['retorno']);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sitemap = $this->sitemap->findOrFail($id); //404 HTTP response eh retornado         
        $comboPrioridades = $sitemap->comboPrioridades();
        $comboFrequencias = $sitemap->comboFrequencias();
        return view('sitemap.edit', ['sitemap' => $sitemap, 'priority' => $comboPrioridades, 'changefreq' => $comboFrequencias] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sitemap = $this->sitemap->find($id);
        $sitemap->loc = $request->loc;
        $sitemap->arquivo = $request->arquivo;
        $sitemap->changefreq = $request->changefreq;
        $sitemap->priority = $request->priority;
        $sitemap->save();
        return redirect()->route('manager.sitemap.index')
                         ->with('status', 'success')
                         ->with('retornomensagem', 'Entrada editada com sucesso');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sitemap = $this->sitemap->findOrFail($id);
        $sitemap->delete();
        return redirect()->route('manager.sitemap.index')
                         ->with('status', 'success')
                         ->with('retornomensagem', 'Entrada editada com sucesso');
    }
}
