<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Newsletter;
use Illuminate\Http\Request;

class NewsletterController extends Controller
{

    private $newsletter;

    public function __construct(Newsletter $news)
    {
        $this->middleware('auth');
        $this->newsletter = $news;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $registros = $this->newsletter->where('status','<', 3)->get();
        return view('newsletter.index', ['registros' => $registros])->with('controlador', new Controller);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('newsletter.create');   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newsletter = $this->newsletter->create($request->all());
                
        if($newsletter){

            $mensagem = array(
                'type' => 'success',
                'retorno' => 'E-mail gravado com sucesso'
            );
        }else{

            $mensagem = array(
                'type' => 'error',
                'retorno' => 'Erro ao salvar'
            );            
        }

        return redirect()->route('manager.newsletter.index')->with('status', $mensagem['type'])
                                                            ->with('retornomensagem', $mensagem['retorno']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $newsletter = $this->newsletter->findOrFail($id);  //404 HTTP response eh retornado                         
        return view('newsletter.edit', ['newsletter' => $newsletter] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->newsletter->find($id)->update($request->all());
        return redirect()->route('manager.newsletter.index')->with('status', 'success')
                                                            ->with('retornomensagem', 'Item editado com sucesso');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->newsletter->destroy($id);       
        return redirect()->route('manager.newsletter.index')->with('status', 'success')
                                                            ->with('retornomensagem', 'Item excluído com sucesso');
    }

    public function delete($id)
    {
        $this->newsletter->find($id)->update(array('status' => 3));       
        return redirect()->route('manager.newsletter.index')->with('status', 'success')
                                                            ->with('retornomensagem', 'Item desativado com sucesso');
    }
}
