<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoriaController extends Controller
{

    private $categoria;

    public function __construct(Categoria $categoria)
    {
        $this->middleware('auth');
        $this->categoria = $categoria;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias = $this->categoria->where('status', '<', 3)->where('tipo', 'artigo')->get();
        return view('categoria.index', ['categorias' => $categorias])->with('controlador', new Controller);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categoria.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $categoria = new Categoria();
        $categoria->id_usuario = Auth::user()->id;
        $categoria->idioma_id = $request->idioma_id;
        $categoria->titulo = $request->titulo;
        $categoria->tipo = 'artigo';
        $categoria->status = $request->status;
        $categoria->slug = str_slug($request->titulo, '-');
                
        if($categoria->save()){

            $mensagem = array(
                'type' => 'success',
                'retorno' => 'Categoria gravada com sucesso'
            );
            
        }else{

            $mensagem = array(
                'type' => 'error',
                'retorno' => 'Erro ao salvar'
            );

        }
        return redirect()->route('manager.categoria.index')->with('status', $mensagem['type'])
                                                           ->with('retornomensagem', $mensagem['retorno']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categoria = $this->categoria->find($id);         
        $menus = Categoria::all();
        $usuarioCadastro = parent::infoUsuarioAutor($categoria->id_usuario);
        
        return view('categoria.edit', ['categoria' => $categoria, 'usuarioCadastro' => $usuarioCadastro] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $categoria = $this->categoria->find($id);
        $categoria->id_usuario = Auth::user()->id;
        $categoria->idioma_id = $request->idioma_id;
        $categoria->titulo = $request->titulo;
        $categoria->tipo = 'artigo';
        $categoria->status = $request->status;
        $categoria->slug = str_slug($request->titulo, '-');
        $categoria->save();
        return redirect()->route('manager.categoria.index')->with('status', 'success')
                                                           ->with('retornomensagem', 'Item editado com sucesso');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->categoria->destroy($id);       
        return redirect()->route('manager.categoria.index')->with('status', 'success')
                                                           ->with('retornomensagem', 'Item excluído com sucesso');
    }

    public function delete($id)
    {
        $this->categoria->find($id)->update(array('status' => 3));       
        return redirect()->route('manager.categoria.index')->with('status', 'success')
                                                           ->with('retornomensagem', 'Item desativado com sucesso');
    }
}
