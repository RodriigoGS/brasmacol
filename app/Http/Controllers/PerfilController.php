<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Perfil;
use App\Menu;
use App\Http\Requests;

class PerfilController extends Controller
{

    private $perfil;

    public function __construct(Perfil $perfil) //injecao de dependencia
    {
        $this->middleware('auth');
        $this->perfil = $perfil;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perfis = $this->perfil->where('status', 1)->get();
        return view('perfil.index', ['perfis' => $perfis])->with('controlador', new Controller);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('perfil.create');
    }

    /**
     * 
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $perfil = $this->perfil->create($request->all());
                
        if($perfil){

            $mensagem = array(
                'type' => 'success',
                'retorno' => 'Perfil gravado com sucesso'
            );

            return redirect()->route('manager.perfil.edit', ['id' => $perfil->id])->with('status', $mensagem['type'])
                                                            ->with('retornomensagem', $mensagem['retorno']);
        }else{

            $mensagem = array(
                'type' => 'error',
                'retorno' => 'Erro ao salvar'
            );

            return redirect()->route('manager.perfil.index')->with('status', $mensagem['type'])
                                                            ->with('retornomensagem', $mensagem['retorno']);

        }
        
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {        
        $perfil = $this->perfil->findOrFail($id);  //404 HTTP response eh retornado         
        $idsMenusPerfil = $this->getIdsMenuPerfil($perfil);
        $menus = Menu::all();
        
        return view('perfil.edit', ['perfil' => $perfil, 'menus_perfil' => $idsMenusPerfil, 'menus' => $menus] );
    }



    public function update($id, Request $request)
    {           
        $this->perfil->find($id)->update($request->all());
        $perfil = $this->perfil->find($id);
        $perfil->menu()->sync( $request->input('menus') );
        return redirect()->route('manager.perfil.index')->with('status', 'success')
                                                        ->with('retornomensagem', 'Item editado com sucesso');
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->perfil->destroy($id);       
        return redirect()->route('manager.perfil.index')->with('status', 'success')
                                                        ->with('retornomensagem', 'Item excluído com sucesso');
    }

    public function delete($id)
    {
        $this->perfil->find($id)->update(array('status' => 3));       
        return redirect()->route('manager.perfil.index')->with('status', 'success')
                                                        ->with('retornomensagem', 'Item desativado com sucesso');
    }


    /**
     * Retorna os ids dos menus vinculados na tabela pivo menu_perfil
     * @param object $perfil instancia model Perfil
     * @return array contendo os id_menu ou vazio se nada encontrar
     */
    private function getIdsMenuPerfil($perfil)
    {
        $menus_perfil = $perfil->menu->all();
        $idsMenus = array();
        foreach ($menus_perfil as $menu) {            
            $idsMenus[] = $menu->id;
        }
        return $idsMenus;
    }
}
