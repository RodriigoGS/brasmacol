<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\FormularioRequest;

use App\Http\Requests;

use Mail;
use Storage;

class FormularioController extends Controller
{
    /**
     * [contato description]
     * @param  FormularioRequest $request [description]
     * @return [type]                     [description]
     */
    public function contato(FormularioRequest $request)
    {
        /**
         * Create - CONTATO
         */
        $contato = $request->contatoCreate();

        if( !$contato )
            return back()
                        ->with('contato', 'Erro ao armazenar mensagem!')
                        ->withInput();

        /**
         * Mail - CONTATO
         */
        Mail::send('emails.contato', ['contato' => $contato], function ($m) use ($contato) 
        {
            $m->from($contato->email, $contato->nome);
            $m->subject('Contato - ' . config('app.name'));
        });

        /**
         * Return - BACK VIEW
         */
        return back()->with('contato', 'Mensagem enviada com sucesso!');
    }

    /**
     * [trabalheConosco description]
     * @param  FormularioRequest $request [description]
     * @return [type]                     [description]
     */
    public function trabalheConosco(FormularioRequest $request)
    {
        /**
         * Create - TRABALHE CONOSCO
         */
        $trabalheConosco = $request->trabalheConoscoCreate();

        if( !$trabalheConosco )
            return back()
                        ->with('trabalheConosco', 'Erro ao armazenar mensagem!')
                        ->withInput();

        /**
         * Mail - CONTATO
         */
        Mail::send('emails.trabalhe-conosco', ['trabalheConosco' => $trabalheConosco], function ($m) use ($trabalheConosco) 
        {
            $m->from($trabalheConosco->email, $trabalheConosco->nome);
            $m->attach(storage_path('app/upload/trabalhe_conosco/' . $trabalheConosco->curriculo));
            $m->subject('Trabalhe Conosco - ' . config('app.name'));
        });

        /**
         * Return - BACK VIEW
         */
        return back()->with('trabalheConosco', 'Mensagem enviada com sucesso!');
    }

    /**
     * [soliciteAcesso description]
     * @param  FormularioRequest $request [description]
     * @return [type]                     [description]
     */
    public function soliciteAcesso(FormularioRequest $request)
    {
        /**
         * Create - SOLICITE CONTATO
         */
        $soliciteAcesso = $request->soliciteAcessoCreate();

        if( !$soliciteAcesso )
            return back()
                        ->with('acesso', 'Erro ao armazenar mensagem!')
                        ->withInput();

        /**
         * Mail - ACESSO
         */
        Mail::send('emails.acesso', ['soliciteAcesso' => $soliciteAcesso], function ($m) use ($soliciteAcesso) 
        {
            $m->from($soliciteAcesso->email, $soliciteAcesso->nome);
            $m->subject('Acesso - ' . config('app.name'));
        });

        /**
         * Return - BACK VIEW
         */
        return back()->with('soliciteAcesso', 'Mensagem enviada com sucesso!');
    }

    /**
     * [newsletter description]
     * @param  FormularioRequest $request [description]
     * @return [type]                     [description]
     */
    public function newsletter(FormularioRequest $request)
    {
        /**
         * Create - NEWSLETTER
         */
        $newsletter = $request->newsletterCreate();

        if( !$newsletter )
            return back()
                        ->with('newsletter', 'Erro ao armazenar mensagem!')
                        ->withInput();

        /**
         * Return - BACK VIEW
         */
        return back()->with('newsletter', 'Mensagem enviada com sucesso!');
    }
}