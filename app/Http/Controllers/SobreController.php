<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Empresa;

class SobreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	/**
    	 * [$empresa description]
    	 * @var [type]
    	 */
    	$empresas = Empresa::select('ano', 'descricao')
    						->where('status', 1)
    						->oldest()
    						->get();
		
		/**
		 * Return SOBRE - [empresa]
		 */
        return view('site.sobre', [
        	'empresas' => $empresas
    	]);
    }
}
