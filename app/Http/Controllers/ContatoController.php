<?php

namespace App\Http\Controllers;

use App\Contato;
use App\Http\Requests;
use App\Newsletter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContatoController extends Controller
{

    private $contato;

    public function __construct(Contato $contato)
    {
        $this->middleware('auth');
        $this->contato = $contato;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contatos = $this->contato->where('status','<', 3)->get();
        return view('contato.index', ['contatos' => $contatos])->with('controlador', new Controller);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contato.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $contato = $this->contato->create($request->all());
        if( isset($request->newsletter) ){
            $news = new Newsletter();
            $news->nome = $request->nome;
            $news->email = $request->email;
            $news->status = 1;
            $news->save();
        }
         
        if($contato){

            $mensagem = array(
                'type' => 'success',
                'retorno' => 'Contato gravado com sucesso'
            );
        }else{

            $mensagem = array(
                'type' => 'error',
                'retorno' => 'Erro ao salvar'
            );            
        }

        return redirect()->route('manager.contato.index')->with('status', $mensagem['type'])
                                                         ->with('retornomensagem', $mensagem['retorno']);
    }

    /**
     * Show the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contato = $this->contato->findOrFail($id);
        $contato->lido = 1;
        $contato->save();
        return view('contato.show', ['contato' => $contato] );
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contato = $this->contato->findOrFail($id);  //404 HTTP response eh retornado                         
        return view('contato.edit', ['contato' => $contato] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->contato->find($id)->update($request->all());
        return redirect()->route('manager.contato.index')->with('status', 'success')
                                                         ->with('retornomensagem', 'Item editado com sucesso');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->contato->destroy($id);       
        return redirect()->route('manager.contato.index')->with('status', 'success')
                                                         ->with('retornomensagem', 'Item excluído com sucesso');
    }


    public function delete($id)
    {
        $this->contato->find($id)->update(array('status' => 3));       
        return redirect()->route('manager.contato.index')->with('status', 'success')
                                                         ->with('retornomensagem', 'Item desativado com sucesso');
    }

    public function atualizacoes()
    {
        $mensagemcontato = $this->contato->select(DB::raw('count(id) as nao_lidos'))
                                     ->where('status', '<>', 3)
                                     ->where('lido', '=', 0)
                                     ->first();
        $mensagens = $this->contato->select(DB::raw('id,nome,created_at'))
                                     ->where('status', '<>', 3)
                                     ->where('lido', '=', 0)
                                     ->get();  
        $returnHTML = view('contato.atualizacao')->with('mensagemcontato', $mensagemcontato)
                                                ->with('mensagens', $mensagens)
                                                ->render();
        return $returnHTML;
    }


}
