<?php

namespace App\Http\Controllers;

use App\Artigo;
use App\Categoria;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Intervention\Image\Facades\Image;

use App\Scopes\IdiomaScope;

class ArtigoController extends Controller
{
    private $artigo;

    public function __construct(Artigo $artigo)
    {
        $this->middleware('auth', ['except' => ['feed']]);

        /**
         * Artigo [ Remove IdiomaScope ]
         * @var [type]
         */
        $this->artigo = $artigo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $artigos = $this->artigo->where('status', 1)->get();
        return view('artigo.index', ['artigos' => $artigos])->with('controlador', new Controller);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = new Categoria();
        $listaCategorias = $categorias->comboCategorias();                
        return view('artigo.create', ['listaCategorias' => $listaCategorias]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $artigo = new Artigo();
        $artigo->id_usuario = Auth::user()->id;  
        $artigo->idioma_id = $request->idioma_id;      
        $artigo->titulo = $request->titulo;        
        $artigo->status = $request->status;        
        $artigo->resumo = $request->resumo;
        $artigo->texto = $request->texto;
        if($request->data != null){
            $artigo->data = parent::converterData($request->data, 'd/m/Y')->format('Y-m-d');
            $artigo->data .= ' '.$request->hora;
        }
        if($request->url == null ) $artigo->slug = str_slug($request->titulo, '-');
        if($request->url != null ) $artigo->slug = str_slug($request->url, '-');
            
        if($request->file('imagem') != null){

            $infoImagem = $this->uploadImagemArtigo($request, 'imagem', $artigo->slug, 630, 300);
            $artigo->imagem = $infoImagem['nome'];
        }

        if($request->file('imagem_thumb') != null){

            $this->uploadImagemArtigo($request, 'imagem', 'thumb-'.$artigo->slug, 255, 225);            
        }
                
        if($artigo->save()){
            $artigo->categorias()->sync( $request->categorias ? $request->categorias : [] );            
            $mensagem = array(
                'type' => 'success',
                'retorno' => 'Artigo gravado com sucesso'
            );
            
        }else{

            $mensagem = array(
                'type' => 'error',
                'retorno' => 'Erro ao salvar'
            );

        }
        return redirect()->route('manager.artigo.index')->with('status', $mensagem['type'])
                                                        ->with('retornomensagem', $mensagem['retorno']);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $artigo = $this->artigo->find($id);
        $categorias = new Categoria();
        $listaCategorias = $categorias->comboCategorias();
        $listaCategoriasMarcadas = $categorias->comboCategorias($artigo->id);
        $usuarioCadastro = parent::infoUsuarioAutor($artigo->id_usuario);        
                
        return view('artigo.edit', ['artigo' => $artigo, 'listaCategorias' => $listaCategorias, 
                                    'listaCategoriasMarcadas' => $listaCategoriasMarcadas, 
                                    'usuarioCadastro' => $usuarioCadastro
                                    ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {        
        $artigo = $this->artigo->find($id);
        $slugantigo = $artigo->slug;
        $novoslug = null;
        if($artigo->titulo != $request->titulo)
        {            
            $artigo->imagem = parent::alterarNomeImagem($artigo->imagem, str_slug($request->titulo, '-'), 'artigos');
            $novoslug = str_slug($request->titulo, '-');
            $artigo->slug = $novoslug;
        }
        $artigo->id_usuario = Auth::user()->id;
        $artigo->idioma_id = $request->idioma_id;
        if($request->id_instrutor != 0) $artigo->id_instrutor = $request->id_instrutor;
        $artigo->titulo = $request->titulo;
        $artigo->status = $request->status;
        $artigo->resumo = $request->resumo;
        $artigo->texto = $request->texto;
        if($request->data != null){
            $artigo->data = parent::converterData($request->data, 'd/m/Y')->format('Y-m-d');
            $artigo->data .= ' '.$request->hora;
        }
        
        if($request->url == null) $artigo->slug = str_slug($request->titulo, '-');
        if($request->slug != null && $novoslug == null){

            $artigo->slug = str_slug($request->slug, '-');
            if($artigo->imagem != null){
                parent::alterarNomeImagem('thumb-'.$artigo->imagem, $artigo->slug, 'artigos');
                $artigo->imagem = parent::alterarNomeImagem($artigo->imagem, $artigo->slug, 'artigos');
            }

        }

        if($request->file('imagem') != null){

            $infoImagem = $this->uploadImagemArtigo($request, 'imagem', $artigo->slug, 630, 300);
            $artigo->imagem = $infoImagem['nome'];            
        }

        if($request->file('imagem_thumb') != null){

            $this->uploadImagemArtigo($request, 'imagem_thumb', 'thumb-'.$artigo->slug, 255, 225);
        }

        $artigo->save();
        $artigo->categorias()->sync( $request->categorias ? $request->categorias : [] );        
        return redirect()->route('manager.artigo.index')->with('status', 'success')
                                                        ->with('retornomensagem', 'Item editado com sucesso');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->artigo->destroy($id);       
        return redirect()->route('manager.artigo.index')->with('status', 'success')
                                                        ->with('retornomensagem', 'Item excluído com sucesso');
    }

    public function delete($id)
    {
        $artigo = $this->artigo->find($id);
        $artigo->update(array('status' => 3));        
        return redirect()->route('manager.artigo.index')->with('status', 'success')
                                                        ->with('retornomensagem', 'Item desativado com sucesso');
    }


    /**
     * Retorna RSS dos artigos, esta funcao deve ser liberada de autenticacao
     * @return \Illuminate\Http\Response
     */
    public function feed()
    {

        $artigos = $this->artigo->artigosFeed();             
        $infoSite = DB::table('manager_configs')->where('nome','site_title')
                                                ->orWhere('nome','site_description')
                                                ->orWhere('nome','site_url')->get();
        $site = array();                                                    
        foreach ($infoSite as $key => $value) {            
            $site[$value->nome] = $value->valor;
        }
        
        return response()
            ->view('artigo.feed', ['artigos' => $artigos, 'site' => $site])
            ->header('Content-Type', 'text/xml');
    }


    /**
     * Adicionar categoria enquanto cadastra/edita artigo
     * @param Request $request 
     * @return string com a view carrega, use com ajax
     */
    public function criarCategoria(Request $request)
    {                
        DB::table('categorias')->insert(
            [
            'id_usuario' => Auth::user()->id,
            'idioma_id' => 1,
            'titulo' => $request->novacategoria, 
            'slug' => str_slug($request->novacategoria, '-'),
            'tipo' => 'artigo',
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s'), 
            'updated_at' => date('Y-m-d H:i:s')
            ]
        );
        $categorias = new Categoria();
        $listaCategorias = $categorias->comboCategorias();
        $listaCategoriasMarcadas = $categorias->comboCategorias($request->id_artigo);        
        $returnHTML = view('artigo.listacategorias')->with('listaCategorias', $listaCategorias)
                                                    ->with('listaCategoriasMarcadas', $listaCategoriasMarcadas)
                                                    ->render();
        return $returnHTML; 
    }


    /**
     * Upload de imagems dos artigos
     * @param \Illuminate\Http\Request $request
     * @param string $inputName name atributido ao input file no html
     * @param string $novoNome novo nome para o arquivo final
     * @param int $width especifica largura da imagem para o fit
     * @param int $height especifica altura da imagem para o fit
     * @return type
     */
    public function uploadImagemArtigo(Request $request, $inputName, $novoNome, $width, $height)
    {                
        $mime = $request->file($inputName)->getMimeType();
        $extensoesValidas = array('image/png', 'image/jpeg', 'image/pjpeg', 'image/gif');        
        if( !in_array($mime, $extensoesValidas) ) return false;

        $extensao = pathinfo($request->file($inputName)->getClientOriginalName(), PATHINFO_EXTENSION);                
        $nomeFinal = $novoNome.'.'.$extensao;
        
        $img = Image::make($request->file($inputName)->getPathName())->fit($width, $height, function ($constraint) {
            $constraint->upsize();
        });
        $img->save(public_path().'/upload/artigos/'.$nomeFinal, 100);
        $retorno = array(
            'nome' => $nomeFinal,
            'extensao' => $extensao
            );
        return $retorno;
    }


    /**
     * Carrega view com a preview de como ficara um texto apos publicacaoo
     * @param Request $request
     * @return string
     */
    public function preview(Request $request)
    {
        return view('artigo.preview', ['conteudo' => $request->conteudo])->render();
    }   

}
