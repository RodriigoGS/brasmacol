<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Artigo;
use Carbon\Carbon;

class NovidadesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /**
         * [$novidades description]
         * @var [type]
         */
        $novidades = $this->novidades( $request );

        /**
         * 
         */
        return view('site.novidades.index', [
            'novidades' => $novidades['novidades'],
            'paginate' => $novidades['paginate'],
            'filtro' => $novidades['filtro']
        ]);
    }
    public function show( $slug )
    {
        /**
         * GET Artigo
         * @var [type]
         */
        $novidade = Artigo::where('status', 1)
                            ->slug( $slug )
                            ->firstOrFail();
                            
        /**
         * Return VIEW
         */
        return view('site.novidades.show', [
            'novidade' => $novidade
        ]);
    }

    /**
     * [ajax description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function ajax(Request $request)
    {
        /**
         * [$novidades description]
         * @var [type]
         */
        $novidades = $this->novidades( $request );

        /**
         * [$returnHTML description]
         * @var [type]
         */
        $HTML = view('site.novidades.news')->with('novidades', $novidades['novidades'])
                                                 ->render();

        /**
         * Return - NEWS HTML
         */
        return $HTML;
    }

    /**
     * [novidades description]
     * @return [type] [description]
     */
    protected function novidades(Request $request)
    {
        /**
         * PARAMETERS
         * @var [type]
         */
        $limit      = $request->limit ? $request->limit : 6;
        $filtro     = $request->filtro ? $request->filtro : '';
        $paginate   = $limit + 6;

        /**
         * GET Artigos
         * @var [type]
         */
        $query = Artigo::where('status', 1)
                            ->idioma()
                            ->limit( $limit );

        /**
         * Has - FILTRO
         */
        if( !empty($filtro) )
        {
            $query->where('titulo', 'LIKE', $request->filtro)
                  ->orWhere('resumo', 'LIKE', $request->filtro);
        }

        /**
         * [$novidades description]
         * @var [type]
         */
        return [
            'novidades' => $query->get(),
            'paginate' => $paginate,
            'filtro' => $filtro
        ];
    }
}
