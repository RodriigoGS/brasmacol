<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;

use App\Cliente;
use App\ClienteAcesso;

class ClienteController extends Controller
{
    
	public function login(Request $request)
	{
		/**
		 * Login - CPF
		 */
		$cliente = Cliente::whereCPF($request->nome, $request->password);

		if( !$cliente )
		{
			/**
			 * Login - CNPJ
			 */
			$cliente = Cliente::whereCNPJ($request->nome, $request->password);

			if( !$cliente )
			{
				/**
				 * Redirect - BACK
				 */
				return back()
							->with('login', 'Credencias invalidas!')
		                    ->withInput();
			}

		}

		/**
		 * Auth - CLIENTE by ID
		 */
		Auth::guard('cliente')->loginUsingId( $cliente->id );

		/**
		 * Create - ACCESS
		 */
			
		ClienteAcesso::create([
			'cliente_id' => Auth::guard('cliente')->user()->id,
			'ip' => $request->ip()
		]);
		
		/**
		 * View - PRODUTOS
		 */
		return redirect()->route('produtos.index');
	}

	public function logout()
	{
		/**
		 * Logout - CLIENTE
		 */
		Auth::guard('cliente')->logout();

		/**
		 * View - HOME
		 */
		return redirect()->route('home.index');
	}
}
