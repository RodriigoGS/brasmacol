<?php

namespace App\Http\Controllers;

use App\Arquivo;
use App\Http\Requests;
use App\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SliderController extends Controller
{
    private $slider;

    public function __construct(Slider $slider)
    {
        $this->middleware('auth');
        $this->slider = $slider;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {                         
        $sliders = DB::table('sliders')
                        ->select(DB::raw('sliders.*, DATE_FORMAT(sliders.created_at,\'%d/%m/%Y\') as data_br, 
                                    (SELECT arquivo FROM manager_arquivos 
                                        WHERE manager_arquivos.id = sliders.id_imagem ) as imagem'))
                        ->where('status', '<', 3)
                        ->orderBy('ordem')
                        ->get();
        return view('slider.index', ['sliders' => $sliders])->with('controlador', new Controller);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {                
        return view('slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $slider = new Slider();        
        $slider->titulo = $request->titulo;
        $slider->titulo_botao = $request->titulo_botao;
        $slider->texto = $request->texto;
        $slider->url = $request->url;
        $slider->status = $request->status;
        $slider->id_usuario = Auth::user()->id;

        $ordem = DB::table('sliders')->select(DB::raw('MAX(ordem) as ordem'))->where('status', '<', 3)->first();                  
        if( empty($ordem) ) $slider->ordem = 1;
        if( !empty($ordem) ) $slider->ordem = $ordem->ordem + 1;        


        $arquivo = new Arquivo();
        $arquivo->titulo = $slider->titulo;
        $upload = parent::uploadImagem($request, 'imagem', null, true);

        if( !$upload ){

            $mensagem = array(
                'type' => 'error',
                'retorno' => 'Arquivo não permitido'
            );
            return redirect()->route('manager.slider.index')->with('status', $mensagem['type'])
                                                            ->with('retornomensagem', $mensagem['retorno']);

        }

        $arquivo->arquivo = $upload['nome'];
        $arquivo->extensao = $upload['extensao'];
        $arquivo->save();        
        $slider->id_imagem = $arquivo->id;
                
        if($slider->save()){

            $mensagem = array(
                'type' => 'success',
                'retorno' => 'Slider gravado com sucesso'
            );
            
        }else{

            $mensagem = array(
                'type' => 'error',
                'retorno' => 'Erro ao salvar'
            );            

        }

        return redirect()->route('manager.slider.index')->with('status', $mensagem['type'])
                                                        ->with('retornomensagem', $mensagem['retorno']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = $this->slider->find($id);  
        $imagem = DB::table('manager_arquivos')->select('arquivo')->where('id', $slider->id_imagem)->first();
        $usuarioCadastro = parent::infoUsuarioAutor($slider->id_usuario);
        return view('slider.edit', ['slider' => $slider, 'imagem' => $imagem, 'usuarioCadastro' => $usuarioCadastro] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {        
        $this->slider->find($id)->update($request->all());
        $slider = $this->slider->find($id);

        if($request->file('imagem') != null){

            $arquivo = new Arquivo();
            $arquivo->titulo = $slider->titulo;
            $upload = parent::uploadImagem($request, 'imagem', null, true);

            if( !$upload ){

                $mensagem = array(
                    'type' => 'error',
                    'retorno' => 'Arquivo não permitido'
                );
                return redirect()->route('manager.slider.index')->with('status', $mensagem['type'])
                                                                ->with('retornomensagem', $mensagem['retorno']);

            }

            $arquivo->arquivo = $upload['nome'];
            $arquivo->extensao = $upload['extensao'];
            $arquivo->save();
            $slider->id_imagem = $arquivo->id;
            $slider->id_usuario = Auth::user()->id;
            $slider->save();

        }        
                
        return redirect()->route('manager.slider.index')->with('status', 'success')
                                                        ->with('retornomensagem', 'Item editado com sucesso');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->slider->destroy($id);       
        return redirect()->route('manager.slider.index')->with('status', 'success')
                                                        ->with('retornomensagem', 'Item excluído com sucesso');
    }

    public function delete($id)
    {
        $this->slider->find($id)->update(array('status' => 3));       
        return redirect()->route('manager.slider.index')->with('status', 'success')
                                                        ->with('retornomensagem', 'Item desativado com sucesso');
    }
}
