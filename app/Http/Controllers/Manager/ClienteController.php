<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\ClienteRequest;

use App\Cliente;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('clientes.index', [
            'clientes' => Cliente::publish()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clientes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClienteRequest $form)
    {
        /**
         * CREATE NEW CLIENTE
         */
        if( !$form->clienteCreate() )
            return redirect()->route('manager.clientes.create')
                                ->withErrors(['Cliente não pode ser cadastrado'])
                                ->withInput();

        /**
         * Message Return
         * @var array
         */
        return redirect()->route('manager.clientes.index')
                            ->with('status', 'success')
                            ->with('retornomensagem', 'Cliente cadastrado com sucesso!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Cliente $cliente)
    {
        return view('clientes.edit', [
            'cliente' => $cliente
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClienteRequest $form, $id)
    {
        /**
         * UPDATE CLIENTE
         */
        if( !$form->clienteUpdate( $id ) )
            return redirect()->route('manager.clientes.edit', ['cliente' => $id])
                                ->withErrors(['Cliente não pode ser atualizado!'])
                                ->withInput();

        /**
         * Message Return
         * @var array
         */
        return redirect()->route('manager.clientes.index')
                            ->with('status', 'success')
                            ->with('retornomensagem', 'Cliente atualizado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Cliente $cliente)
    {
        if( ! $cliente->update(['status' => 3]) )
            return redirect()->route('manager.clientes.index')
                                ->with('status', 'error')
                                ->with('retornomensagem', 'Cliente não pode ser deletada!');

        /**
         * Message Return
         * @var array
         */
        return redirect()->route('manager.clientes.index')
                            ->with('status', 'success')
                            ->with('retornomensagem', 'Cliente deletada com sucesso!');
    }

    /**
     * Display a listing of the resource.
     * 
     * @param  Cliente $cliente [description]
     * @return [type]           [description]
     */
    public function access(Cliente $cliente)
    {
        /**
         * [$acessos description]
         * @var [type]
         */
        $acessos = $cliente->acessos()
                           ->get();

        return view('clientes.access', [
            'acessos' => $acessos
        ]);
    }
}
