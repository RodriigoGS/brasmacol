<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CategoriaRequest;

use App\Categoria;
use App\ProdutoCategoria;

class ProdutoCategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /**
         * [$categorias description]
         * @var [type]
         */
        $categorias = Categoria::where('tipo', 'produto')
                                ->where('status', 1)
                                ->get();

        /**
         * Return
         */
        return view('produtos.categorias.index', [
            'categorias' => $categorias,
            'controlador' => new Controller
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('produtos.categorias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoriaRequest $form)
    {
        /**
         * CREATE Categoria
         */
        $categoria = $form->categoriaCreate();
        
        /**
         * CREATE NEW CATEGORIA
         */
        if( !$categoria )
            return redirect()->route('manager.produtos.categorias.create')
                                ->withErrors(['Categoria não pode ser cadastrada'])
                                ->withInput();

        /**
         * Message Return
         * @var array
         */
        return redirect()->route('manager.produtos.categorias.index')
                            ->with('status', 'success')
                            ->with('retornomensagem', 'Categoria cadastrada com sucesso!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Categoria $categoria)
    {
        /**
         * Adding USER
         * @var [type]
         */
        $categoria->user = $categoria->usuario;

        /**
         * Return
         */
        return view('produtos.categorias.edit', [
            'categoria' => $categoria,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoriaRequest $form, $id)
    {
        /**
         * Update Categoria
         */
        $categoria = $form->categoriaUpdate( $id );

        /**
         * UPDATE Categoria
         */
        if( !$categoria )
            return redirect()->route('manager.produtos.categorias.create')
                                ->withErrors(['Categoria não pode ser cadastrada'])
                                ->withInput();

        /**
         * Message Return
         * @var array
         */
        return redirect()->route('manager.produtos.categorias.edit', ['categoria' => $id])
                            ->with('status', 'success')
                            ->with('retornomensagem', 'Categoria atualizada com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Categoria $categoria)
    {
        if( ! $categoria->update(['status' => 3]) )
            return redirect()->route('manager.produtos.categorias.index')
                                ->with('status', 'error')
                                ->with('retornomensagem', 'Categoria não pode ser deletada!');

        /**
         * Message Return
         * @var array
         */
        return redirect()->route('manager.produtos.categorias.index')
                            ->with('status', 'success')
                            ->with('retornomensagem', 'Categoria deletada com sucesso!');
    }

    /**
     * Adicionar categoria enquanto cadastra/edita produtos
     * @param Request $request 
     * @return string com a view carrega, use com ajax
     */
    public function adicionarAjax(CategoriaRequest $form)
    {    
        /**
         * CREATE Categoria
         */
        $categoria = $form->categoriaCreate();
        
        /**
         * [$categoriasProdutos description]
         * @var [type]
         */
        $categoriasProdutos = ProdutoCategoria::select('categoria_id')
                                                ->where('produto_id', $form->produto_id)
                                                ->get()
                                                ->toArray();

        /**
         * [$returnHTML description]
         * @var [type]
         */
        $returnHTML = view('produtos.categorias.categorias', [
            'categorias' => $categorias,
            'categoriasProdutos' => $categoriasProdutos
        ])->render();

        return $returnHTML; 
    }
}
