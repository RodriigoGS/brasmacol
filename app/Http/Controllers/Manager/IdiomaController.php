<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\IdiomaRequest;

use App\Idioma;

class IdiomaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('idiomas.index', [
            'idiomas' => Idioma::publish()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('idiomas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(IdiomaRequest $form)
    {
        /**
         * CREATE NEW IDIOMA
         */
        if( !$form->idiomaCreate() )
            return redirect()->route('manager.idiomas.create')
                                ->withErrors(['Idioma não pode ser cadastrado'])
                                ->withInput();

        /**
         * Message Return
         * @var array
         */
        return redirect()->route('manager.idiomas.index')
                            ->with('status', 'success')
                            ->with('retornomensagem', 'Idioma cadastrado com sucesso!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Idioma $idioma)
    {
        return view('idiomas.edit', [
            'idioma' => $idioma
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(IdiomaRequest $form, $id)
    {
        /**
         * UPDATE IDIOMA
         */
        if( !$form->idiomaUpdate( $id ) )
            return redirect()->route('manager.idiomas.edit', ['idioma' => $id])
                                ->withErrors(['Idioma não pode ser atualizado!'])
                                ->withInput();

        /**
         * Message Return
         * @var array
         */
        return redirect()->route('manager.idiomas.index')
                            ->with('status', 'success')
                            ->with('retornomensagem', 'Idioma atualizado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Idioma $idioma)
    {
        if( ! $idioma->update(['status' => 3]) )
            return redirect()->route('manager.idiomas.index')
                                ->with('status', 'error')
                                ->with('retornomensagem', 'Idioma não pode ser deletado!');

        /**
         * Message Return
         * @var array
         */
        return redirect()->route('manager.idiomas.index')
                            ->with('status', 'success')
                            ->with('retornomensagem', 'Idioma deletado com sucesso!');
    }
}
