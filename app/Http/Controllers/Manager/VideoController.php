<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\VideoRequest;

use App\Video;
use App\Categoria;
use App\VideoCategoria;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /**
         * GET Videos
         * @var [type]
         */
        $videos = Video::where('status', '<>', 3)->get();

        /**
         * Return VIEW - [videos]
         */
        return view('videos.index', [
            'videos' => $videos,
            'controlador' => new Controller
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /**
         * [$categorias description]
         * @var [type]
         */
        $categorias = Categoria::select('id', 'idioma_id', 'titulo', 'slug')
                                ->where('tipo', 'video')
                                ->where('status', 1)
                                ->get();

        return view('videos.create', [
            'categorias' => $categorias
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VideoRequest $form)
    {
        /**
         * CREATE NEW IDIOMA
         */
        if( !$form->videoCreate() )
            return redirect()->route('manager.videos.create')
                                ->withErrors(['Video não pode ser cadastrado'])
                                ->withInput();

        /**
         * Message Return
         * @var array
         */
        return redirect()->route('manager.videos.index')
                            ->with('status', 'success')
                            ->with('retornomensagem', 'Video cadastrado com sucesso!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video)
    {
        /**
         * [$categorias description]
         * @var [type]
         */
        $categorias = Categoria::select('id', 'idioma_id', 'titulo', 'slug')
                                ->where('tipo', 'video')
                                ->where('status', 1)
                                ->get();
                            
        $categoriasVideos = VideoCategoria::select('categoria_id')
                                                ->where('video_id', $video->id)
                                                ->get()
                                                ->toArray();

        return view('videos.edit', [
            'video' => $video,
            'categorias' => $categorias,
            'categoriasVideos' => $categoriasVideos
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(VideoRequest $form, $id)
    {
        /**
         * UPDATE IDIOMA
         */
        if( !$form->videoUpdate( $id ) )
            return redirect()->route('manager.videos.edit', ['video' => $id])
                                ->withErrors(['Video não pode ser atualizado!'])
                                ->withInput();

        /**
         * Message Return
         * @var array
         */
        return redirect()->route('manager.videos.edit', ['video' => $id])
                            ->with('status', 'success')
                            ->with('retornomensagem', 'Empresa atualizado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Video $video)
    {
        if( ! $video->update(['status' => 3]) )
            return redirect()->route('manager.videos.index')
                                ->with('status', 'error')
                                ->with('retornomensagem', 'Video não pode ser deletada!');

        /**
         * Message Return
         * @var array
         */
        return redirect()->route('manager.videos.index')
                            ->with('status', 'success')
                            ->with('retornomensagem', 'Video deletada com sucesso!');
    }
}
