<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\FormularioRequest;

use App\TrabalheConosco;

class TrabalheConoscoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /**
         * [$trabalheConosco description]
         * @var [type]
         */
        $trabalheConosco = TrabalheConosco::where('status', '<>', 3)
                                            ->get();

        /**
         * Return View - TRABALHE CONOSCO
         */
        return view('trabalhe-conosco.index', [
            'trabalheConosco' => $trabalheConosco
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('trabalhe-conosco.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FormularioRequest $form)
    {
        /**
         * CREATE NEW TRABALHE CONOSCO
         */
        if( !$form->trabalheConoscoCreate() )
            return redirect()->route('manager.trabalheconosco.create')
                                ->withErrors(['Contato não pode ser cadastrado'])
                                ->withInput();

        /**
         * Message Return
         * @var array
         */
        return redirect()->route('manager.trabalheconosco.index')
                            ->with('status', 'success')
                            ->with('retornomensagem', 'Contato cadastrado com sucesso!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(TrabalheConosco $trabalheConosco)
    {
        return view('trabalhe-conosco.edit', [
            'trabalheConosco' => $trabalheConosco
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FormularioRequest $form, $id)
    {
        /**
         * Update - TRABALHE CONOSCO
         */
        if( !$form->trabalheConoscoUpdate( $id ) )
            return redirect()->route('manager.trabalheconosco.edit', ['trabalheConosco' => $id])
                                ->withErrors(['Contato não pode ser atualizado!'])
                                ->withInput();

        /**
         * Message Return
         * @var array
         */
        return redirect()->route('manager.trabalheconosco.index')
                            ->with('status', 'success')
                            ->with('retornomensagem', 'Contato atualizado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(TrabalheConosco $trabalheConosco)
    {
        if( ! $trabalheConosco->update(['status' => 3]) )
            return redirect()->route('manager.trabalheconosco.index')
                                ->with('status', 'error')
                                ->with('retornomensagem', 'Contato não pode ser deletado!');

        /**
         * Message Return
         * @var array
         */
        return redirect()->route('manager.trabalheconosco.index')
                            ->with('status', 'success')
                            ->with('retornomensagem', 'Contato deletado com sucesso!');
    }

    public function download( $curriculo )
    {
        /**
         * File - CURRICULO
         * @var [type]
         */
        $file = storage_path('app/upload/trabalhe_conosco/' . $curriculo);

        /**
         * Download - FILE
         */
        return response()->download($file);
    }
}
