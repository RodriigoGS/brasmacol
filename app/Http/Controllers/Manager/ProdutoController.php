<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProdutoRequest;

use App\Produto;
use App\Galeria;
use App\Categoria;
use App\ProdutoCategoria;

class ProdutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('produtos.index', [
            'produtos' => Produto::publish(),
            'controlador' => new Controller
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /**
         * [$galerias description]
         * @var [type]
         */
        $galerias   = Galeria::selectGaleria();

        /**
         * [$categorias description]
         * @var [type]
         */
        $categorias = Categoria::select('id', 'idioma_id', 'titulo', 'slug')
                                ->where('tipo', 'produto')
                                ->where('status', 1)
                                ->get();
        
        /**
         * Return
         */
        return view('produtos.create', [
            'galerias' => $galerias,
            'categorias' => $categorias
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProdutoRequest $form)
    {
        /**
         * CREATE NEW PRODUTO
         */
        if( !$form->produtoCreate() )
            return redirect()->route('manager.produtos.create')
                                ->withErrors(['Produto não pode ser cadastrado'])
                                ->withInput();

        /**
         * Message Return
         * @var array
         */
        return redirect()->route('manager.produtos.index')
                            ->with('status', 'success')
                            ->with('retornomensagem', 'Produto cadastrado com sucesso!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Produto $produto)
    {
        /**
         * [$galerias description]
         * @var [type]
         */
        $galerias = Galeria::selectGaleria();

        /**
         * [$categorias description]
         * @var [type]
         */
        $categorias = Categoria::select('id', 'idioma_id', 'titulo', 'slug')
                                ->where('tipo', 'produto')
                                ->where('status', 1)
                                ->get();
                                
        $categoriasProdutos = ProdutoCategoria::select('categoria_id')
                                                ->where('produto_id', $produto->id)
                                                ->get()
                                                ->toArray();

        /**
         * Return
         */
        return view('produtos.edit', [
            'produto' => $produto,
            'galerias' => $galerias,
            'categorias' => $categorias,
            'categoriasProdutos' => $categoriasProdutos
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProdutoRequest $form, $id)
    {
        /**
         * UPDATE PRODUTO
         */
        if( !$form->produtoUpdate( $id ) )
            return redirect()->route('manager.produtos.edit', ['produto' => $id])
                                ->withErrors(['Produto não pode ser atualizado!'])
                                ->withInput();

        /**
         * Message Return
         * @var array
         */
        return redirect()->route('manager.produtos.edit', ['produto' => $id])
                            ->with('status', 'success')
                            ->with('retornomensagem', 'Produto atualizado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Produto $produto)
    {
        if( ! $produto->update(['status' => 3]) )
            return redirect()->route('manager.produtos.index')
                                ->with('status', 'error')
                                ->with('retornomensagem', 'Produto não pode ser deletado!');

        /**
         * Message Return
         * @var array
         */
        return redirect()->route('manager.produtos.index')
                            ->with('status', 'success')
                            ->with('retornomensagem', 'Produto deletado com sucesso!');
    }
}
