<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\EmpresaRequest;

use App\Empresa;

class EmpresaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('empresas.index', [
            'empresas' => Empresa::publish()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('empresas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmpresaRequest $form)
    {
        /**
         * CREATE NEW IDIOMA
         */
        if( !$form->empresaCreate() )
            return redirect()->route('manager.empresas.create')
                                ->withErrors(['Empresa não pode ser cadastrado'])
                                ->withInput();

        /**
         * Message Return
         * @var array
         */
        return redirect()->route('manager.empresas.index')
                            ->with('status', 'success')
                            ->with('retornomensagem', 'Empresa cadastrado com sucesso!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Empresa $empresa)
    {
        return view('empresas.edit', [
            'empresa' => $empresa
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmpresaRequest $form, $id)
    {
        /**
         * UPDATE IDIOMA
         */
        if( !$form->empresaUpdate( $id ) )
            return redirect()->route('manager.empresas.edit', ['empresa' => $id])
                                ->withErrors(['Empresa não pode ser atualizado!'])
                                ->withInput();

        /**
         * Message Return
         * @var array
         */
        return redirect()->route('manager.empresas.index')
                            ->with('status', 'success')
                            ->with('retornomensagem', 'Empresa atualizado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Empresa $empresa)
    {
        if( ! $empresa->update(['status' => 3]) )
            return redirect()->route('manager.empresas.index')
                                ->with('status', 'error')
                                ->with('retornomensagem', 'Empresa não pode ser deletada!');

        /**
         * Message Return
         * @var array
         */
        return redirect()->route('manager.empresas.index')
                            ->with('status', 'success')
                            ->with('retornomensagem', 'Empresa deletada com sucesso!');
    }
}
