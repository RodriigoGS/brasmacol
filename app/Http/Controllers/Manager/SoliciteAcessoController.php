<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\FormularioRequest;

use App\SoliciteAcesso;

class SoliciteAcessoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /**
         * [$soliciteoAcesso description]
         * @var [type]
         */
        $soliciteAcessos = SoliciteAcesso::where('status', '<>', 3)
                                            ->get();

        /**
         * Return View - SOLICITE CONTATO
         */
        return view('solicite-acesso.index', [
            'soliciteAcessos' => $soliciteAcessos
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('solicite-acesso.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FormularioRequest $form)
    {
        /**
         * CREATE NEW SOLICITE CONTATO
         */
        if( !$form->soliciteAcessoCreate() )
            return redirect()->route('manager.soliciteacesso.create')
                                ->withErrors(['Contato não pode ser cadastrado'])
                                ->withInput();

        /**
         * Message Return
         * @var array
         */
        return redirect()->route('manager.soliciteacesso.index')
                            ->with('status', 'success')
                            ->with('retornomensagem', 'Contato cadastrado com sucesso!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SoliciteAcesso $soliciteAcesso)
    {
        return view('solicite-acesso.edit', [
            'soliciteAcesso' => $soliciteAcesso
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FormularioRequest $form, $id)
    {
        /**
         * UPDATE SOLICITAÇAO DE CONTATO
         */
        if( !$form->soliciteAcessoUpdate( $id ) )
            return redirect()->route('manager.soliciteacesso.edit', ['soliciteAcesso' => $id])
                                ->withErrors(['Contato não pode ser atualizado!'])
                                ->withInput();

        /**
         * Message Return
         * @var array
         */
        return redirect()->route('manager.soliciteacesso.index')
                            ->with('status', 'success')
                            ->with('retornomensagem', 'Contato atualizado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(SoliciteAcesso $soliciteAcesso)
    {
        if( ! $soliciteAcesso->update(['status' => 3]) )
            return redirect()->route('manager.soliciteacesso.index')
                                ->with('status', 'error')
                                ->with('retornomensagem', 'Contato não pode ser deletado!');

        /**
         * Message Return
         * @var array
         */
        return redirect()->route('manager.soliciteacesso.index')
                            ->with('status', 'success')
                            ->with('retornomensagem', 'Contato deletado com sucesso!');
    }
}
