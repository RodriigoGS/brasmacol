<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CatalogoRequest;

use App\Catalogo;

class CatalogoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('catalogos.index', [
            'catalogos' => Catalogo::publish()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('catalogos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CatalogoRequest $form)
    {
        /**
         * CREATE NEW IDIOMA
         */
        if( !$form->catalogoCreate() )
            return redirect()->route('manager.catalogos.create')
                                ->withErrors(['Catalogo não pode ser cadastrado'])
                                ->withInput();

        /**
         * Message Return
         * @var array
         */
        return redirect()->route('manager.catalogos.index')
                            ->with('status', 'success')
                            ->with('retornomensagem', 'Catalogo cadastrado com sucesso!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Catalogo $catalogo)
    {
        return view('catalogos.edit', [
            'catalogo' => $catalogo
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CatalogoRequest $form, $id)
    {
        /**
         * UPDATE IDIOMA
         */
        if( !$form->catalogoUpdate( $id ) )
            return redirect()->route('manager.catalogos.edit', ['catalogo' => $id])
                                ->withErrors(['Catalogo não pode ser atualizado!'])
                                ->withInput();

        /**
         * Message Return
         * @var array
         */
        return redirect()->route('manager.catalogos.index')
                            ->with('status', 'success')
                            ->with('retornomensagem', 'Catalogo atualizado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Catalogo $catalogo)
    {
        if( ! $catalogo->update(['status' => 3]) )
            return redirect()->route('manager.catalogos.index')
                                ->with('status', 'error')
                                ->with('retornomensagem', 'Catalogo não pode ser deletada!');

        /**
         * Message Return
         * @var array
         */
        return redirect()->route('manager.catalogos.index')
                            ->with('status', 'success')
                            ->with('retornomensagem', 'Catalogo deletada com sucesso!');
    }
}
