<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Categoria;
use App\Artigo;
use App\Video;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /**
         * GET Categorias AND Produtos
         * @var [type]
         */
        $produtos = Categoria::where('status', 1)
                            ->where('tipo', 'produto')
                            ->idioma()
                            ->limit(3)
                            ->get();

        /**
         * GET Videos
         * @var [type]
         */
        $videos = Video::where('status', 1)
                        ->idioma()
                        ->limit(6)
                        ->get();

        /**
         * GET Artigos
         * @var [type]
         */
        $novidades = Artigo::where('status', 1)
                            ->idioma()
                            ->limit( 3 )
                            ->get();

        /**
         * Return HOME - [ produtos | videos | novidades ]
         */
        return view('site.home', [
            'produtos' => $produtos,
            'videos' => $videos,
            'novidades' => $novidades
        ]);
    }
}
