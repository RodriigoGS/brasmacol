<?php

namespace App\Http\Controllers;

use App\Arquivo;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ArquivoController extends Controller
{

    private $arquivo;

    public function __construct(Arquivo $arquivo)
    {
        $this->middleware('auth');
        $this->arquivo = $arquivo;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arquivos = $this->arquivo->where('status','<', 3)->get();
        return view('arquivos.index', ['arquivos' => $arquivos])->with('controlador', new Controller);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('arquivos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $arquivoCarregado = parent::uploadArquivo($request, 'arquivo', null, true);
        $arquivo = new Arquivo();
        $arquivo->titulo = $request->titulo;
        $arquivo->arquivo = $arquivoCarregado['nome'];
        $arquivo->extensao = $arquivoCarregado['extensao'];
        $arquivo->status = $request->status;

        if($arquivo->save()){

            $mensagem = array(
                'type' => 'success',
                'retorno' => 'Arquivo gravado com sucesso'
            );
            
        }else{

            $mensagem = array(
                'type' => 'error',
                'retorno' => 'Erro ao salvar'
            );            

        }

        return redirect()->route('manager.arquivo.index')->with('status', $mensagem['type'])
                                                         ->with('retornomensagem', $mensagem['retorno']);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arquivo = $this->arquivo->find($id);              
        return view('arquivos.edit', ['arquivo' => $arquivo] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $arquivo = $this->arquivo->find($id);              
        $arquivo->titulo = $request->titulo;
        $arquivo->status = $request->status;

        if($request->file('arquivo') != null){
            $oldName = $arquivo->arquivo;
            File::delete(public_path().'/upload/'.$oldName);
            $arquivoCarregado = parent::uploadArquivo($request, 'arquivo', null, true);
            $nomeParts = explode('.', $oldName);
            
            // $arquivo->arquivo = $arquivoCarregado['nome'];
            // $arquivo->extensao = $arquivoCarregado['extensao'];
            parent::alterarNomeImagem($arquivoCarregado['nome'], $nomeParts[0] );
        }

        if($arquivo->save()){

            $mensagem = array(
                'type' => 'success',
                'retorno' => 'Arquivo atualizado com sucesso'
            );
            
        }else{

            $mensagem = array(
                'type' => 'error',
                'retorno' => 'Erro ao salvar'
            );            

        }

        return redirect()->route('manager.arquivo.edit', ['id' => $id])->with('status', $mensagem['type'])
                                                                       ->with('retornomensagem', $mensagem['retorno']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete($id)
    {
        $arquivo = $this->arquivo->find($id);
        File::delete(public_path().'/upload/'.$arquivo->arquivo);
        $arquivo->delete();
        $mensagem = array(
            'type' => 'success',
            'retorno' => 'Arquivo excluído com sucesso'
        );
        return redirect()->route('manager.arquivo.index')->with('status', $mensagem['type'])
                                                         ->with('retornomensagem', $mensagem['retorno']);
    }


    /**
     * Upload para o CKEditor
     * @param Request $request 
     * @return string com funcao de callback para o ckeditor
     */
    public function uploadExterno(Request $request)
    {       
        $arquivoCarregado = parent::uploadArquivo($request, 'upload', null, true);
        $arquivo = new Arquivo();
        $arquivo->titulo = $arquivoCarregado['nome'];
        $arquivo->arquivo = $arquivoCarregado['nome'];
        $arquivo->extensao = $arquivoCarregado['extensao'];
        $arquivo->status = 1;
        $message = '';
        $url = '';
        if($arquivo->save()){
            $url = url('/upload').'/'.$arquivoCarregado['nome'];
        }else{
            $message = 'Erro no upload de imagem';
        }

       $funcNum = 1;
       $CKEditor = $request->CKEditor; //referencia do editor, ja vem no ajax
       $langCode = $request->langCode; //referencia do idioma do editor, ja vem no ajax
       echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
    
    }
    
}
