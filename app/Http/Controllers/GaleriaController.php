<?php

namespace App\Http\Controllers;

use App\Galeria;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class GaleriaController extends Controller
{

    private $galeria;


    public function __construct(Galeria $galeria)
    {
        $this->middleware('auth');
        $this->galeria = $galeria;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galerias = $this->galeria->where('status', 1)->get();
        return view('galeria.index', ['galerias' => $galerias])->with('controlador', new Controller);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('galeria.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $galeria = new Galeria();
        $galeria->descricao = $request->descricao;
        $galeria->id_usuario = Auth::user()->id;
        $galeria->status = $request->status;        
                
        if($galeria->save()){

            $mensagem = array(
                'type' => 'success',
                'retorno' => 'Galeria gravada com sucesso'
            );

            return redirect()->route('manager.galeria.edit', ['id' => $galeria->id])->with('status', $mensagem['type'])
                                                                                    ->with('retornomensagem', $mensagem['retorno']);
        }else{

            $mensagem = array(
                'type' => 'error',
                'retorno' => 'Erro ao salvar'
            );

            return redirect()->route('manager.galeria.index')->with('status', $mensagem['type'])
                                                             ->with('retornomensagem', $mensagem['retorno']);

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $galeria = $this->galeria->find($id);
        $imagens = DB::table('galerias_imagens')->select('id','id_galeria','imagem','ordem','created_at')
                                                ->where('id_galeria', $id)
                                                ->where('status', 1)
                                                ->orderBy('ordem', 'asc')
                                                ->get();
        $usuarioCadastro = parent::infoUsuarioAutor($galeria->id_usuario);
        $dados = array(
            'galeria' => $galeria,
            'imagens' => $imagens,
            'usuarioCadastro' => $usuarioCadastro
        );
        return view('galeria.edit')->with($dados);
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->galeria->find($id)->update($request->all());
        $galeria = $this->galeria->find($id);
        
        return redirect()->route('manager.galeria.index')->with('status', 'success')
                                                         ->with('retornomensagem', 'Item editado com sucesso');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->galeria->destroy($id);       
        return redirect()->route('manager.galeria.index')->with('status', 'success')
                                                         ->with('retornomensagem', 'Item excluído com sucesso');
    }

    public function delete($id)
    {
        $this->galeria->find($id)->update(array('status' => 3));       
        return redirect()->route('manager.galeria.index')->with('status', 'success')
                                                         ->with('retornomensagem', 'Item desativado com sucesso');
    }



    /**
     * Realiza o upload da imagem para a galeria
     * @param Request $request 
     * @return boolean true em caso de upload realizado com sucesso, false do contrario
     */
    public function uploadImagemGaleria(Request $request)
    {        
        
        if ($request->file('file') != null) {
            
            $mime = $request->file('file')->getMimeType();
            $extensoesValidas = array('image/png', 'image/jpeg', 'image/pjpeg', 'image/gif');        
            if( !in_array($mime, $extensoesValidas) ) return response()->json(['error' => 'Extensão não permitida'],400);
            
            $nomeArquivo = pathinfo($request->file('file')->getClientOriginalName(), PATHINFO_FILENAME);
            $extensao =  pathinfo($request->file('file')->getClientOriginalName(), PATHINFO_EXTENSION);            
            $nomeArquivo = str_slug($nomeArquivo, '-');
            $nomeFinal = rand(1000,9999).'-'.$nomeArquivo.'.'.$extensao;            
            DB::table('galerias_imagens')->insert(
                [
                'id_galeria' => $request->id_galeria, 
                'imagem' => $nomeFinal,
                'ordem' => 2,
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
                ]
            );
            $request->file('file')->move(public_path().'/upload', $nomeFinal);
            return response()->json(['success' => 'Imagem enviada com sucesso!'],200);
        }
        return response()->json(['error' => 'Upload não autorizado'],403);
    }


    /**
     * Lista imagens da galeria.
     * @param Request $request 
     * @return string a view renderizada dentro de uma string, uso comum em ajax
     */
    public function imagensGaleria(Request $request)
    {

        $imagens = DB::table('galerias_imagens')->select('id','id_galeria','imagem','ordem','created_at')
                                                ->where('id_galeria', $request->id)
                                                ->where('status', 1)
                                                ->orderBy('ordem', 'asc')
                                                ->get();
        $returnHTML = view('galeria.imagens')->with('imagens', $imagens)->render();
        return $returnHTML;
    }

    /**
     * Alterar status da imagem da galeria para 3 e nao exclui a imagem
     * @param int $idAlbum 
     * @param int $id 
     * @return response volta para edicao da galeria
     */
    public function deleteImagem($idAlbum, $id)
    {
        
        DB::table('galerias_imagens')->where('id', $id)->update(['status' => 3]);
        return redirect()->route('manager.galeria.edit', ['id' => $idAlbum])->with('status', 'success')
                                                                            ->with('retornomensagem', 'Imagem excluída');        
    }


}