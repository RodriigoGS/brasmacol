<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Catalogo;

class CatalogoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /**
         * GET Catalogos
         * @var [type]
         */
        $catalogos = Catalogo::where('status', 1)
                            ->idioma()
                            ->get();
                            
        /**
         * Return VIEW
         */
        return view('site.catalogos.index', [
            'catalogos' => $catalogos
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($catalogo)
    {
        /**
         * [$catalogo] - slug
         * @var [type]
         */
        $catalogo = Catalogo::slug( $catalogo )->firstOrFail();

        /**
         * Return VIEW
         */
        return view('site.catalogos.show', [
            'catalogo' => $catalogo
        ]);
    }
}
