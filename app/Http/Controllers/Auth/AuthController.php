<?php

namespace App\Http\Controllers\Auth;

use App\Usuario;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Auth;
use Session;
use Illuminate\Http\Request;
use App\Http\Requests\LoginPostRequest;
use App\Http\Requests\RegisterUserPostRequest;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/manager/dashboard';
    protected $loginPath = '/auth/login';
    protected $redirectAfterLogout = '/auth/login';    

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware($this->guestMiddleware(), ['except' => 'logout']);
        $this->middleware($this->guestMiddleware(), ['except' => ['logout', 'getLogout']]);        
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        $rules = array(
            'usuario' => 'required|max:255',
            'email' => 'required|email|max:255|unique:usuarios',
            'senha' => 'required|min:6|confirmed',
        );

        $messages = array(
            'email.required'=>'Campo e-mail é obrigatório',
            'email.unique'=>'Já existe este e-mail em nossa base de dados',
            'usuario.required'=>'Campo usuário é obrigatório',
            'senha.required' => 'Campo senha é obrigatório',
            'senha.min'=>'Deve possuir no mínimo 6 caracteres',
            'senha.confirmed'=>'As senhas não conferem'
        );
        
        return Validator::make($data, $rules, $messages);       
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {

        return Usuario::create([
            'id_perfil' => 1,
            'nome' => $data['usuario'],
            'usuario' => $data['usuario'],
            'email' => $data['email'],
            'senha' => bcrypt($data['senha']),
        ]);
    }


    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin()
    {        
        $view = property_exists($this, 'loginView')
                    ? $this->loginView : 'auth.authenticate';

        if (view()->exists($view)) {
            return view($view);
        }

        return view('auth.login');
    }


    /**
     * Handle a login request to the application.
     *
     * @param  \App\Http\Requests\LoginPostRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(LoginPostRequest $request) //https://scotch.io/tutorials/simple-and-easy-laravel-login-authentication
    {        


        // criando arrays para conferir os dados do form login com o banco de dados
        $dadosNome = array(
            'usuario' => $request->input('email'),                
            'password'  => $request->input('senha'), //password != senha da tabela, password eh hardcoded no framework
        );  

        $dadosEmail = array(
            'email' => $request->input('email'),                
            'password'  => $request->input('senha'), //password != senha da tabela, password eh hardcoded no framework
        );           

        //tenta realizar o login
        if (Auth::attempt($dadosNome) || Auth::attempt($dadosEmail)) { //confere com os dados na tabela/model

            // validation successful!                             
            return redirect('manager/dashboard');

        } else {        
            
            
            $mensagem = array(
                'type' => 'danger',
                'retorno' => 'Nome de usuário ou E-mail e Senha não conferem com nossos registros'
            );
            return redirect('auth/login')->with('status', $mensagem['type'])
                                         ->with('retornomensagem', $mensagem['retorno']);                                              

        }

    }


    /**
     * Get the failed login message.
     * @return string
     */
    protected function getFailedLoginMesssage()
    {
        return 'Nome de usuário ou E-mail e Senha não conferem com nossos registros';
    }


    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {       
        Auth::logout();
        Session::flush();     
        return redirect('auth/login');      
        
    }


    public function register(Request $request)
    {        
        $validator = $this->validator($request->all());
        if(count($validator->errors()) > 0){
            return redirect('auth/register')->withErrors($validator)
                                            ->withInput($request->except('senha'));            
        }else{

            $usuario = $this->create($request->all());
            //dd($usuario->id);
             $mensagem = array(
                'type' => 'success',
                'retorno' => 'Registro realizado com sucesso'
            );
            return redirect('auth/login')->with('status', $mensagem['type'])
                                         ->with('retornomensagem', $mensagem['retorno']);  
            
        }
        
        
    }
}
