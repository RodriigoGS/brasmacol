<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Video;
use App\Categoria;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( $categoria = null )
    {
         /**
         * GET Categorias AND Videos
         * @var [type]
         */
        $categorias = Categoria::where('status', 1)
                                ->where('tipo', 'video')
                                ->idioma()
                                ->with(['videos' => function ($query) {
                                    $query->where('status', '1');
                                    $query->idioma();
                                }])->get();

        /**
         * Return VIEW
         */
        return view('site.videos', [
            'categorias' => $categorias,
            'categoria' => $categoria
        ]);
    }
}
