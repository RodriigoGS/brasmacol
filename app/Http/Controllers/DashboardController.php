<?php

namespace App\Http\Controllers;

use App\Acesso;
use App\Http\Requests;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {               
        $acessos = Acesso::visitas();
        $acessosunicos = Acesso::visitas(true);
        $maisvisitado = Acesso::maisVisitado();      
        return view('home', ['pageviews' => $acessos->acessos, 
                             'visitasunicas' => $acessosunicos->acessos,
                             'maisvisitado' => $maisvisitado
                             ]
        );
    }

    /**
     * Realiza a contabilidade de pageviews por dia dentro de um mes/ano
     * @param Request $request informar ano e mes para a busca
     * @return json object
     */
    public function graficoAcessos(Request $request)
    {        
        $acessos = new Acesso();
        $dadosAcessos = $acessos->acessosDiarios($request->mes, $request->ano);
        $dadosBrutos = $dadosAcessos->keyBy('dia')->all(); //o numero do dia transformado no indice do conjunto
        $dados = array();
        for($i=1; $i <= 31; $i++){
            if(array_key_exists($i,$dadosBrutos)){
                $dados[] = $dadosBrutos[$i]->total;
            }else{
                $dados[] = 0;
            }
        }        
        return json_encode($dados);
    }
}
