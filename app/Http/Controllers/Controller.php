<?php

namespace App\Http\Controllers;

use App\Sitemap;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;


    /**
     * Realiza o upload de uma imagem generica
     * @param Request $request Illuminate\Http\Request
     * @param string $inputName 
     * @param string $novoNome 
     * @param boolean $randName se null o nome do arquivo sera $novoNome, se true sera adicionado prefixo com valor randomico
     * @param string $folder diretorio dentro de /upload
     * @return array|boolean nome e extgensao do arquivo ou false se nao finalizar o upload
     */
    public function uploadImagem(Request $request, $inputName, $novoNome = null, $randName = null, $folder = null)
    {                
        $mime = $request->file($inputName)->getMimeType();
        $extensoesValidas = array('image/png', 'image/jpeg', 'image/pjpeg', 'image/gif');        
        if( !in_array($mime, $extensoesValidas) ) return false;

        $extensao = pathinfo($request->file($inputName)->getClientOriginalName(), PATHINFO_EXTENSION);

        if($novoNome == null) $novoNome = str_slug(pathinfo($request->file($inputName)->getClientOriginalName(), PATHINFO_FILENAME), '-');
        
        if($randName == null) $nomeFinal = $novoNome.'.'.$extensao;
        if($randName != null) $nomeFinal = rand(11111,99999).'-'.$novoNome.'.'.$extensao;

        if($folder != null) $folder .= '/';
        $request->file($inputName)->move(public_path().'/upload/'.$folder, $nomeFinal);
        $retorno = array(
            'nome' => $nomeFinal,
            'extensao' => $extensao
            );
        return $retorno;
    }


    /**
     * Renomeia o nome do arquivo caso o slug tambem tenha alterado
     * @param string $imagem nome antigo da imagem
     * @param string $slug 
     * @param string $folder diretorio da localizacao do arquivo dentro de /upload
     * @return string|boolean novo nome caso tenha conseguido renomear, false do contrario
     */
    public function alterarNomeImagem($imagem, $slug, $folder = null)
    {
        if($folder != null) $folder .= '/';
        $infoImagem = explode(".", $imagem);
        $old = public_path().'/upload/'.$folder.$imagem;
        $novoNome = $slug.'.'.$infoImagem[1];
        $new = public_path().'/upload/'.$folder.$novoNome;
        $file = File::move($old, $new);
        if(!$file) return false;
        return $novoNome; 
  
    }


    /**
     * Exibição de status padrao
     * @param int $cod 
     * @return string
     */
    public function exibeStatus($cod){

        if($cod == 2) return 'Desativado';
        if($cod == 1) return 'Ativo';
        return '';
    }


    /**
     * Retorna informações de algum usuario, utilizar somente para exibição de dados 
     * nas informações de autoria de CRUDs
     * @param int $id 
     * @return object
     */
    public function infoUsuarioAutor($id)
    {
        return DB::table('manager_usuarios')->where('id', $id)->first();
    }

    /**
     * Utilizado no gerenciador de arquivos para exibir no link o icone correspondente ao tipo do arquivo
     * @param string $type 
     * @return string
     */
    public function faIcon($type)
    {
        $icone = 'fa-';
        if( ($type == 'png') || ($type == 'jpg') || ($type == 'jpeg') ) $icone .= 'file-image-o';
        if( ($type == 'pdf') ) $icone .= 'pdf-o';
        if( ($type == 'avi') || ($type == 'mpeg') || ($type == 'flv') ) $icone .= 'file-video-o';
        if( ($type == 'wave') || ($type == 'mp3') ) $icone .= 'file-audio-o';
        if( ($type == 'xls') || ($type == 'xlsx') || ($type == 'csv') ) $icone .= 'file-excel-o';
        if( ($type == 'ppt') || ($type == 'pptx') || ($type == 'odp') ) $icone .= 'file-powerpoint-o';
        if( ($type == 'doc') || ($type == 'docx') || ($type == 'odf') ) $icone .= 'file-word-o';
        return $icone;
    }


    /**
     * Estrutura de Unidades Federativas para select de options, compativel com Laravel HTML Collective
     * @return array
     */
    public function comboUF()
    {
        return array(
            "RS" =>'Rio Grande do Sul',
            "AC" =>'Acre',
            "AL" =>'Alagoas',
            "AP" =>'Amapá',
            "AM" =>'Amazonas',
            "BA" =>'Bahia',
            "CE" =>'Ceará',
            "DF" =>'Distrito Federal',
            "ES" =>'Espirito Santo',
            "GO" =>'Goiás',
            "MA" =>'Maranhão',
            "MS" =>'Mato Grosso do Sul',
            "MT" =>'Mato Grosso',
            "MG" =>'Minas Gerais',
            "PA" =>'Pará',
            "PB" =>'Paraíba',
            "PR" =>'Paraná',
            "PE" =>'Pernambuco',
            "PI" =>'Piauí',
            "RJ" =>'Rio de Janeiro',
            "RN" =>'Rio Grande do Norte',
            "RO" =>'Rondônia',
            "RR" =>'Roraima',
            "SC" =>'Santa Catarina',
            "SP" =>'São Paulo',
            "SE" =>'Sergipe',
            "TO" =>'Tocantins'
            );
    }

    /**
     * Retira mascara de input com jquery.input.maskMoney()
     * @param string $value o value do input
     * @param string $thousand 
     * @param string $decimal      
     * @param string $currency 
     * @return string input sem mascara
     */
    public function removerMascaraMoeda($value, $thousand = '.', $decimal = ',', $currency = 'R$ ')
    {
        $value = str_replace($thousand, '', $value );
        $value = str_replace($decimal, '.', $value );
        $value = str_replace($currency, '', $value );        
        return $value;
    }

    public function uploadArquivo(Request $request, $inputName, $folder = null, $randName = null, $novoNome = null)
    {                
        $mime = $request->file($inputName)->getMimeType();       

        if($novoNome == null) $novoNome = str_slug(pathinfo($request->file($inputName)->getClientOriginalName(), PATHINFO_FILENAME), '-');
        $extensao = pathinfo($request->file($inputName)->getClientOriginalName(), PATHINFO_EXTENSION);
        
        if($randName == null) $nomeFinal = $novoNome.'.'.$extensao;
        if($randName != null) $nomeFinal = $novoNome.'-'.rand(11111111,99999999).'.'.$extensao;        
        
        if($folder != null) $folder .= '/';
        $request->file($inputName)->move(public_path().'/upload/'.$folder, $nomeFinal);
        $retorno = array(
            'nome' => $nomeFinal,
            'extensao' => $extensao
            );
        return $retorno;
    }

    public function criarEntradaSiteMap($slug, $arquivo, $periodicidade = 'weekly', $prioridade = 0.5)
    {
        $sitemap = new Sitemap();        
        $sitemap->loc = URL::to('/').'/'.$slug;
        $sitemap->arquivo = $arquivo;
        $sitemap->changefreq = $periodicidade;
        $sitemap->priority = $prioridade;
        if($sitemap->save()) return true;
        return false;
    }

    public function atualizaEntradaSiteMap($slugantigo, $slugnovo)
    {       
        $entrada = Sitemap::where('loc', 'like', '%' . $slugantigo . '%')->first();
        if($entrada != null){
            $entrada->loc = URL::to('/').'/'.$slugnovo;
            $entrada->save();
            return true;
        }
        return false;
    }


    public function removerEntradaSitemap($slug)
    {
        $entrada = Sitemap::where('loc', 'like', '%' . $slug . '%')->first();
        if($entrada != null){
            $entrada->delete();
            return true;
        }
        return false;
    }

    /**
     * Conversao de datas
     * @param string $dataOriginal
     * @param string $formatoOriginal pode ser 'Y-m-d'     
     * @return DateTime object
     */
    public function converterData($dataOriginal, $formatoOriginal)
    {      
        $date = \DateTime::createFromFormat($formatoOriginal, $dataOriginal);
        return $date;
    }
}
