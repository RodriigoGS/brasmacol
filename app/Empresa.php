<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'ano', 'descricao', 'status', 'idioma_id'
	];

    /**
     * Get the language record associated with the company.
     */
    public function idioma()
    {
        return $this->belongsTo('App\Idioma');
    }

    /**
     * [publish description]
     * @return [type] [description]
     */
    public static function publish()
    {
        return Empresa::where('status', 1)->get();
    }
}
