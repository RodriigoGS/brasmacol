<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class ImageRepository
{
	/**
	 * [$request description]
	 * @var [type]
	 */
	protected $request;

	/**
	 * [__construct description]
	 * @param Request $request [description]
	 */
	public function __construct(Request $request)
	{
		$this->request = $request;
	}

	/**
	 * [imageModel description]
	 * @return [type] [description]
	 */
	public function imageModel( $field, $path, $width, $height )
	{
	    if ($this->request->hasFile( $field )) 
	    {
	    	/**
	    	 * [$file description]
	    	 * @var [type]
	    	 */
	    	$file = $this->fileName( $this->request->file( $field ));
	        /**
	         * Path IMAGE
	         * @var [type]
	         */
	        $path = public_path() . '/upload/' . $path . '/' . $file;

	        /**
	         * Save image
	         */
	        $save = Image::make($this->request->file( $field )->getPathName())->fit(890, 400, function ($constraint) 
	        {
	            $constraint->upsize();

	        })->save($path, 100);

	        /**
	         * Verify save
	         */
	        if( \File::exists( $path ) )
	        {
	            return $file;
	        }
	    }

	    return false;
	}

	/**
	 * [fileName description]
	 * @param  [type] $request [description]
	 * @return [type]          [description]
	 */
	public function fileName( $request )
	{
		/**
		 * File EXTENSION
		 * @var [type]
		 */
	    $extensao   = pathinfo($request->getClientOriginalName(), PATHINFO_EXTENSION);
	    
	    /**
	     * File NAME
	     * @var [type]
	     */
	    $nome       = str_replace($extensao, '', $request->getClientOriginalName());
	    
	    /**
	     * File FULLNAME
	     * @var [type]
	     */
	    $file       = str_slug(config('app.name') . '-' . (new \DateTime(''))->format('H:i:s') . ' - ' . $nome) . '.' . $extensao;

	    /**
	     * Return FILE
	     */
	    return $file;
	}
}