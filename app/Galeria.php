<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Galeria extends Model
{    
    protected $fillable = ['id_usuario','descricao','status'];

    /**
     * Get the produtos.
     */
    public function produtos()
    {
        return $this->hasMany('App\Produto');
    }

    /**
     * [idioma description]
     * @return [type] [description]
     */
    public function galeria_imagens()
    {
    	return $this->hasMany('App\GaleriaImagem', 'id_galeria');
    }

    /**
     * [selectGaleria description]
     * @return [type] [description]
     */
    public static function selectGaleria()
    {
    	/**
    	 * Galeria: ID, DESCRICAO
    	 * @var [type]
    	 */
    	$galerias = Galeria::select('id', 'descricao')->get();

		/**
		 * Filter
		 */
        $galeriasArray[] = 'Nenhuma';

    	foreach ($galerias as $galeria) 
        {
            $galeriasArray[ $galeria->id ] = $galeria->descricao;
        }

        return $galeriasArray;
    }
}
