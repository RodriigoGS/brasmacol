<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Acesso extends Model
{
    public $timestamps = false;
    //protected $primaryKey = ['id', 'data'];
    protected $fillable = ['data', 'ip', 'caminho', 'useragent'];

    /**
     * Contabiliza visitas
     * @param null|bool $unicas true para contabilizar visitias unicas, defaul false para pageviews
     * @return object com atributo acessos
     */
    public static function visitas($unicas = false)
    {
        $acessos = null;
        if(!$unicas) $acessos = Acesso::select(DB::raw('count(*) as acessos'))->first();        
        if($unicas) $acessos = Acesso::select(DB::raw('COUNT(DISTINCT ip) as acessos'))->first();       
        return $acessos;
    }

    /**
     * Contabiliza a url do site mias visitada
     * @return object com total_acessos e caminho
     */
    public static function maisVisitado()
    {
        $consulta = null;
        $consulta = Acesso::select(DB::raw('count(*) as total_acessos, caminho'))
                            ->groupBy('caminho')
                            ->orderBy('total_acessos', 'DESC')
                            ->limit(1)
                            ->first();
        return $consulta;
    }


    public function acessosDiarios($mes, $ano)
    {       
        $consulta = $this->select(DB::raw('count(id) as total, DAY(data) as dia'))
                         ->whereMonth('data', '=', $mes)
                         ->whereYear('data', '=', $ano)
                         ->groupBy('dia')
                         ->get();
        return $consulta;
    }


    public function acessosDiariosUnicos($mes, $ano)
    {       
        $consulta = $this->select(DB::raw('COUNT(DISTINCT ip) as total, DAY(data) as dia'))
                         ->whereMonth('data', '=', $mes)
                         ->whereYear('data', '=', $ano)
                         ->groupBy('dia')
                         ->get();
        return $consulta;
    }


    public function getPorPagina()
    {
        $data = array();
        $acessos = $this->select(DB::raw('caminho, COUNT(id) as total'))       
                         ->groupBy('caminho')                         
                         ->get();
        $acessosunicos = $this->select(DB::raw('caminho, count(DISTINCT ip) as total'))       
                              ->groupBy('caminho')
                              ->get();
        if( count($acessos) <= 0) return $data;
        foreach ($acessos as $key => $value) {
           $data[$key] = array(
                            'caminho' => $acessos[$key]->caminho,
                            'pageviews' => $acessos[$key]->total,
                            'acessosunicos' => $acessosunicos[$key]->total
                            );
        }

        return $data;
    }

    public function porNavegador()
    {
        $acessos = $this->all();
        $navegadores = array(
                    'Firefox' => 0,
                    'IE' => 0,                  
                    'Chrome' => 0,
                    'Opera' => 0,
                    'Safari' => 0,
                    'Outros' => 0,
        );
        foreach ($acessos as $acesso) {

            if ( stripos($acesso->useragent, 'Firefox') !== false ) {
                $navegadores['Firefox']++;
            } elseif ( stripos($acesso->useragent, 'MSIE') !== false ) {
                $navegadores['IE']++;
            } elseif ( stripos($acesso->useragent, 'Trident') !== false ) {
                $navegadores['IE']++;            
            } elseif ( stripos($acesso->useragent, 'Chrome') !== false ) {
                $navegadores['Chrome']++;
            } elseif ( stripos($acesso->useragent, 'Safari') !== false ) {
                $navegadores['Safari']++;
            } elseif ( stripos($acesso->useragent, 'Opera') !== false ) {
                $navegadores['Opera']++;
            }elseif ( stripos($acesso->useragent, 'OPR') !== false ) {
                $navegadores['Opera']++;
            } else{
                $navegadores['Outros']++;
            } 
        }

        return $navegadores;
    }


    public function porSistemaOperacional()
    {
        $acessos = $this->all();
        $sistemasoperacionais = array(
                    'Windows' => 0,
                    'Linux' => 0,                  
                    'Android' => 0,
                    'iPad' => 0,                   
                    'iPhone' => 0,
                    'Outros' => 0
        );
        foreach ($acessos as $acesso) {

            if ( stripos($acesso->useragent, 'Windows') !== false ) {
                $sistemasoperacionais['Windows']++;
            } elseif ( stripos($acesso->useragent, 'Linux') !== false ) {
                $sistemasoperacionais['Linux']++;
            } elseif ( stripos($acesso->useragent, 'Android') !== false ) {
                $sistemasoperacionais['Android']++;            
            } elseif ( stripos($acesso->useragent, 'iPad') !== false ) {
                $sistemasoperacionais['iPad']++;
            } elseif ( stripos($acesso->useragent, 'iPhone') !== false ) {
                $sistemasoperacionais['iPhone']++;
            } else{
                $sistemasoperacionais['Outros']++;
            } 
        }

        return $sistemasoperacionais;
    }
}
