<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Idioma extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'locale', 'nome', 'status'
	];

	/**
	 * [publish description]
	 * @return [type] [description]
	 */
	public static function publish()
	{
		return Idioma::where('status', 1)->get();
	}
}
