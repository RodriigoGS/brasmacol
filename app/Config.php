<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $table = 'manager_configs';
    protected $fillable = ['nome','valor'];
    public $timestamps = false;
}
