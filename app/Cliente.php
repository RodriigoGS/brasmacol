<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Cliente extends Authenticatable
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'nome', 'cpf', 'cnpj', 'telefone', 'email', 'status'
	];

    /**
     * Get the access for the cliente.
     */
    public function acessos()
    {
        return $this->hasMany('App\ClienteAcesso');
    }

    /**
     * [publish description]
     * @return [type] [description]
     */
    public static function publish()
    {
    	return Cliente::where('status', 1)->get();
    }

    /**
     * [whereCPF description]
     * @param  [type] $nome [description]
     * @param  [type] $cpf  [description]
     * @return [type]       [description]
     */
    public static function whereCPF($nome, $cpf)
    {
        return Cliente::where('nome', $nome)
                      ->where('cpf', $cpf)
                      ->first();
    }

    /**
     * [whereCNPJ description]
     * @param  [type] $nome [description]
     * @param  [type] $cnpj [description]
     * @return [type]       [description]
     */
    public static function whereCNPJ($nome, $cnpj)
    {
        return Cliente::where('nome', $nome)
                      ->where('cnpj', $cnpj)
                      ->first();
    }
}
