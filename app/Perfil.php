<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Perfil extends Model
{
    protected $table = 'manager_perfis';
    protected $fillable = ['descricao','status'];


    public function menu()
    {
    	return $this->belongsToMany('App\Menu', 'manager_perfil_menu', 'id_perfil', 'id_menu');
    }


    public function opcoesSelect()
    {    	
    	if(Auth::user()->id_perfil == 1) $data = $this->where('status', 1)->get();
        if(Auth::user()->id_perfil != 1) $data = $this->where('status', 1)->where('id','<>', 1)->get();
    	$opcoes = array(); 

		foreach ($data as $perfil) {
			$opcoes[$perfil->id] = $perfil->descricao;
			
		}	
    	return $opcoes;
    }

    
}
