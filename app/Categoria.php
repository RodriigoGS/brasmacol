<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Categoria extends Model
{    
    /**
     * [$fillable description]
     * @var [type]
     */
    protected $fillable = [
        'titulo','slug', 'tipo', 'descricao', 'thumb', 'status', 'id_usuario', 'idioma_id'
    ];

    /**
     * [usuario description]
     * @return [type] [description]
     */
    public function usuario()
    {
        return $this->belongsTo('App\Usuario', 'id_usuario');
    }

    /**
     * [artigos description]
     * @return [type] [description]
     */
    public function artigos()
    {
    	return $this->belongsToMany('App\Artigo', 'artigos_categorias', 'id_categoria', 'id_artigo');    	
    }

    /**
     * [artigos description]
     * @return [type] [description]
     */
    public function produtos()
    {
        return $this->belongsToMany('App\Produto', 'produto_categorias', 'categoria_id', 'produto_id');     
    }

    /**
     * [artigos description]
     * @return [type] [description]
     */
    public function videos()
    {
        return $this->belongsToMany('App\Video', 'video_categorias', 'categoria_id', 'video_id');     
    }

    /**
     * [comboCategorias description]
     * @param  [type] $idArtigo [description]
     * @return [type]           [description]
     */
    public function comboCategorias($idArtigo = null)
    {    	
    	$categorias = null;
    	if($idArtigo == null) $categorias = Categoria::where('status', 1)->where('tipo', 'artigo')->get();
    	if($idArtigo != null) $categorias = DB::table('artigos_categorias')->where('id_artigo', $idArtigo)->lists('id_categoria');    	
        return $categorias;
    }

    /**
     * [publishProduto description]
     * @return [type] [description]
     */
    public static function publishProduto()
    {
        return Categoria::where('status', '1')->get();
    }

    /**
     * [scopeIdioma description]
     * @param  [type] $query  [description]
     * @param  [type] $idioma [description]
     * @return [type]         [description]
     */
    public function scopeIdioma($query, $idioma = null)
    {
        /**
         * [$idioma_id description]
         * @var [type]
         */
        $idioma_id = $idioma ? $idioma : config('location.modelLocale')[ config('app.locale') ];

        /**
         * Return Scope
         */
        return $query->where('idioma_id', $idioma_id);
    }
}
