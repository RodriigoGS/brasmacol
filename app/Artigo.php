<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use App\Scopes\IdiomaScope;

class Artigo extends Model
{    
    /**
     * [$fillable description]
     * @var [type]
     */
    protected $fillable = ['titulo','slug', 'resumo', 'texto', 'imagem', 'id_usuario', 'idioma_id', 'status'];    

    /**
     * [categorias description]
     * @return [type] [description]
     */
    public function categorias()
    {    	
    	return $this->belongsToMany('App\Categoria', 'artigos_categorias', 'id_artigo', 'id_categoria');
    }

    /**
     * Retorna informacoes de todos os artigos para o feed RSS
     * @return array
     */
    public function artigosFeed()
    {
    	$artigos = array();
        $i = 0;
        $infoArtigos = $this->where('status', 1)->get();
        foreach ($infoArtigos as $artigo) {

            $artigos[$i]['titulo'] = $artigo->titulo;
            $artigos[$i]['resumo'] = $artigo->resumo;
            $artigos[$i]['slug'] = $artigo->slug;
            $artigos[$i]['created_at'] = $artigo->created_at;
            $artigos[$i]['data'] = Carbon::parse($artigo->created_at)->format('D, d M Y H:i:s'); // data em GMT, RSS 2.0
            $artigos[$i]['categorias'] = $artigo->categorias()->get();
            $i++;
        }
        return $artigos;
    }
    
    /**
     * [publish description]
     * @return [type] [description]
     */
    public static function publish()
    {
        return Artigo::where('status', 1)->get();
    }

    /**
     * [formatDate description]
     * @param  [type] $date   [description]
     * @param  [type] $format [description]
     * @return [type]         [description]
     */
    public function formatDate( $date, $format )
    {
        /**
         * SET LOCALE
         */
        setlocale(LC_ALL, config('location.format'));

        /**
         * Return DATE
         */
        return strftime($format, strtotime( $date ));
    }

    /**
     * [scopeIdioma description]
     * @param  [type] $query  [description]
     * @param  [type] $idioma [description]
     * @return [type]         [description]
     */
    public function scopeIdioma($query, $idioma = null)
    {
        /**
         * [$idioma_id description]
         * @var [type]
         */
        $idioma_id = $idioma ? $idioma : config('location.modelLocale')[ config('app.locale') ];

        /**
         * Return Scope
         */
        return $query->where('idioma_id', config('location.modelLocale')[ config('app.locale') ]);
    }

    /**
     * [scopeSlug description]
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeSlug( $query, $slug )
    {
        return $query->where('slug', $slug);
    }
}
