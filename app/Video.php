<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use App\Scopes\IdiomaScope;

class Video extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'titulo', 'link', 'status', 'idioma_id'
    ];

    /**
     * Get the language record associated with the video.
     */
    public function idioma()
    {
        return $this->hasOne('App\Idioma');
    }

    /**
     * Get the category that owns the video.
     */
    public function categorias()
    {
        return $this->belongsToMany('App\Categoria', 'video_categorias', 'video_id', 'categoria_id');
    }

    /**
     * [publish description]
     * @return [type] [description]
     */
    public static function publish()
    {
    	return Video::where('status', 1)->get();
    }

    /**
     * [scopeIdioma description]
     * @param  [type] $query  [description]
     * @param  [type] $idioma [description]
     * @return [type]         [description]
     */
    public function scopeIdioma($query, $idioma = null)
    {
        /**
         * [$idioma_id description]
         * @var [type]
         */
        $idioma_id = $idioma ? $idioma : config('location.modelLocale')[ config('app.locale') ];

        /**
         * Return Scope
         */
        return $query->where('idioma_id', $idioma_id);
    }
}
