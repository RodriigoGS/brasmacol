<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrabalheConosco extends Model
{
	/**
	 * [$table description]
	 * @var string
	 */
	protected $table = 'trabalhe_conosco';

    /**
	 * [$fillable description]
	 * @var [type]
	 */
    protected $fillable = [
    	'nome','email', 'telefone', 'mensagem', 'curriculo', 'status', 'lido'
	];
}
