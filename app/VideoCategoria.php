<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoCategoria extends Model
{
    /**
     * [$table description]
     * @var string
     */
    protected $table = 'video_categorias';

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'categoria_id', 'video_id'
	];

    /**
     * Get the videos for the Category.
     */
    public function videos()
    {
        return $this->hasMany('App\Video');
    }

    /**
     * [categorias description]
     * @return [type] [description]
     */
    public function categorias()
    {
        return $this->belongsToMany('App\Categoria');
    }
}
