<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{    
    protected $fillable = ['id_usuario','id_imagem','titulo', 'titulo_botao', 'texto', 'url', 'ordem', 'status'];
    protected $dates = ['created_at', 'updated_at'];
}
