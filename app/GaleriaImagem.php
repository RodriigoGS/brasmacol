<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GaleriaImagem extends Model
{
	/**
	 * [$table description]
	 * @var string
	 */
    protected $table = 'galerias_imagens';

    /**
     * [$fillable description]
     * @var [type]
     */
    protected $fillable = [
    	'id_galeria', 'imagem', 'ordem', 'status'
    ];

    /**
     * [galeria_imagem description]
     * @return [type] [description]
     */
    public function galeria_imagem()
    {
    	return $this->belongsTo('App\Galeria', 'id_galeria');
    }
}
